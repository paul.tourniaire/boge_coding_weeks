# Setting up Git and GitLab for Your Project

For any software development work, it's essential to put your project under Git so you can share your code and collaborate. This guide outlines the steps to set up Git and GitLab for your project.

## First Task: Give a Name to Your Project

One of the initial things to do before setting up Git is to agree on a name for your project. We recommend using a meaningful name that refers to your project's content and helps identify your team. For example, if you're working on the "2048 game" project and your team is called "Uranus Team," you could use "2048_game_by_Uranus_team" as your project name. We'll assume this in the following steps.

## Second Task: Set-up an SSH key

Carefully read the instructions from the [documentation](https://docs.gitlab.com/ee/user/ssh.html), in particular these paragraphs:

+ [Generate an SSH key pair](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)
+ [Add an SSH key to your Gitlab account](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)
+ [Verify that you can connect](https://docs.gitlab.com/ee/user/ssh.html#verify-that-you-can-connect)

:warning: Windows users, the instructions work when using the "Git Bash" console instead of the Terminal application.

## Creating Your GitLab Repository

To begin, configure Git and GitLab to collaborate on your project. Suppose your group consists of three people: :a:, __B__, and __C__.

### Step :one:: :abc: All Team Members - Logging In to GitLab

Each person in the group logs in to the dedicated GitLab server for your project. Next, let's assume user :a: is taking the lead.

### Step :two:: :a: only - Creating Your GitLab Repository

- Access the GitLab server dedicated to your project and create a blank project, naming it with the project name you agreed upon, e.g., "2048_game_by_Uranus_team."

- Untick the "Initialize repository with a README" box in the "Project Configuration" section

![Creating a GitLab Project](./Images/Gitlab_project_1.png)

![Untick readme](./Images/initialize_without_readme.png)

- You'll now have a view like this:

![New GitLab Project](./Images/Gitlab_project_3.png)


### Step :three:: :a: only - Adding Collaborators

- You will add other members of your group, your assigned instructors, as well as the course staff (Paul Tourniaire, Eva Feillet and Maykel Mattar)  with usernames: @paul.tourniaire, @eva.feillet and @maykel.mattar) as collaborators to your repository.

  - Go to your GitLab repository and choose "Members" from the left-hand menu. Add your team members and instructors as collaborators, with different roles (e.g., "Maintainer" for team members and "Reporter" for instructors).

![Adding Collaborators in GitLab](./Images/Gitlab_project_4.png)



### Step :four:: :abc: All Team Members - Clone Your Repository Locally

Each team member should clone the repository to their local machine:

- Open a terminal (you can use the [integrated terminal in Visual Studio Code](https://code.visualstudio.com/docs/editor/integrated-terminal) if you prefer).

- Navigate to the directory where you want to create your local project using the `cd` command.

- Get the web address of your GitLab repository.

![Get GitLab Repository URL](./Images/Gitlab_project_5.png)

Instructions for these steps are provided on your GitLab repository's website.

- (Optional) Copy the two configuration lines:
```bash
git config --local user.name "Surname Name"
git config --local user.email "name.surname@student-cs.fr"
```

- Clone the repository to your machine by running the following in your chosen directory:

```bash
git clone repository_url
```

A new directory will appear, currently empty if your repository is empty. If there were files in your repository, the directory would contain the repository's content.

- If you encounter the "Couldn't find ref remote master" error, create any file in your GitLab repository. If you encounter a certificate error, you can either disable SSL or create an SSH key pair.

- Next, update your local repository from the remote server:

```bash
git pull
```

  This command downloads new commits from the server to your local machine. If you haven't made any changes, it's a straightforward update.

## Exercise: Collaborative README.md Editing

To familiarize yourselves with Git and collaborative coding, you'll work together to write the README.md file for your project.

The README.md file is crucial for providing essential information about the project's files. You can follow this comprehensive guide on how to write such a file: [Make A README](https://www.makeareadme.com/). It will help you when you need to create a README for your project.

Here's how you can tackle this task collaboratively:

- One team member initiates the README.md file from the GitLab repository using the online editor. Start by completing the file with the team member descriptions.

![Creating a README.md in GitLab](./Images/AddReadme.png)

  - Commit the README.md file with the descriptions but leave the team member descriptions incomplete.

![Committing the README.md](./Images/commitreadme.png)

  - The file should look similar to the one below, with placeholders for team member descriptions:

![GitLab README.md](./Images/readmegitlab.png)

  By completing these steps, you have made a modification to your remote repository by adding a new file and making a commit. The actions taken are similar to running the following Git commands:

```bash
git add README.md
git commit -m "ADD README.md"
```

- Each team member updates their local repository from the remote repository to ensure they have the latest changes:

```bash
git pull
```

  At this stage, your local repository and the remote repository should be in sync.

- Each team member can now work on their individual branch, just as if they were developing a feature for the project. Create a branch named "readmeX," where X is your name (e.g., "readmeMartin"). The branch will be used for editing your part of the README.md file:

```bash
git checkout -b readmeX
```

  - Make your edits to the README.md file using an editor such as VSCode and save the file. Commit your changes:

```bash
git add README.md
git commit -m "Add of my description"
```

  - You can also use the `git log` command to view the commit history:

```bash
git log
```

  This command displays the commit history.

- Push your branch and changes to the remote repository:

```bash
git push --set-upstream origin readmeX
```

  The `--set-upstream` option establishes a link between your local branch and the remote branch. 

## Recap: At this point, all team members should have created their own branch, made the required changes, and pushed their branches to the remote repository, where you should see several branches.

Your remote repository will now have multiple branches:

![GitLab Branches](./Images/branchgitlab.png)

and

![GitLab Branches 2](./Images/branchgitlab2.png)

### Undoing a Commit

If you realize you've made a commit by mistake or your project stops working after a commit, you'll need to undo the problematic change from your repository. Here are several Git commands to help you in this situation.

To undo your last commit:

```bash
git reset HEAD
```

If you want to update the commit message of your last commit:

```bash
git commit --amend -m "Updated commit message"
```

If you need to completely undo the last commit, including the changes in your files:

```bash
git reset --hard HEAD^
```

Remember that this will remove all the work done in the previous commit.

Undoing changes to a file before a commit:

```bash
git checkout filename
```

We will delve into these concepts further as you work on your project.

### Merging Changes with `git merge`

When you've finished working on a branch and it's ready, it's customary to merge that branch into `master` with the `git merge` command. This step can occasionally lead to conflicts, especially when you and other team members have edited the same lines in the README.md file.

To merge your changes with the `master` branch:

- Update your local repository from the remote repository to ensure that it's up-to-date:

```bash
git pull
```

- Check which branch you are currently on using the appropriate command.

- Switch to the `master` branch:

```bash
git checkout master
```

- Merge the branch `readmeX` with your changes into the `master` branch:

```bash
git merge readmeX
```

- Once the merge is completed, the `readmeX` branch is no longer needed and can be deleted:

```bash
git branch -d readmeX
```

### Handling Conflicts

Conflicts typically arise when two people have modified the same lines in a file, or when one developer has deleted a file while another developer is editing it. In these cases, Git cannot automatically determine the correct version. You'll need to resolve the conflict manually.

Here are some common conflict situations.

#### Git Can't Start the Merge

```
error: Entry '<fileName>' not uptodate. Cannot merge. (Changes in working directory)
```

This typically occurs when there are pending changes in the working directory or the staging area of the current project. Git cannot start the merge because these pending changes might be replaced by the merged commits. It doesn't signify a conflict between developers but an issue with local changes. Resolve this by stabilizing the local state using `git checkout`, `git commit`, or `git reset`.

#### Git Encounters an Issue During Merge

```
error: Entry '<fileName>' would be overwritten by merge. Cannot merge. (Changes in the staging area)
```

This indicates a conflict between the current local branch and the branch being merged. It's often a conflict with other developers' code. Git will do its best to merge the files but will leave it to you to resolve conflicts manually in the affected files. If the merge fails, you'll receive an error message.

#### Step to Handle Conflicts

If you want to practice dealing with merge conflicts:

- Create a new branch that you will use for the conflict merge by running:

```bash
git checkout -b readMetomergelater
```

- Add some content to the README.md file:

```bash
echo "totally different content to merge later" > README.md
```

- Commit the new content.
- Switch back to the main branch (master) and add more content to the README.md file:

```bash
echo "content to append" > README.md
```

- Commit the new content.
- Execute the command to merge the `readMetomergelater` branch:

```bash
git merge readMetomergelater
```

  You should encounter a conflict.

To view information about the conflict and resolve it, use the `git status` command. The output will guide you in resolving the conflict:

This indicates that some paths are not merged due to a conflict. The file `README.md` now appears as modified. You can use `cat README.md` to see the content of the file.

Certain lines will look strange, like:

```plaintext
<<<<<<< HEAD
=======
>>>>>>> readMetomergelater
```

These lines are conflict markers:

- `=======` is the center of the conflict.
- Content between `<<<<<<< HEAD` and `=======` is from the current main branch (where the HEAD reference points).
- Content between `=======` and `>>>>>>> readMetomergelater` is from the branch you're trying to merge.

To resolve the conflict, simply edit the conflicted file, remove the conflict markers, and save it. Your file should contain the merged content you want in the README.md file.

After editing, run the following commands:

```bash
git add README.md
git commit -m "Merged and resolved the conflict in README.md"
```

Git will recognize that the conflict has been resolved and create a merge commit to complete the merge.

### Final Steps

At this point, you should be prepared to work on the README.md collaboratively. Remember to:

- Push your changes to the remote repository.
- Resolve any potential conflicts
- Update individually your local folder