# Cheatsheet Bash

An interface called a shell is used to communicate between the user and the operating system. `Bash` is a widely used shell. The user interacts with the shell via a CLI (Command Line Interface), i.e. by typing lines of code which contain commands to be executed by the operating system. A terminal is a program that opens a graphical window in which users can type these commands. You saw this during the **Root Me challenge** in seminar.

## Open a shell

* VSCode contains an integrated terminal (View > Terminal), giving access to the [`bash`](https://fr.wikipedia.org/wiki/Bourne-Again_shell) shell.
* Windows accesses the [`PowerShell`](https://en.wikipedia.org/wiki/PowerShell) shell (there is also a `Terminal` application running `PowerShell` on the most recent versions of Windows).
* Under Mac, `Terminal` is used to access the `bash` (or `zsh`) shell.
* Under Ubuntu, Terminal is used to launch the `bash` shell.
* If you have a more exotic distribution, you probably don't need this
section of the tutorial


## Commands
All these terminals open different shells; each shell has subtle differences in the commands they are able to interpret. Nevertheless, the following commands can be used
in any terminal:

* `pwd`: gives the name of the parent folder. Useful for understanding where you've put your working folder.
* `cd <location>`: "change directory". Allows you to move around the file tree.
	* Example: `cd Documents` to enter the "Documents" folder; `cd ..` to move up one tree (back to parent folder).
* `ls`: display folders and files in the current directory. Useful for checking why your autocomplete isn't working, or if you've successfully moved or created files.
* `mkdir <folder_name>`: create a folder
* `touch <file_name>`: create a file
* `mv <source> <destination>`: move a folder.

## Git commit and no message

:x: __When using git, at some point, you may forget to add a message to your commit. Here are two things that can happen if this is the case:__

## :point_right: I have opened `nano`

nano is a minimalist text editor. 


![Nano](./Images/nano.png)

Some basic commands :

+ `ctrl+o` to quit while saving
+ `ctrl+x` to exit. If any changes are made, nano will ask if you want to save.



## :point_right: I have opened `vim` (which is very likely when committing to git if you forget to add a message)

`Vim` is a command-line editor. It is often recognized by the "vim" keyword in the header (see image), and the absence of navigation aids at the bottom of the screen (unlike nano, see image above). Most commands are typed directly, with no need for the 'enter' key to validate them.


![Nano](./Images/vim.png)




* `i` : enter insert mode (to write text). It will say "insert" at the bottom.
* `esc`: exit insert mode
* `:x` or `:wq` to save and exit
* `:q` or `:q!` to exit without saving

## Small training exercise

To practice your basic commands, try this little exercise.



* Open a terminal: you can either use VS Code's built-in terminal or your own OS terminal (Terminal on macOS or Linux, Git Bash on Windows).
* What is your current directory? (:beginner: hint, command `pwd`)
* Using the `cd` command, go to your working directory (very often the `~/Documents` directory).
* Display directory contents (:beginner: hint, command `ls`)
* Create a new `TestBash` directory in your current directory (:beginner: hint, command `mkdir`)
* Check that this directory has been created by redisplaying the contents of your current directory.
* Create a new `HelloWorld.txt` file in this directory (:beginner: hint, command `touch`).
* Edit it with the content of your choice (here you can choose to do it with VSCode as editor, but it's also a good opportunity to test `vim` or `nano`).
* Now test the commands below:
	* `ls -a`
	* `ls -l`
	* `ls -t`
What do they do? (:beginner: [hint](https://www.hostinger.com/tutorials/linux-commands))
* Copy the contents of your `HelloWorld.txt` file into the `HelloWorldbis.txt` file (:beginner: hint, command [`cp`](https://www.hostinger.com/tutorials/linux-commands))
* Check your action
* Delete the `HelloWorldbis.txt` file (:beginner: [hint](https://www.hostinger.com/tutorials/linux-commands))
* Once you have completed this little exercise, you can now take the associated test on Edunao, which can be found [here](https://centralesupelec.edunao.com/mod/quiz/edit.php?cmid=139300)




