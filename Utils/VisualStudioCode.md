# Visual Studio Code (or VSCode)


[Visual Studio Code](https://code.visualstudio.com/) (not to be confused with Visual Studio) is the most popular code editor of recent years. It's very well suited to Python and web development and, above all, it's very easy to learn and understand.


You should have familiarized yourself with Visual Studio Code during the previous course. If not, a very good VSCode tutorial for python is available [here](https://realpython.com/python-development-visual-studio-code/) or on the [official website](https://code.visualstudio.com/docs/python/python-tutorial).

The rest of this document is a quick overview of this tool.

## Installation and configuration

There are several installation options.

* If you have installed Anaconda, you also have direct access to VSCode via the Anaconda Navigator.


<img src="./Images/Anaconda_navigator.png" alt="drawing" width="500"/>



* Alternatively, just go to this [site](https://code.visualstudio.com/) and follow the instructions.



Visual Studio Code is an extensible editor that supports a wide range of programming languages on the basis of an extension principle. In our case, this means installing the [Python extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python). It is also possible to use the [Anaconda] extension directly (https://marketplace.visualstudio.com/items?itemName=ms-python.anaconda-extension-pack).



## Getting started: a folder-oriented editor

Visual Studio Code is a folder-oriented text editor, i.e. it is designed to work on folders, which it regards as projects. 

To create a project, all you have to do is open a folder in VSC.


<img src="./Images/vscpython.png" alt="drawing" width="500"/>



For example, to create the HelloWorld project in python, you'll need to:
 
 + Open a `helloworld` folder by creating one if necessary.
 + Once this folder has been created, create and add to it a `helloworld.py` file as shown [here](https://code.visualstudio.com/docs/python/python-tutorial#_create-a-python-hello-world-source-code-file)

## Running a program

To run a program, such as `helloworld.py`, execute the `Run Python File in Terminal` command, accessible by right-clicking from the editing window.

<img src="./Images/runvsc.png" alt="drawing" width="500"/>



Other options include :
 
 + selecting one or more lines, then `Shift+Enter` or right-click and `Run Selection/Line in Python Terminal`. This command is useful for testing only part of a file.
 + In the `(⇧⌘P)` command palette, select the `Start REPL` command, which opens a REPL terminal (interactive window) for the selected Python interpreter. In the REPL, you can then enter and execute lines of code one at a time.
 + The [coderunner] extension (https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner) is also an interesting extension for code execution, especially for those who like execution buttons.  

## Debugging 
 
VS Code offers several interesting features for debugging your code:

 + Variable inspection
 + Breakpoints
 + Call stack inspection

The Degogger can simply be launched with the `F5` command.

<img src="./Images/debug.png" alt="drawing" width="500"/>

The basic functions are described [here](https://code.visualstudio.com/docs/python/python-tutorial#_configure-and-run-the-debugger).

## Git integration

  
It's also possible to integrate Git directly into Visual Studio Code. If you'd like to do so, take a look at this [tutorial](https://code.visualstudio.com/docs/editor/versioncontrol).
 
 