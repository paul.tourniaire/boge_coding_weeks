# File and Module Imports in Python

As you'll quickly discover, we'll place a lot of emphasis on code structuring during these two weeks. In particular, we'll use the concepts of **modules** and **packages**, which are fundamental in Python and facilitate **[modular programming](https://en.wikipedia.org/wiki/Modular_programming)**.

We'll have the opportunity to delve into the principles of proper Python project structuring in detail. Here, we'll provide a quick reminder of what modules and packages are in Python.

## Modules

A module is any Python code file (i.e., any file with a `.py` extension) imported into another file or script.

Modules allow code separation and organization. You'll see this week that it's common to split your code into different files, each containing coherent functionality or parts of the final program. This makes the code easier to understand and maintain, and it's especially important for teamwork.

You're already familiar with various modules, such as standard library modules that are not part of the core Python language but are automatically integrated into Python, like [`math`](https://docs.python.org/3/library/math.html) and [`random`](https://docs.python.org/3/library/random.html), for example.

### Creating a Module

Creating a module in Python is straightforward. You simply write a set of functions (and/or constants) in a file and save it with a `.py` extension, just like any other Python script.

#### Example

Create a file in your working directory named `power.py` and define two functions in it: `square()` and `cube()`. Such a file is called a module and can be imported into another file, especially the one containing the main program.

```python
def square(value):
    result = value ** 2
    return result

def cube(value):
    result = value ** 3
    return result
```

Now, you can use the functions defined in the `power.py` module in another program. To do this, you need to import the functions from the module.

```python
from power import square

a = 5
u = square(a)
print("The square is", u)
```

The `power.py` file must be in the same directory as the program (or it should be in Python's "path").

When importing functions from the module, you can either:

- Explicitly import one or both functions:

  ```python
  from power import square
  ```

  ```python
  from power import square, cube
  ```

- Import everything:

  ```python
  from power import *
  ```

**:heavy_exclamation_mark: Importing all functions with `*` is strongly discouraged. It doesn't provide a clear view of which functions are imported and can lead to potential errors.**

A good practice is to give the module an alias:

```python
import power as pw
a = 5
u = pw.square(a)
print("The square is", u)
v = pw.cube(a)
print("The cube is", v)
```

## Packages

When you have a large number of modules, it can be beneficial to organize them into directories. A directory that contains modules is called a **package**. The package name is the same as the directory name. For example, if you create a directory named `package1` and place a file named `module1.py` in it:

```python
def function1(a):
    return a ** 2
```

You can then use the `function1()` defined in `module1.py` by importing `package1.module1`, as shown in the following example:

```python
import package1.module1

u = package1.module1.function1(3)
print("u is", u)
```

Before Python 3.5, for a directory to be recognized as a package, it had to contain an `__init__.py` file. This is no longer required, but it's still useful. When a package is imported, it's actually its `__init__.py` module that's imported.

## How Does Import Work?

What happens when you execute the `import mod1` statement?

The first thing Python does is look for the name `mod1` in `sys.modules`. This is a cache of all modules that have been previously imported.

If the name isn't found in the module cache, Python will search in a list of built-in modules. These modules come pre-installed with Python and are part of the Python standard library. If the name still isn't found among the built-in modules, Python will search in a list of directories defined by `sys.path`. This list usually includes the **current directory**, which is searched first.

When Python finds the module, it binds it to a name with local scope. This means that `mod1` is now defined and can be used in the current file without causing a `NameError` exception.

If the name is never found, you'll get a `ModuleNotFoundError` error.

If you want to learn more, you can read the official documentation [here](https://docs.python.org/3/reference/import.html).

## Absolute Imports

An absolute import specifies the resource to be imported using its full path from the project's root directory.

Let's consider the project structure below:

* A directory `P` containing your entire project.
* A directory `A` within `P` containing the file `test.py` (which contains the function `my_test()`).
* A directory `B` within `P` containing

 the file `main.py`.

![Example](./Images/PAB.png)

You want to import the `my_test()` function into `main.py`.

In the `main.py` file, you should write:

```python
from A.test import my_test
```

To execute the `main.py` file, it's best to use the terminal directly. If you click the green arrow, this execution is done relative to the directory where `main.py` is located (i.e., `B`), so the `import A.test` won't be found (as mentioned above).

Open the terminal, navigate to the root directory of the project (`P`), and run the following command:

```bash
python -m B.main
```

This way, Python will use the `P` directory as the working directory. Any reference it finds (e.g., `from A.test import my_test`) will be interpreted relative to this directory.

## Small Practice Exercise

To become familiar with these concepts, we invite you to try this small exercise:

* Create a project with the following structure.

![Example](./Images/proj.png)

* Write the `function1` function in the `module1.py` file that displays a message of your choice.
* Write the `function3` function in the `module3.py` file that displays a message of your choice.
* Write the `function2` function in the `module2.py` file, which calls `function1` and `function3`. You'll need to manage imports for this. Try to do it both using absolute and relative imports.
* Place yourself in the `project` directory.
* Execute the `module2.py` file.
