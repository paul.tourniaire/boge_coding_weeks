# Getting started with git and gitlab


As you have seen in the `Introduction to Programming Course`, [**git**](https://git-scm.com/) is a decentralized version management software, and certainly the most [popular](https://news.softpedia.com/news/git-2-8-2-popular-source-code-management-system-released-with-over-18-bug-fixes-503591.shtml).


As it's a fundamental tool for all team-based software development work, and even more so in today's remote environment, we'll be giving you a number of hands-on training sessions on this tool during the first week of coding weeks.


## Available resources

* A small cheatsheet 
* A more complete cheatsheet [here](https://education.github.com/git-cheat-sheet-education.pdf)
* Other recommended resource : 
	* [http://marklodato.github.io/visual-git-guide/index-en.html](http://marklodato.github.io/visual-git-guide/index-en.html)


## Some training

Before taking the plunge, we'd like to offer you a few training sessions using the [Learn Git Branching tool](https://learngitbranching.js.org/).


 **WARNING**: this is clearly not a speed race, but a question of understanding this tool and its various commands. Please feel free to refer to the various resources mentioned.


In particular, before proceeding to the next level, we ask you to :

* Complete the first 4 challenges of the `Introduction Sequence` level in the `Main` tab:  
	*  `1: Introduction to Git Commits`
	*  `2: Branching in Git`
	*  `3: Merging in Git`
	*  `4: Rebase Introduction`

You should have this as your screen after solving these 4 challenges. 


<img src="./Images/gittuto.png" alt="drawing" width="500"/>
	

* Similarly, complete the first four challenges in the `Push & Pull -- Git Remotes!` level of the `Remote` tab.
	* `1: Clone Intro`
	* `2: Remote Branches`
	* `3: Git Fetchin'`
	*  `4: Git Pullin'`
	


You should have this as a screen this after solving these 4 challenges. 


<img src="./Images/gittuto2.png" alt="drawing" width="500"/>


When you've finished, you can take the short knowledge test on the EDUNAO space for coding weeks : [it's here](https://centralesupelec.edunao.com/mod/quiz/view.php?id=139301)

 
 
 
 
 
 

 
 
 
 
 
 
 
