# Python and environments

## Knowing your python version

To check which version of python is installed on your system, simply type in the command interpreter:

```
python -V
```

or

```
python --version
```

In a Python script, you can use the sys (source) module

```
import sys
print (sys.version)

```

## Python Environments

In Python, we often use what are known as Python environments. A Python "environment" is the context in which a Python program runs, and consists of an interpreter and any number of installed packages.


### Global and virtual environment 

By default, any Python interpreter you install runs in its own **global environment**, which is not specific to a particular project. For example, if you launch `python` (Windows) or `python3` (macOS/Linux) at a new command prompt, you'll be running in that interpreter's global environment. Consequently, any package you install or uninstall affects the global environment and all the programs you run in that context.

Although working in the global environment is an easy way to get started, over time this environment will become cluttered with many different packages you have installed for different projects.

For this reason, developers often create a **virtual environment** for a given project. A virtual environment is a sub-folder within a project that contains a copy of a specific interpreter. When you activate the virtual environment, any packages you install are installed only in that environment's subfolder. When you run a Python program in this environment, you know that it will only run with these specific packages.

Please note that if you're not using a virtual environment and you have several versions of Python installed and defined in the path environment variable, you may need to specify which Python interpreter to use in the terminal to install packages in the global environment.


Here's what you need to know about using a virtual environment.


* To activate the virtual environment, i.e. configure the user's session to use this Python installation rather than the system one, type the command :

```
source env1/bin/activate
```

In general, the command prompt will be modified to display, in brackets, the name of the virtual environment (here `(env1)`).


* To install a library in the virtual environment, make sure you have activated it, then type the command :

```
pip install the_library
```

* To deactivate the virtual environment, i.e. reconfigure the user's session to use the Python system installation, type the command :

```
deactivate
```


### conda environment

A conda environment is a Python environment managed using the package manager [`conda`](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html). Conda works well for creating environments with interdependent dependencies as well as binary packages. Unlike virtual environments, which are limited to a single project, conda environments are available globally on a given computer. This makes it easy to set up several different Conda environments, and then choose the right one for a given project.



## Selecting and activating an environment in VSCode

By default, the Python extension searches for and uses the first Python interpreter it finds in the system path. To select a specific environment, use the Python command: Select interpreter in the command palette (`⇧⌘P`).


![SelectInterpreter](./Images/select-interpreters-command.png)

The Python `: Select Interpreter` command displays a list of available global environments, conda environments and virtual environments. The following image, for example, shows several installations of Anaconda and Python as well as a conda environment and a virtual environment (env) located in the workspace folder:


![interpreters-list](./Images/interpreters-list.png)


The Python extension uses the selected environment to execute Python code (using the command `Python : Run Python File in Terminal`), provide linguistic services (auto-completion, syntax checking, linting, formatting, etc.) when you have a .py file open in the editor, and open a terminal with the command `Terminal : Create a new terminal`. In the latter case, VS Code has automatically activated the selected environment.



The status bar always indicates the selected environment.


![selected-interpreter-status-bar](./Images/selected-interpreter-status-bar.png)


## Small training exercise

To practice with the basic commands, try this little exercise.


* Create a `TestEnv` directory in your working directory and add [this file](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/Codes/clustering.py) to it. This file is used to illustrate the kmeans clustering algorithm in the [`sklearn`] library (https://scikit-learn.org/stable/).
* Open this file with Visual Studio Code 
* Open VSCode's built-in terminal.
* What is the python execution environment chosen by your VSCode? 
* Run the file from VSCode (with the `Run Python File in Terminal` command) (depending on your python installation and chosen environment, this may or may not work).
* Create a virtual environment called `testsklearn` with the command `python -m venv testsklearn` in VSCode's built-in terminal.
* The window below should appear. You can ignore this window or click `Yes` to make this environment your workspace environment. 

![env](./Images/envvsc.png)

* Activate your virtual environment with the command `source testsklearn/bin/activate`.
* Observe that your command prompt changes.
* Using the `pip` or `pip3` command, install the `matplotlib` and `sklearn` packages in your `testsklearn` environment. 
	* `pip install matplotlib` command
	* `pip install sklearn`
* * Select this environment as the runtime environment in VSCode and execute the `clustering.py` file.
* Deactivate your `testsklearn` environment with the `deactivate` command.

In the rest of the week, you may or may not be working with virtual environments. In all cases, you'll need to pay close attention to the execution environment of your programs, as many of the bugs you encounter are due to a lack of control over this aspect.

You can now return to the subject of your project and continue with the python tutorials.

