# Git cheatsheet
## Initialize a repository
Create a directory, `cd` in it and write:
```bash
git init
```
## Add a file to commit queue
```bash
git add filename.ext
```

## Check status of the changes
```bash
git status
```

## Make a commit of the changes
```bash
git commit -m "Your message here"
```

## Create a branch
```bash
git branch nameOfTheBranch
```

## Delete a branch
```bash
git branch -d nameOfTheBranch
```

## Check list of branches
```bash
git branch
```

## Switch to this branch
```bash
git checkout nameOfTheBranch
```

## Push changes (only after `git commit`)
```bash
git push -u origin nameOfTheBranch
```
## Merge branch with main
```bash
# From within the main branch
git merge nameOfTheBranch
```

## Hold modifications in the commit queue
```bash
# In case a file has been added using git add
git stash
```

## Display stash list
```bash
git stash list
```

## Select stash by id
```bash
# stash IDs are given by the previous instruction
git stash apply stashID
```

## Get commit log
```bash
git log commit
```

## Cancel last commit
```bash
git reset --hard HEAD^
```

## Register canceled commit on branch
```bash
git checkout nameOfTheBranch
# commit ID is given by git log
git reset --hard HEAD^ 
```

## Correct commit message
```bash
git commit --amend -m "New Message"
```

## Add a file to the previous commit
```bash
# First add the file
git add ForgottenFile
# Then, amend the commit
git commit --amend --no-edit
```

## Use `rebase` instead of `merge`
```bash
# First create a new branch
git checkout -b newBranch
# After modifications, make a commit on this branch
git commit
# Rebase on main (make sure you saved your last commit on main)
git rebase main
```