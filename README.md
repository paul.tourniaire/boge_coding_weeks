# Bachelor of Global Engineering Coding Weeks 

## Welcome to the Coding Weeks!

This two-week activity will enable you to improve your programming and development skills. 

At the end of this period, you'll be able to create a project of your choice in Python.

This course will be divided into two sequences. The first week will be dedicated to learning useful concepts, through practice, by programming one of these projects:

* The 2048 game
* The game of life
* A pendulum simulation
* A data visualization tool on a public dataset

The second will be a project week, during which you will be working a more elaborate, similar kind of tool you worked on during the first week.

## Prerequisites


<!-- Before you start, please :

- Join the TEAMS team with the code  **vvzjguh** 

- Name and register your team in this [sheet](https://centralesupelec.sharepoint.com/:x:/s/CodingWeels_BoGe_2023_2024/ERteWPh4mZBNqpG4mruvPCwB_rY0z23pO577kX8xCxZnJA?e=lxK4uE)
- Join the coding weeks slack using this [invitation](https://join.slack.com/t/slack-gng6614/shared_invite/zt-264dws1y9-lr_Mf8FbSiQNuFSicTL_7Q)


> Slack is a collaborative communication platform widely used in business. It's how you chat with each other, share advices and receive additional information from supervisors. You can either download the Slack app or use the online version.  -->


<!-- ### Visual Studio Code (VSCode)

For these two weeks, we strongly recommend the use of [Visual Studio Code](https://code.visualstudio.com/) (not to be confused with Visual Studio).

You can use another editor if you wish. It will then be your responsibility to know how to use it.

* :point_right: _[I don't know how to use VSCode](./Utils/VisualStudioCode.md)._ -->

### Git

Git is one of the most important tools of the coding weeks. Using git, you will be able to collaborate on the same project together.

+ __First and foremost, follow this [tutorial](./Utils/git_install_en.md)__
+  [Here](./Utils/git_cheatsheet.md) is a cheetsheat for the most used git commands.
+  A short tutorial on Git using [Learn Git Branching](https://learngitbranching.js.org/) which is available [here](./Utils/Git.md)

### A quick reminder on bash

During these two weeks, you will have to use the shell a lot. You should always have a bash cheatsheet close by to help you navigate easily in the terminal. Remember, the internet is also full of answers!

:point_right: _[Cheatsheet Bash](./Utils/bash.md)_

### Python

You should all have python installed on your machine at this point. If that is not the case, all the instructions needed to install it are on edunao.

Here are just some tips you can come back to once you have started working on the projects:

:point_right: _[Knowing and checking your python version ](./Utils/pythonversion.md) (see later)_

:point_right: _[How to import files and modules](./Utils/modulespackagespython.md)_


 For the rest, you can always go back to your seminar, tutorial, and homework subjects, and ask online—or even better: your friends—for precious advice

Before moving on, you'll need to take the 2 tests on Edunao if you haven't already done so: 
 
* [Bash Test](https://centralesupelec.edunao.com/mod/quiz/view.php?id=167112)
* [Git Test](https://centralesupelec.edunao.com/mod/quiz/view.php?id=167113)


## Before you go
Before delving deeper in the projects, make sure you always add to your newly created repository a file called `.gitignore`. This file allows git to automatically know if there are files or directories that should never be tracked and added to the repo. By default, you can already add the following lines inside the .gitignore file you will create:

```
.DS_Store
__pycache__/
*.pyc
```

They correspond to files that may be automatically created, either by the OS or python itself when the code is run. Depending on the files you will create in your project folder, you may have to add new entries to this file.
 
## Week 1 project

### To start, click on one of the following links

:point_right: _[The 2048 game](./cs_codingweeks_game2048/TemplateProject_2048.md)_

:point_right: _[The game of life](./cs_codingweeks_game_of_life/TemplateProject_jeudelavie.md)_

:point_right: _[The pendulum simulation](./cs_codingweeks_pendulum/TemplateProject_pendulum.md)_

:point_right: _[The data visualization project](./cs_codingweeks_dataviz/TemplateProject_dataviz.md)_
