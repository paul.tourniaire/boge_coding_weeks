# Game of Life - Sprint 0: Design Considerations

After this analysis phase, we can start thinking about the initial design and try to identify the main objects of our MVP. 

By reading the [description of the Game of Life on Wikipedia](https://fr.wikipedia.org/wiki/Jeu_de_la_vie), we can identify several concepts such as **grid**, **universe**, **cell**, **state**, **seed**, and all the concepts related to different structures.

## Establishing a Common Language

In this phase of design thinking, to facilitate collaborative work and common understanding among all project members, it's important to define a **common vocabulary** around domain-specific terms from the outset of the project. In software development jargon, this is called [**ubiquitous language**](https://tanzu.vmware.com/developer/blog/ubiquitous-language/). It's a principle derived from the *Domain Driven Design* approach described in the book of the same name, which involves identifying and defining a shared language around domain-specific terms.

In the case of the Game of Life, this work may seem tedious and perhaps unnecessary, but it will be very useful for many other projects involving different stakeholders.

The approach involves producing what are called [**User Stories**](https://en.wikipedia.org/wiki/User_story) (with all project stakeholders) that represent the needs of users to be implemented. This work also helps in defining the shared language.

A **user story** is nothing more than a simple, natural language sentence that describes the content of a feature to be developed by specifying *Who?*, *What?*, and *Why?*

 `As a <who>, I want <what> in order to <why>`

For example:

+ As an application user, I want to be able to **configure the simulation** of the Game of Life.
+ As an application user, I want to be **notified** when a structure is recognized.
+ As a teacher, I want to be able to **generate, configure, and save animations** of instances of the Game of Life.
+ ...

Regarding the shared language, for the Game of Life, several terms are used to refer to the game's grid, such as *grid*, *universe*, *table*...

Therefore, it's necessary to choose a specific term for each object of the application.

For the rest of the project, we can choose, for example, the name **universe** to refer to the grid of the Game of Life, which we will define as a container of **cells**.

A **cell** is an element in the universe that can take a value between 0 and 1 depending on its **state**, dead or alive.

## Main Objects and Modules of Your Application

This analysis phase should also make you think about the design of your application and, in particular, the different objects and modules in it, trying to separate responsibilities.

To do this, try to think about the functional architecture of your application and how different actors, modules, and objects interact with each other. Use a pen and paper to do this in a schematic way. Group work and reflection can be interesting here!

This is the moment to use Microsoft Teams!

Take some time to do this work. Once completed, take a photo of what you put on paper and store it in a directory titled `WorkingDocs` in your Git repository, which you need to create. Don't forget to implement all the lessons you learned during the `git` tutorial.

The goal of this work is to allow you to model your problem from a computer science perspective, but it's also essential for organizing your work, i.e., how it's divided into different sprints and functionalities, for example.

Here, this work has been done for you, but you will need to do it for your project in Week 2.

#### :clap: <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 2: Analysis and Design of my Product.</span>

You can now proceed to [**Feature 1: Representation of the Universe.**](./jeudelavie_S1_Univers.md)