# Feature 4: Apply the Rules of the Game of Life to a Cell - the `survival` Function

The goal of this feature is to implement the rules of the Game of Life for a given cell.

This involves implementing the following steps:

+ Given the coordinates `x` and `y` of the `cell` in the `universe`
+ Examine the neighborhood of the cell
+ Decide whether the cell changes its state or not (i.e., whether it survives or dies).

You are free to choose how to implement this feature. We expect well-commented code to accomplish this feature, along with the associated tests. You can apply the TDD methodology or not as you prefer.

**Don't forget to update your local and remote repositories.**

You can now proceed to [**Feature 5**: Apply the Rules of the Game of Life to All Cells in the Universe for One Generation - the `generation` Function](./S2_generation.md).