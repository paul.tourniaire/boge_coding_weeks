# Feature 9: Parameter Handling with `argparse`

The goal here is to allow a given user to run the simulation program from the command line by specifying various simulation parameters as command-line arguments.

We will use the Python `argparse` module for this purpose, and you can find its documentation [here](https://docs.python.org/3/howto/argparse.html).

Quickly familiarize yourself with this module and use it to implement this functionality.

#### <span style="color: #26B260"> :clap: At this stage of the project, you have reached MILESTONE 7: Managing program parameters from the command line.</span>

We are nearing the end of our MVP. We just need to wrap it up with [**Feature 10**: Putting Everything in a Main Program](./S4_gamemain.md).