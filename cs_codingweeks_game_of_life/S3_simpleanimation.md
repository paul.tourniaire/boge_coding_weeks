# Feature 7: Initial Animation with Matplotlib

In this feature, we aim to visualize the simulation of our Game of Life. To display the universe, we've been using `matplotlib`, and now we'll see how we can generate animations with this library.

## Step 1: Learn about Matplotlib's Animation Features

So far, we have used the `matplotlib` library for displaying our universe. Now, we want to visualize an animation: the evolution of the universe according to the Game of Life rules.

Not sure how to do it? Start by checking if the tools at your disposal, specifically `matplotlib`, allow you to create animations. `matplotlib` has an animation utility, as described [here](https://matplotlib.org/stable/api/animation_api.html).

Your first task is to familiarize yourself with this utility. You can use the official documentation [here](https://matplotlib.org/stable/api/animation_api.html), which provides numerous examples, or search for other tutorials. There are plenty available:

- [Courspython Animation with Matplotlib](https://www.courspython.com/animation-matplotlib.html)
- [Matplotlib Animation Tutorial](https://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/)
- [Matplotlib Animations: The Easy Way](https://brushingupscience.com/2016/06/21/matplotlib-animations-the-easy-way/)

Take the time to read and test the possibilities of `matplotlib`.

## Step 2: Create a Function to Visualize Universe Evolution

Based on what you learned in the previous step, write a function that allows you to visualize the evolution of the Game of Life universe using `matplotlib` with a simple universe and a "beacon" seed.

## Step 3: Save Your Animation as a .gif File

You should end up with an animation like the one shown below:

![Animation](./Images/beacon.gif)

**Don't forget to update your local and remote repositories.** After this, you can proceed to [**Feature 8**: Visualization and Animation of the Game of Life](./S3_animategame.md).