# Feature 3: Configure a Set of Seeds

In this feature, we will allow the Game of Life to be initialized with a wide selection of seeds, which are the classical seeds of this game.

To get an idea of what these seeds might look like, you can check out websites like [this](http://conwaylife.appspot.com/library) or [this](http://conwaylife.com/wiki/Category:Patterns).

In the advanced features of this project, you could even populate a seed catalog by scraping the second website and inferring and generating the seed's shape from the information on the page or by analyzing an image of the seed (pattern).

We're not there yet, but keep this feature in mind (you can add it to a `to_do.md` file in your Git repository for future reference).

You are free to choose how to implement this feature. The code should be well-commented to accomplish this feature, and it should include the associated tests. You can apply the TDD methodology or not, as you prefer.

As a starting point, we suggest the following steps.

## Step 1: Define a Seed Catalog

In this step, define and add a catalog to your project, consisting of about a dozen seeds. For example, you can include seeds presented on the [Wikipedia page](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns):

+ [`acorn`](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#/media/File:Game_of_life_acorn.svg)
+ [`boat`](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#/media/File:Game_of_life_boat.svg)
+ ...

**<span style='color:blue'>To have the richest catalog possible, you can also divide the work among your team members and distribute different patterns to create. This is a good exercise for group programming and code sharing with Git. However, it's essential to have a common design step regarding how you will represent your catalog. </span>**

To help you move forward with the project, we provide the minimal catalog below, modeled as a Python dictionary (`dict`):

```python
seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}
```

## Step 2: Initialize the Game of Life Universe with the Option to Choose a Seed

In this step, set up the initialization of your Game of Life universe, taking into account the diversity of seed types available. Handle this step with care, as the size of your universe must meet certain constraints depending on the seed type. It's advisable to use exception handling to the fullest for the implementation of this step.

## Step 3: Update Your Repository

You've just completed a feature, so don't forget to apply the best practices presented in the Git tutorial.

In this case, this feature concludes a sprint, so you must **<span style='color:blue'>tag your commit</span>** and remember to run a code coverage check using `coverage`.

#### :clap: <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 6: A complete first software development sprint in a group.</span>

Now that we have our universe and its initialization, we can move on to simulating the actual Game of Life. This will be [**Feature 4: Applying the Rules of the Game of Life to a Cell - the `survival` function**](./S2_survival.md).