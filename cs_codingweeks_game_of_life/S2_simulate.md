# Feature 6: Simulate the Game of Life for a Given Universe Over a Set Number of Iterations - the `game_life_simulate` Function

The goal of this feature is to implement the Game of Life for a given universe and make it evolve over multiple iterations.

There are no major difficulties here, and you are free to choose how to implement this feature. We expect well-commented code to accomplish this feature, along with the associated tests. You can apply the TDD methodology or not as you prefer.

**Don't forget to update your local and remote repositories, and make sure to tag your last commit as this feature completes a sprint.**

We will now proceed to [**Feature 7**: A Simple Animation with Matplotlib](./S3_simpleanimation.md).