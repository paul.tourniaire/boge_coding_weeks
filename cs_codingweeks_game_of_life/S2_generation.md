# Feature 5: Apply the Rules of the Game of Life to All Cells in the Universe for One Generation - the `generation` Function

The goal of this feature is to implement the rules of the Game of Life for all cells in a given universe.

This is a very straightforward feature, and you are free to choose how to implement it. We expect well-commented code to accomplish this feature, along with the associated tests. You can apply the TDD methodology or not as you prefer.

**Don't forget to update your local and remote repositories.**

You can then proceed to [**Feature 6**: Simulate the Game of Life for a Given Universe Over a Set Number of Iterations - the `game_life_simulate` Function](./S2_simulate.md).