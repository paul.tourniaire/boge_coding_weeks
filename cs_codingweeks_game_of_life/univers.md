# Feature 12: Displaying the Game Grid in a Tkinter Window

In this feature, we will create a Tkinter interface to display Conway's Game of Life. Initially, we'll focus on setting up the `display_game_of_life_gui` module.

## Step 1: Creating the Tkinter Structure to Display the Game Universe

Step 1 involves displaying the initial game grid in Tkinter. In this step, we'll focus on grid visualization only.

To complete this step, you need to:

- Define the widget objects that will represent your grid.
- Set constants for grid display, including cell colors based on their values, value colors, and the size of the main window.

### Choosing the Representation of the Game Grid in Tkinter

For displaying our game grid with Tkinter, we will use the `Toplevel` widget, which allows you to create primary windows with independent existence in the operating system's window manager.

You can find a good explanation of this widget type [here](https://www.tutorialspoint.com/python/tk_toplevel.htm).

+ Create a main window with Tkinter, titled `gameoflife`, and create a `Toplevel` widget that you associate with your main window.
+ Place this widget using Tkinter's `grid()` placement function.

### Representing the Game Universe

Now we need to create and display the game universe itself.

+ Initialize the game universe.
+ Explore the [`Canvas` widget](https://tkdocs.com/tutorial/canvas.html) for displaying the game universe.

At this point, we have two objects:

+ `grid_game` containing the game data to display (the **MODEL**).
+ `graphical_grid`, our Tkinter data structure for the graphical interface (the **VIEW**).

This approach is very typical in GUI development. It follows a software architecture pattern known as [**Model-View-Controller (MVC)**](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller), which is dedicated to graphical user interfaces and dates back to 1978. It has become very popular and consists of three types of modules with different responsibilities: models, views, and controllers:

+ A **model** contains the data to be displayed.
+ A **view** contains the presentation of the graphical interface.
+ A **controller** contains the logic regarding user actions.

At this point, we have at least implemented the first two modules. However, we still need to connect the view to the model, and that's what we're going to do now.

### Displaying the Simulation

Now, it's time to display the evolution of your universe on your interface.

To do this, you need to configure your cells by updating the parameters of your different graphical objects with the values from the grid.

Before you write your code, it's necessary to think about the design. The key question you should ask is how many times this view update operation can occur in the game's execution. Once? Multiple times? At each game turn?

Depending on your answer, it might be useful to put the code related to this task in a function, for example, called `display_and_update_graphical_gameoflife()`, which you can call whenever needed.

#### :clap: <span style="color: #26B260">At this stage of the project, you've reached MILESTONE 12: Becoming Familiar with the MVC Design Pattern</span>

## Step 3: Simulate!

At this point in the project, you should be able to simulate Conway's Game of Life using your graphical interface. Take the time to do this and make sure to list any missing features.

We have now completed this feature, and you need to:

+ <span style='color:blue'>Commit your latest changes.</span>
+ <span style='color:blue'>Tag the last commit.</span>
+ <span style='color:blue'>Synchronize your work.</span>
+ <span style='color:blue'>Run a code coverage test on your project and push the results to your remote GitLab repository.</span>

Next, we can add graphical tools for configuring the game, which is part of [**Feature 13**: Allowing Game Configuration via the Graphical Interface](./config.md).