# Feature 13: Allowing Game Configuration via the Graphical Interface

In this feature, we will add various widgets to our main window (the empty white window) to enable:

+ Choosing the size of the universe.
+ Selecting a pattern.
+ Placing the pattern.

If you've completed this, you can now add other features of your choice to your game.

## AND TO FINISH

+ <span style='color:blue'>Commit your latest changes.</span>
+ <span style='color:blue'>Tag the last commit.</span>
+ <span style='color:blue'>Synchronize your work.</span>

You can now move on to [Objective 3: A Game of Life with Pygame](TemplateProject_jeudelavie.md).