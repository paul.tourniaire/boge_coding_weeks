# Feature 2: Display the Universe

The goal of the second feature is to be able to **display the Game of Life universe properly**, no matter how it looks.

As a reminder, for this MVP, we have chosen a simple display of the game without a graphical interface. Specifically, we will rely on the `matplotlib` library for this, which you have already seen in class.

Your visualization should look like the image below:

![Visualization](./Images/visu.png)

We leave it up to you to decide how to implement this feature, so **YOUR TURN TO PLAY!**

You've just completed a feature, so don't forget to:

- <span style='color:blue'>Commit your changes.</span>
- <span style='color:blue'>Conduct a code review and synchronize your code.</span>

Now that we have the basic components to generate, initialize, and display the Game of Life universe, we will enhance the initialization parameters to allow users to choose from a wide range of seeds. This will be part of the [**Feature 3: Configure a Set of Seeds**](./jeudelavie_S1_Amorces.md).