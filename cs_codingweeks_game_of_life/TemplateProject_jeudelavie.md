# Conway's Game of Life Programming

The **objective** of this mini-project is to develop Conway's Game of Life, a **cellular automaton**, in a highly incremental manner. The aim is to help you gain good software development practices and the culture of software quality. Through this project, you will explore several principles of the so-called [*Software Craftsmanship* paradigm](https://github.com/jnguyen095/clean-code/blob/master/Clean.Code.A.Handbook.of.Agile.Software.Craftsmanship.pdf).

## About Conway's Game of Life

[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) is a [cellular automaton](https://en.wikipedia.org/wiki/Cellular_automaton) conceived by [John Horton Conway](https://en.wikipedia.org/wiki/John_Horton_Conway) in 1970. It is probably the most well-known of all cellular automata, which are models where each state mechanically leads to the next state based on predefined rules.

If you've never heard of Conway's Game of Life, take the time to read the [Wikipedia page](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) describing it.

![Conway's Game of Life](./Images/Gospers_glider_gun.gif)

## Rules of the Game

The game takes place on a two-dimensional grid, theoretically infinite but typically finite in length and width in practice. The grid contains cells, which can be in one of two states: alive or dead.

At each step, the evolution of a cell is entirely determined by the state of its eight neighbors, following these rules:

- A dead cell with exactly three live neighbors becomes a live cell (it is born) (**reproduction**).
- A live cell with two or three live neighbors remains alive, otherwise, it dies (**stability, overpopulation, and underpopulation**).

## Structures

Various structures, consisting of multiple cells, can appear in the game:

- [Stable structures](https://en.wikipedia.org/wiki/Still_life_(cellular_automaton)): sets of cells that have stopped evolving.
- [Periodic structures or oscillators](https://en.wikipedia.org/wiki/Oscillator_(cellular_automaton)): they transform cyclically, taking on various forms before returning to their initial state.
- [Spaceships](https://en.wikipedia.org/wiki/Spaceship_(cellular_automaton)): structures that, after a certain number of generations, can produce a copy of themselves, but shifted in the game's universe.
- [Methuselahs](https://en.wikipedia.org/wiki/Methuselah_(cellular_automaton)): active structures that take some time to stabilize.
- Many others, such as puffers, guns, gardens of Eden, and more.

The goal of this project is to program Conway's Game of Life in Python, starting simply with scientific libraries like `numpy`, `scipy`, and `matplotlib`. Later, an interface and animation modules will be added.

The simulation will work on a torus, meaning there are no edges. In our grid, the first column is adjacent to the last column, and the first row is adjacent to the last row.

## Organization of the Mini-Project

This mini-project is divided into several objectives, further divided into **sprints** and **features**. The concept of a sprint refers to the [agile methodology](https://en.wikipedia.org/wiki/Agile_software_development), where a sprint is a time interval during which the project team completes a set of tasks.

This work of division has been done for you, but it's one of the first steps in any software development project, at least on a macro level. Consider this for next week!

### **Objective 1 (MVP): A Simple Game of Life Without a Graphical Interface**

The first objective is to build and implement a simple version of Conway's Game of Life, which can be considered an [MVP (Minimum Viable Product)](https://en.wikipedia.org/wiki/Minimum_viable_product).

The concept of MVP was popularized by Eric Ries, the author of [The Lean Startup](http://theleanstartup.com), which is a specific approach for starting a business and launching a product. The figure below explains this concept:

![MVP](./Images/mvp.png)

- **Sprint 0**:
  - [Installation of the technical foundation](./Sprint0Installbis.md)
  - [Needs analysis](./Sprint0Analyse.md)
  - [Design considerations](./Sprint0Conception.md)

- **Sprint 1: Setting Up the Game of Life Data**
  - [**Feature 1**: Representing the universe with a randomly positioned seed](./jeudelavie_S1_Univers.md)
  - [**Feature 2**: Displaying the universe](./jeudelavie_S1_Display.md)
  - [**Feature 3**: Configuring a set of triggers](./jeudelavie_S1_Amorces.md)

- **Sprint 2: Simulating the Game of Life**
  - [**Feature 4**: Applying the rules of the Game of Life to a cell - the `survival` function](./S2_survival.md)
  - [**Feature 5**: Applying the rules of the Game of Life to all cells in the universe for one generation - the `generation` function](./S2_generation.md)
  - [**Feature 6**: Simulating the Game of Life for a given universe over a fixed number of iterations - the `game_life_simulate` function](./S2_simulate.md)

- **Sprint 3: Display and Animation Generation with `matplotlib`**
  - [**Feature 7**: A first animation with `matplotlib`](./S3_simpleanimation.md)
  - [**Feature 8**: Visualization and animation of the Game of Life](./S3_animategame.md)

- **Sprint 4: Let's Play the Game of Life!**
  - [**Feature 9**: Parameter management with argparse](./S4_arguments.md)
  - [**Feature 10**: Putting everything into a main program](./S4_gamemain.md)

### Objective 2: A Game of Life with a Graphical Interface (Improvement of the MVP)

- **Sprint 5: Developing Skills in Python's Graphical Interfaces**
  - [**Feature 11**: Initial steps in Tkinter](S5_GUI_Tutorial.md)

- **Sprint 6: Creating the Interface for the Universe**
  - [**Feature 12**: Displaying the universe in a Tkinter window](./univers.md)
  - [**Feature 13**: Allowing configuration of the game through the graphical interface](./config.md)

### Objective 3: A Game of Life with Pygame

Here, the goal is to use the Pygame library to simulate the Game of Life. For this objective, we will follow a slightly different approach. We will look for an existing implementation of the Game of Life in Pygame. You can, for example, take a look at [this Game of Life implementation](https://gist.github.com/bennuttall/6952575).

You can also read the comments provided with this code. Try to understand it, and if it doesn't work right away, take the time to identify and fix the issues. 

Now, try to understand it. For that, you can refer to [this Pygame tutorial](https://zestedesavoir.com/tutoriels/846/pygame-pour-les-zesteurs).

You are now ready to write your own version of the Game of Life in Pygame. You will apply the same methodologies as for objectives 1 and 2. Before you start, you should break down your work into sprints. A document describing your breakdown should be submitted to your project's Gitlab space.

### Objective 4: Further Improvements

At this stage of the project, the objectives have been completely achieved, and it is appropriate to go through a design cycle after receiving user feedback. Several features could be added, such as:

<!-- - Automatic enrichment of the catalog through image analysis. If you are interested in this objective, you can follow the tutorial provided by the **Facial Recognition** group, which can be found [here](https://gitlab-cw7.centralesupelec.fr/celine.hudelot/cs_codingweek_facerecognition_2020/-/blob/master/Facerecognition_S1_displayimage.md). -->
- Adding new simulation rules.
- And more...

Again, your work will be done by applying the same methodologies as for objectives 1 and 2. Before you start, break down your work into sprints, and submit a document describing your breakdown to your project's Gitlab space.
