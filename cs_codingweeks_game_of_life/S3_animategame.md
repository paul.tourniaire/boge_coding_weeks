# Feature 8: Visualization and Animation of the Game of Life

The objective of this feature is to implement animation for any type of Game of Life universe, allowing animation parameterization.

In particular, the comments for your animation function should be somewhat similar to the following:

```python
"""
:param  tuple (int, int) universe_size: dimensions of the universe
:param seed: (list of lists, np.ndarray) initial starting array
:param seed_position: (tuple (int, int)) coordinates where the top-left corner of the seed array should be pinned
:param cmap: (str) the matplotlib cmap that should be used
:param n_generations: (int) number of universe iterations, defaults to 30
:param interval: (int) time interval between updates (milliseconds), defaults to 300ms
:param save: (bool) whether the animation should be saved, defaults to False
"""
```

Write the code to implement this functionality.

**Don't forget to update your local and remote repositories and tag your commit to mark the end of the sprint.** After that, you can move on to [**Feature 9**: Parameter Handling with argparse](./S4_arguments.md).