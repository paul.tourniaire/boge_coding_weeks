# Skill development: Python Graphical User Interfaces

There are several frameworks available for developing graphical user interfaces (GUIs) in Python. You can find a list of these tools [here](https://wiki.python.org/moin/GuiProgramming).

In particular, the Python standard library includes a module called **Tkinter (Tk interface)**, which allows you to create GUIs. For this part of the project, we'll stick to using this module as it doesn't require any additional installation.

You can find the documentation for this module [here](https://wiki.python.org/moin/TkInter).

Now, let's dive into a quick tutorial (inspired by OpenClassRooms' course [Learn Python Programming](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/234859-des-interfaces-graphiques-avec-tkinter) and this [tutorial](https://www.python-course.eu/python_tkinter.php)) to get you familiar with this module and tool.

Vincent Maillol's tutorial (in French) is also highly recommended, which can be found [here](https://vincent.developpez.com/cours-tutoriels/python/tkinter/apprendre-creer-interface-graphique-tkinter-python-3/).

<!-- Another well-documented resource in French for Tkinter is available [here](http://tkinter.fdex.eu/index.html). -->

## Creating Your First Tkinter GUI

Create a file named `gui_tutorial.py` in your project directory and copy the following code. It demonstrates how to create a window displaying the message "Hello World!"

```python
from tkinter import *

# Create a window, the root of the interface
window = Tk()

# Create a label (text line) with the text "Hello World!" and use the window as the first parameter
label_field = Label(window, text="Hello World!")

# Display the label
label_field.pack()

# Run the Tkinter loop, which ends when we close the window
window.mainloop()
```

Executing this code will display the following window:

![Tkinter Hello World](./Images/hello.png)

The `config()` method allows you to configure widgets. In the previous example, the following two lines are equivalent:

```python
label_field = Label(window, text="Hello World!")
label_field.config(text="Hello World!")
```

is equivalent to:

```python
label_field = Label(window, text="Hello World!")
```

`pack` is a geometry manager that handles the composition of widgets.

The placement of the widget is determined relative to its container using the `side` configuration option, with cardinal placement values (`Tkinter.TOP`, `Tkinter.LEFT`, `Tkinter.RIGHT`, `Tkinter.BOTTOM`). By default, a widget is attached at the top or below existing widgets.

If you want to occupy the maximum available space, you can use the `expand=YES` configuration option. The direction of the maximum occupation is specified using the `fill` option, which can be set to width (`Tkinter.X`), height (`Tkinter.Y`), or both (`Tkinter.BOTH`).

Tkinter offers other positioning managers like `grid`, which divides the container into a grid and places the widget in one of the cells, and `place`, which positions the widget precisely in pixels but can be tricky to use in practice.

## Exploring Key Tkinter Widgets

Tkinter provides a variety of widgets, which are graphical objects like buttons, text fields, checkboxes, progress bars, etc. We'll introduce you to some of the key widgets.

### Label

In the previous example, we used the `Label` widget to display text in the main window. The text displayed by a `Label` cannot be modified by the user.

### Button

`Button` widgets are elements that you can click on, and they can trigger actions or commands. Copy the following code into your test file and execute it:

```python
import tkinter as tk

def write_text():
    print("Hello CentraleSupelec")

root = tk.Tk()
frame = tk.Frame(root)
frame.pack()

button = tk.Button(frame,
                   text="QUIT",
                   activebackground="blue",
                   fg="red",
                   command=quit)
button.pack(side=tk.LEFT)
slogan = tk.Button(frame,
                   fg="blue",
                   text="Hello",
                   command=write_text)
slogan.pack(side=tk.LEFT)

root.mainloop()
```

Please note that for macOS users, colors may not be fully supported. You can find information on this issue [here](https://stackoverflow.com/questions/1529847/how-to-change-the-foreground-or-background-colour-of-a-tkinter-button-on-mac-os).

This example demonstrates how to initiate an action from a graphical interface. In this case, the `quit` command is associated with the first button to exit the window, and the command defined in the `write_text()` function is associated with the second button.

### Entry

The `Entry` widget is used for data input. It allows users to type in data. It requires a `StringVar` object as a parameter, which is used to retrieve the text entered. When the `StringVar` is updated, the input field is also updated.

Test the following code:

```python
from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial

def update_label(label, stringvar):
    text = stringvar.get()
    label.config(text=text)
    stringvar.set('thank you')

root = Tk()
text = StringVar(root)
label = Label(root, text='Your name')
entry_name = Entry(root, textvariable=text)
button = Button(root, text='click',
                command=partial(update_label, label, text))

label.grid(column=0, row=0)


entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)

root.mainloop()
```

The `Button` widget takes a text to display and a function to execute. The function doesn't take any parameters, so if you want to use an existing function, you need to wrap it using the `partial` module.

You can explore other Tkinter widgets like `Radiobuttons` and `Checkbuttons` using the tutorial available [here](https://www.python-course.eu/tkinter_radiobuttons.php).

## Containers

Containers are widgets designed to hold other widgets. In our examples, we've already used windows (Toplevel) with the statement `root = Tk()`. Among the containers, there is the `Frame` widget that allows you to group multiple widgets together, making their placement easier, as demonstrated in the following code:

```python
from tkinter import Tk, Label, Frame

root = Tk()
f1 = Frame(root, bd=1, relief='solid')
Label(f1, text='I am in Frame 1').grid(row=0, column=0)
Label(f1, text='Me too in Frame 1').grid(row=0, column=1)

f1.grid(row=0, column=0)
Label(root, text='I am in root').grid(row=1, column=0)
Label(root, text='Me too in root').grid(row=2, column=0)

root.mainloop()
```

## Events

You can capture events like key presses or mouse clicks to perform specific actions. To handle events, a widget must have focus. Generally, a widget gets focus when you click on it, and events can be processed by a function.

The following code, which you should test, displays "Hello" based on mouse clicks or key presses:

```python
from tkinter import *
from pprint import pformat

def print_hello(i):
    label.config(text="Hello")

root = Tk()
frame = Frame(root, bg='white', height=100, width=400)
entry = Entry(root)
label = Label(root)

frame.grid(row=0, column=0)
entry.grid(row=1, column=0, sticky='ew')
label.grid(row=2, column=0)

frame.bind('<ButtonPress>', print_hello)
entry.bind('<KeyPress>', print_hello)
root.mainloop()
```

Take a moment to read through this excellent [tutorial on Tkinter events](https://pythonfaqfr.readthedocs.io/en/latest/prog_even_tkinter.html).

Now you're ready to work on your Game of Life with a graphical user interface. Feel free to revisit the mentioned tutorials to deepen your knowledge of Tkinter.

With this foundation, we can move on to [**Feature 12**: Displaying the Universe in a Tkinter Window](./univers.md).