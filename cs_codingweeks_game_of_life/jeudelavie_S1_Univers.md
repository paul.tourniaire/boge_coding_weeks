# Feature 1: Representing the Universe with a Randomly Positioned Seed

The goal of this first feature is to be able to **represent the game of life universe**. Specifically, we aim to:

1. Prompt the user to input the size of the game.
2. Generate the associated universe containing only dead cells.

We will get acquainted with the **Test Driven Development (TDD)** approach, which involves specifying the expected behavior through a test before actually implementing it. The principle is to write a test first and then the simplest code that makes the test pass, satisfying the specified behavior. The code can be improved afterwards. The idea is to focus on functionality rather than the code.

We'll start step by step, and you will gradually gain autonomy in this approach.

## Creating a Universe

To create the universe, we need to identify the data that needs to be manipulated, i.e., the data required to represent this universe, which is a container of cells that can only be in two states. Then, we will choose the data structures to use in the computational sense.

1. **Acceptance Criteria**

   One of the first tasks here is to research and list all the criteria that will ensure the correct response to the needs that the "Create a Universe" feature is supposed to cover. These criteria are known as **acceptance criteria**. In this case, it's very simple; the acceptance criterion for "Create a Universe" is to **have a game of life universe with all dead cells**.

2. **Development in TDD Mode**

   **Test Driven Development (TDD)** is development driven by tests, so the first line of your program should be in a test file. In our case, we will use the [`pytest`](https://docs.pytest.org/en/latest/) module, which should be added to your project. The TDD principle is based on three complementary steps:

   - First step (**<span style='color:red'>RED</span>**): Write a test that fails.
   - Second step (**<span style='color:green'>GREEN</span>**): Write the simplest code that makes the test pass.
   - Third step (**REFACTOR**): Improve the source code.

We will apply this method to the functionality of creating the game of life universe.

### **<span style='color:red'>RED STEP</span>**

Our first test is to verify that, given a desired size, our game of life universe contains only dead cells. Here is the test code:

```python
from generate_universe import *
from pytest import *


def test_generate_universe():
    assert generate_universe((4, 4)) == [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
```

Please copy this code into a file named `test_generate_universe.py`. This test should fail since, at this stage of the project, there is no code for the function and even the `generate_universe` module does not exist. You will see an error when running the code: <span style='color:red'>`ImportError: cannot import name 'generate_universe'`</span>.

### **<span style='color:green'>GREEN STEP</span>**

We will now write the code that makes this test pass as quickly as possible. To do that, you should:

- Create a file named `generate_universe.py` in your project.
- Create and complete a function called `generate_universe` in the `generate_universe.py` file. This function should be designed to make the above test pass.

You can define the function as follows in the `generate_universe.py` file:

```python
def generate_universe(size):
    s = []
    for i in range(0, size[0]):
        p = []
        for j in range(0, size[1]):
            p.append(0)
        s.append(p)
    return s
```

Your test should pass after implementing this code during the **<span style='color:green'>GREEN STEP</span>**.

### **<span style='color:black'>REFACTOR STEP</span>**

The last step is a [refactoring](https://refactoring.com) step, to be executed if necessary.

[Refactoring](https://en.wikipedia.org/wiki/Code_refactoring) is a programming principle that involves changing the internal structure of software without altering its observable behavior. This step should always be carried out when all the tests are passing, but it is not obligatory. It should aim to improve the **code quality**, including:

- **Design**: Breakdown into functions, modules, or classes to simplify the code.
- **Code readability**: Applying the principles of [clean code](https://cleancoders.com/) introduced by Robert C. Martin in his book of the same name. One of the principles is the Boy Scout Rule: *Always leave the campground cleaner than you found it*.

In this step, **<span style='color:black'>REFACTOR</span>**, you can also work on optimizing the program's performance if deemed necessary.

In our case, you can begin by verifying variable naming (variables, functions, packages, classes) and checking for the presence of comments in your code.

You can find some [Python clean code principles here](https://github.com/zedr/clean-code-python#objects-and-data-structures). Take some time to quickly read this resource and apply these principles to the code you will be writing.

In this step, **<span style='color:black'>REFACTOR</span>**, we can also consider changing the data structure used to represent our universe. We will use the `numpy` array data structure to represent the universe since we will perform numerical calculations on this universe.

Let's add the `numpy` library to your project using the command `pip3 install numpy` or `pip install numpy`, and modify the code (along with the associated tests) to represent the universe with a `numpy` array.

**Please complete this refactoring work**.

### **ATTENTION**

1. **After completing this step, don't forget to rerun the tests to ensure that the behavior of your code has not changed and everything is still GREEN.**

2. At this point, we have completed the implementation of "Create the Universe". It's time to **commit this change to your version control system with a clear commit message that reflects the goal of this step**.

### YOUR TURN!

You will now proceed in iterations to complete the implementation of Feature 1: *Representing the Universe with a Randomly Positioned Seed*, whose acceptance criterion is: **have a universe with a seed (e.g., the `"r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]]` seed) randomly placed within it.**

Each iteration begins with a test that fails but introduces new behavior to the system. At the end of each iteration, you should think about the next test to write. In our case, we haven't yet dealt with adding the seed to the universe, so the next test can be about creating the seed and adding it to the universe.

#### Create a "r_pentomino" Seed and Position It Randomly in the Universe

1. **Acceptance Criteria**

   - A "r_pentomino" seed is created.
   - This seed is placed randomly within the game of life universe.

2. **Development in TDD Mode**

   Applying the Test Driven Development (TDD) principle as described earlier, you will write the code to complete the implementation of this feature. It's recommended to iterate on the following tests for this feature. These tests correspond to each iteration in the **RED STEP**. You should follow the full TDD cycle to make these tests pass and improve the quality of your code.

   **Iteration 1 - Test: Create a "r_pentomino" Seed**

   ```python
   def test_create_seed():
       seed = create_seed(type_seed="r_pentomino")
       assert seed == [[0, 1, 1], [1, 1, 0], [0, 1, 0]]
   ```

   **Iteration 2 - Test: Place the Seed Randomly in the Game Grid**

   ```python
   def test_add_seed_to_universe():
       seed = create_seed(type_seed="r_pentomino")
       universe = generate_universe(size=(6, 6))
       universe = add_seed_to_universe(seed, universe, x_start=1, y_start=1)
       test_equality = np.array(
            universe == np.array(
                [[0, 0, 0, 0, 0, 0],
                [0, 0, 1, 1, 0, 0],
                [0, 1, 1, 0, 0, 0],
                [0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0]],
                dtype=np.uint8
            )
        )
       assert test_equality.all()
   ```

At the end of these two iterations, you should have implemented the entire code necessary for completing Feature 1: **Representing the Universe with a Randomly Positioned Seed**.

#### :clap: <span style="color: #26B260">At this point in the project, you've reached MILESTONE 3: Writing Code with a TDD Approach</span>

**<span style='color:blue'>Now, commit your changes to your version control system as soon as a feature or sub-feature is completed. Also, don't forget to perform code reviews within your team for each feature, push stable code to the `main` branch, and push the code to your remote GitLab repository.</span>**

#### :clap: <span style="color: #26B260">At this point in the project, you've reached MILESTONE 4: A First Real Use of Git and GitLab with My Project Group</span>

## About Code Coverage by Your Tests

Code coverage allows us to know the percentage of our code that is tested. It gives us an idea of what parts of our project remain untested.

In general, a code coverage of more than 80% is considered a sign of a well-tested project, making it easier to add new features.

To calculate your project's code coverage, you can use the Python libraries [`coverage`](https://coverage.readthedocs.io) and [`pytest-cov`](https://pypi.org/project/pytest-cov/). You should install these libraries either via the command line or from your IDE.

Install `coverage` by running:

```bash
pip3 install coverage
```

or

```bash
pip install coverage
```

Install `pytest-cov` by running:

```bash
pip3 install pytest-cov
```

or

```bash
pip install pytest-cov
```

Then, navigate to your project directory and run the following command:

```bash
pytest --cov=game_of_life --cov-report html test_*.py
```

This command tests the files in the `game_of_life` directory, generates an HTML report, and stores it in the `htmlcov` directory. It uses tests located in that directory and having filenames in the format `test_[characters].py`.

To check your code coverage, open the `index.html` file in the `htmlcov` directory. This report provides a summary of code coverage, which should be good if you've followed the TDD approach. Clicking on each file allows you to view coverage information specific to that file.

![testcoverage](./Images/coverage.png)

![testcoverage](./Images/coveragebis.png)

#### :clap: <span style="color: #26B260">At this point in the project, you've reached MILESTONE 5: Achieving Code Coverage for My Project</span>

## About Version Management

<span style='color:blue'>For the remainder of the project, you are required to:</span>

- <span style='color:blue'>Commit as soon as the implementation of a feature or sub-feature is completed.</span>
- <span style='color:blue'>Tag your last commit at the end of each day.</span>
- <span style='color:blue'>Conduct a code review with your team for each feature.</span>
- <span style='color:blue'>Push the stable code to the `main` branch.</span>
- <span style='color:blue'>Push the code to your remote GitLab repository.</span>

You can now move on to [**Feature 2: Display the Universe**](./jeudelavie_S1_Display.md).