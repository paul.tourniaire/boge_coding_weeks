# Game of Life - Sprint 0: Needs Analysis

One of the first steps in any programming and software development work, regardless of the development methodology used, is to perform a quick **needs analysis** before any implementation phase.

The purpose of this analysis is to identify the main functionalities to be developed to achieve the desired behavior of the developed system. This initial list of functionalities does not need to be exhaustive or set in stone, but it will help you build your initial developments.

## Needs Analysis: Key Features

The goal here is to allow a user to test and understand the game of life through simulations. It will involve designing software that enables a user to interact with a simulation of the game of life, initially in console mode, without a graphical interface.

The **[MVP (Minimum Viable Product)](https://en.wikipedia.org/wiki/Minimum_viable_product)** of this project will be to deliver a first version of the game of life with console-based interaction. Your solution:

+ **Will allow a user to choose the various parameters of the game: the size of the universe, the seed, the number of iterations, etc.**
+ **Will enable the simulation of the game of life based on the parameters entered by the user.**
+ **Will display the simulation as an animation using the `matplotlib` animation module.**
+ **Will highlight different known structures of the game of life.**

Other features can certainly be added later to enhance the game after completing this MVP. By allowing a quick prototype of the game of life, you can quickly get feedback from your users. Imagine that your users are students who want to understand the game of life. They can quickly provide you with feedback on the missing features needed to achieve this goal.

You can now continue with [Sprint 0: Design Considerations](./Sprint0Conception.md).