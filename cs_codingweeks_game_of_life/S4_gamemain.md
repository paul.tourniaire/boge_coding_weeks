# Feature 10: Putting Everything into a Main Program

The next step is to put everything we have done into a main program.

To do this, you need to add a main function like this:

```python
if __name__ == '__main__':
    # Your main program logic here
```

You can find some documentation on this topic [here](https://docs.python.org/3/library/__main__.html) and [here](https://www.guru99.com/learn-python-main-function-with-examples-understand-main.html).

For more general information, you can refer to the [RealPython](https://realpython.com/) website, which has excellent tutorials, including one on the [main function](https://realpython.com/python-main-function/).

#### :clap: <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 8: Orchestrating a project with a main function.</span> 

At this point, we have completed our goal 1, and we now have our MVP for the Game of Life.

Before moving on and updating your Git repository, we ask you to take the time to add as many unit tests as possible to your project (if you applied the TDD methodology, this should not take too long), and to comment your various functions.

#### :clap: <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 9: Consolidating developments, as well as MILESTONE 10: Designing and implementing an MVP.</span>

## User Feedback on Your Game of Life

At this stage, we will also try to get user feedback on this MVP. You will take on this role and test your Game of Life by "playing" with it.

What are your user feedback and thoughts on this MVP? Are there missing features you would like to add to your project?

Take the time to discuss within your group to list the features you would like to add to your Game of Life. Also, give them priorities.

Following this brainstorming session, add or update a `TO_DO.md` (or `TO_DO.md`) file to your project, listing these features in order of priority.

And if you still have a lot of time, you can start working on some of these features!

## To Wrap Up

+ <span style='color:blue'>Commit your latest changes.</span> 
+ <span style='color:blue'>Tag this last commit.</span> 
+ <span style='color:blue'>Synchronize your work.</span> 
+ <span style='color:blue'>Run a code coverage test for your MVP and push the results to your remote GitLab repository.</span>

Now you can move on to [objective 2](./TemplateProject_jeudelavie.md#objective-2-a-game-of-life-with-a-graphical-interface-improvement-of-the-mvp).