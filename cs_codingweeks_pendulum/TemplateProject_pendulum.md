# Physics simulation: The pendulum, and other illustrative cases

The **objective** of this mini-project is to develop, in a very incremental way, the simulation of a pendulum and possible other physical systems and the animation of this simulation in order to train you in good programming practices and the culture of software quality. In particular, through this project, you will discover several principles of the so-called [*Software Craftsmanship* paradigm](https://github.com/jnguyen095/clean-code/blob/master/Clean.Code.A.Handbook.of.Agile.Software.Craftsmanship.pdf).


## On numerical modeling and physical simulation

Numerical modeling is the transcription of a physical phenomenon into computer language. It consists in transforming a physical reality into abstract models, as close as possible to the reality of the system, accessible to analysis and calculation.  

Numerical simulation is the process that enables to calculate the solutions to these models on a computer, thus enabling the reproduction of the physical reality.

The aim of this project is not to teach you modeling and numerical simulation, but to enable you to acquire programming and software development skills through an end-to-end programming project, so you'll have to be willing to use approaches without necessarily mastering them completely. The aim is also to get familiar with some of the Python libraries that are very useful in this context:


+ The [scipy](https://www.scipy.org/) library, which is a set of software tools to facilitate scientific programming and includes several packages including:
	+ [Numpy](https://numpy.org/), for numerical calculation;
	+ [Matplotlib](https://matplotlib.org/), for creating drawings and animations;
	+ [Sympy](https://www.sympy.org/en/index.html), for symbolic calculation;
	+ ...
+ the [pymunk](http://www.pymunk.org/en/latest/) library, an easy-to-use 2d physics engine built on top of the [Chipmunk](https://chipmunk-physics.net/) 2d physics library.
+ and others such as [PyODE](http://pyode.sourceforge.net/).
	
In terms of the purpose of our application, a good example of an objective is [this type of application](https://www.myphysicslab.com/pendulum/pendulum-en.html):

<img src="./Images/applipendule.png" alt="drawing" width="600"/>


## The pendulum problem

In physics, the simple pendulum is a point mass attached to the end of a weightless, inextensible and stiff wire, which oscillates under the effect of gravity. This is the simplest model of a weighing pendulum. 

We therefore consider the following conditions to be the state of the system:

+ One end is fixed;
+ The other end is free;
+ It is assumed that the string is always fully taut;
+ No friction is assumed.
+ To model the problem, we need to know $\theta(t)$, the angle of the pendulum at time $t$.


### The pendulum movement equation

As the aim is not to teach modelling, the equation of motion of the pendulum is recalled here.

<img src="./Images/Pendulum.gif" alt="drawing" width="150"/>


A simple pendulum is a mass $m$ attached to the end $P$ of a wire of length $L$. When moved through a fixed angle and released, the pendulum will oscillate periodically. Applying Newton's second law, we obtain the equation of motion of the pendulum below:

$$F = ma \Rightarrow -mg\sin\theta L = mL^{2}\frac{d^2\theta}{dt}$$


<!-- <img src="./Images/CodeCogsEqn.gif" alt="drawing" width="300"/> -->



This is because:

+ the velocity vector of the ball is always perpendicular to the string;
+ Consequently, the component of the force of gravity acting perpendicular to the string is given by:

 $$F = -mg\sin\theta L = ma \Rightarrow a = -g\sin\theta$$

<!-- <a href="https://www.codecogs.com/eqnedit.php?latex=F&space;=&space;-mg&space;\sin\theta&space;=&space;ma&space;\Rightarrow&space;a&space;=&space;-g&space;\sin\theta" target="_blank"><img src="https://latex.codecogs.com/gif.latex?F&space;=&space;-mg&space;\sin\theta&space;=&space;ma&space;\Rightarrow&space;a&space;=&space;-g&space;\sin\theta" title="F = -mg \sin\theta = ma \Rightarrow a = -g \sin\theta" /></a> -->

+ The angular acceleration is given by:

$$a = L\frac{d^2\theta}{dt}$$

<!-- <a href="https://www.codecogs.com/eqnedit.php?latex=a&space;=&space;L&space;\frac{d^2\theta}{dt^2}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?a&space;=&space;L&space;\frac{d^2\theta}{dt^2}" title="a = L \frac{d^2\theta}{dt^2}" /></a> -->


Combining the two, we obtain:

$$\frac{d^2\theta}{dt} + \frac{g}{L}\sin\theta = 0$$

<!-- <a href="https://www.codecogs.com/eqnedit.php?latex=\frac{d^2\theta}{dt^2}&space;&plus;&space;\frac{g}{L}\sin\theta&space;=&space;0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{d^2\theta}{dt^2}&space;&plus;&space;\frac{g}{L}\sin\theta&space;=&space;0" title="\frac{d^2\theta}{dt^2} + \frac{g}{L}\sin\theta = 0" /></a> -->

This equation is difficult to solve exactly, but if the amplitude of angular displacement is small, then we have the approximation $\sin\theta \approx \theta$ which gives a much more simple differential equation:

$$\frac{d^2\theta}{dt} + \frac{g}{L}\theta = 0$$
<!-- <a href="https://www.codecogs.com/eqnedit.php?latex=\sin\theta\approx\theta" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\sin\theta\approx\theta" title="\sin\theta\approx\theta" /></a> -->

<!-- <a href="https://www.codecogs.com/eqnedit.php?latex=\frac{d^2\theta}{dt^2}&space;&plus;&space;\frac{g}{L}\theta&space;=&space;0" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\frac{d^2\theta}{dt^2}&space;&plus;&space;\frac{g}{L}\theta&space;=&space;0" title="\frac{d^2\theta}{dt^2} + \frac{g}{L}\theta = 0" /></a> -->

A simple harmonic solution is $\theta(t)=\theta_{0}\cos(\omega t)$ where $\theta_{0}$ is the original angular displacement and $\omega = \sqrt{g/L}$ is the frequency of the pendulum. Thus, the period is given by:

$$T = \frac{2\pi}{\omega} = 2\pi\sqrt{\frac{L}{g}}$$
<!-- <a href="https://www.codecogs.com/eqnedit.php?latex=\theta(t)&space;=&space;\theta_o&space;\cos(\omega&space;t)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\theta(t)&space;=&space;\theta_o&space;\cos(\omega&space;t)" title="\theta(t) = \theta_o \cos(\omega t)" /></a>  -->
<!-- avec <a href="https://www.codecogs.com/eqnedit.php?latex=\theta_o" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\theta_o" title="\theta_o" /></a> le déplacement angulaire initial, et <a href="https://www.codecogs.com/eqnedit.php?latex=\omega&space;=&space;\sqrt{g/L}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\omega&space;=&space;\sqrt{g/L}" title="\omega = \sqrt{g/L}" /></a>  la fréquence du mouvement.  La période est :


<a href="https://www.codecogs.com/eqnedit.php?latex=T&space;=&space;\frac{2\pi}{\omega}&space;=&space;2\pi\sqrt{\frac{L}{g}}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?T&space;=&space;\frac{2\pi}{\omega}&space;=&space;2\pi\sqrt{\frac{L}{g}}" title="T = \frac{2\pi}{\omega} = 2\pi\sqrt{\frac{L}{g}}" /></a> -->


When the approximation $\sin\theta \approx \theta$ is not valid, the differential equation must be numerically integrated to obtain the evolution of the pendulum's position and angular velocity over time.
<!-- <a href="https://www.codecogs.com/eqnedit.php?latex=\sin\theta\approx\theta" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\sin\theta\approx\theta" title="\sin\theta\approx\theta" /></a>  -->

This requires a differential equation integrator, which we'll find in `scipy`.


We'll now turn our attention to pendulum simulation and visualization.

## Mini-project organization

This mini-project is divided into several objectives, further divided into **sprints** and **features**. The concept of a sprint refers to the [agile methodology](https://en.wikipedia.org/wiki/Agile_software_development), where a sprint is a time interval during which the project team completes a set of tasks.

This work of division has been done for you, but it's one of the first steps in any software development project, at least on a macro level. Consider this for next week!

### **Objective 1 (MVP): A simple pendulum with `matplotlib`** 

The first objective of this week is to build and implement the simulation and visualization of the movement of a simple pendulum with `matplotlib` and this is what will constitute our **[MVP (Minimum Viable product)](https://www.youtube.com/watch?v=joNKkWPafZs)**. 


This MVP concept was popularized by Eric Ries, author of [The Lean Startup](http://theleanstartup.com/), a specific approach to starting up a business and launching a product. The figure below explains the concept.


![MVP](./Images/mvp.png)

+ **Sprint 0**:
	+ [Technical base installation](./Sprint0Installbis.md)
	+ [Requirements analysis](./Sprint0Analyse.md)
	+ [Design review](./Sprint0Conception.md)

+ **Sprint 1 : Set up the pendulum model**
	+ [**Feature 1**: Pendulum representation](./pendulum_S1_pendule.md)
	+ [**Feature 2**: Pendulum display](pendule_S1_visualisation.md)
 	 		
+ **Sprint 2** : **Moving the pendulum**
	+ [**Feature 3**: Simulating a the movement of a pendulum](./S2_pendule_motion.md) 
	+ [**Feature 4**: A first animation with `matplotlib`](./S2_simpleanimation.md)
	+ [**Feature 5**: Speed and position visualization as a function of the time](./S2_motionvisualisation.md)

+ **Sprint 3** : **Parameterize and launch the simulation from the command line**
	+ [**Feature 6**: The `argparse` module](./S3_argparse.md) 
	+ [**Feature 7**: A main program](./S4_gamemain.md) 
	+ [**Feature 8**: A command line pendulum](./S3_pendulum_mvp.md) 



### **Objective 2: A pendulum with a graphical interface** 

+ **Sprint 4**: **Skill enhancement: graphical interfaces**

	+ [**Feature 9**: First steps with Tkinter](S4_GUI_Tutorial.md)

+ **Sprint 5**: **A graphical interface for the pendulum**

	+ [**Feature 10**: A graphical interface for the pendulum: model](S4_GUI_pendulum_mock.md)
	+ [**Feature 11**: A graphical interface for the pendulum](S4_GUI_pendulum.md)
	+ [**Feature 12**: Allow pendulum configuration from the graphical interface](config.md)

 	
### **Objective 3: A pendulum with `pymunk`** 	
[`pymunk`](http://www.pymunk.org/en/latest/) is an easy-to-use 2d physics engine built on top of the [Chipmunk](https://chipmunk-physics.net/) 2d physics library. It's very easy to use and think about it whenever you need 2d physics of rigid bodies in Python.


The first thing to do is to install this library:

```bash
pip install pymunk
```
or
```bash
pip3 install pymunk
```

If you are using anaconda, use `conda` instead of pip. 

To meet the objective, you will have to get familiar with `pymunk`, following [this](http://www.pymunk.org/en/latest/tutorials/SlideAndPinJoint.html) tutorial for instance.


After following this tutorial, an interesting step might be to see if `pymunk` already offers a pendulum simulation example. This is the case [here](https://pymunk-tutorial.readthedocs.io/en/latest/intro/intro.html#pin-joint).


You are now ready to write your own simulation of the pendulum with `pymunk`. Your turn to play! Of course, you'll need to apply the same methodologies as for Objectives 1 and 2. Before getting started, you'll need to divide your work into sprints. A document describing your breakdown should be uploaded to your project's Gitlab space.


### **Objective 4: Improvements, other models and simulations**

At this stage of the project, the objectives have been fully achieved, and it's time to go through the design cycle again after getting some user feedback. 

Several features may be added to your project, or you may want to extend it by adding other features, for example.

It's up to you to decide. Here too, your work will be carried out using the same methodologies as for Objective 1 and Objective 2. Before getting started, you'll need to divide your work into sprints. A document describing your breakdown should be uploaded to your project's Gitlab space.







