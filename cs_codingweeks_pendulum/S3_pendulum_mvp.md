# Feature 8: A Command-Line Pendulum

At this stage, you are almost done with your MVP. Here, you need to:

+ Complete and modify the `launch_simulation_pendulum.py` module so that a given user can configure the simulation to the maximum extent, not just by setting the pendulum's length.

Before moving on and updating your Git repository, we ask you to take the time to add as many unit tests as possible to your project (if you have followed the TDD methodology, this should not take too long), and comment on your various functions.

#### <span style="color: #26B260">At this stage of the project, you have achieved:</span>
+ <span style="color: #26B260">:clap: MILESTONE 9: Consolidate Developments.</span>
+ <span style="color: #26B260">:clap: MILESTONE 10: Design and Implement an MVP.</span>

## User Feedback on Your Simulation

At this stage, we will also try to gather user feedback on this MVP. You will take on this role and test your simulation by using it.

What are your user feedback on this MVP? Are there missing features that you would like to add to your project?

Take the time to discuss within the group to list the features you would like to add to your game. Give them a priority.

Following this brainstorming, you will add or update a `TO_DO.md` (or `TO_DO.md`) file in your project, listing these different features in order of priority.

And if you still have a lot of time, you can start implementing some of these features before the end of the week!

#### <span style="color: #26B260"> :clap: At this stage of the project, you have achieved MILESTONE 11: Obtain and Analyze Initial User Feedback with a First MVP.</span>

## To Conclude

+ <span style='color:blue'>Make a commit of your latest changes.</span> 
+ <span style='color:blue'>Tag this last commit.</span> 
+ <span style='color:blue'>Push your code to your remote repository on GitLab.</span> 
+ <span style='color:blue'>Run a code coverage test for your MVP and push the result to your remote repository on GitLab.</span>

You can now proceed to [**Objective 2**](./S4_GUI_Tutorial.md).