# Feature 11: A Graphical Interface for the Pendulum

In this feature, you need to implement your GUI for the pendulum simulation, similar to the one illustrated below.

![pendulum](./Images/pendule.png)

At this stage of the project, you have the freedom to choose how to set up this interface.

You will need to:

- Create a canvas.
- Draw the pendulum bob and pivot on this canvas.
- ...

<!-- For inspiration, you can refer to the [proposed layout](https://gitlab-cw5.centralesupelec.fr/celine.hudelot/cs_codingweek_game_of_life_2020/-/blob/master/univers.md#fonctionnalit%C3%A9-8-affichage-de-la-grille-de-jeu-dans-une-fen%C3%AAtre-tkinter) for another week 1 coding weeks project, simulating the Game of Life. -->

Complete all the necessary steps, which should now become routine at the end of a feature, and then move on to [**Feature 12**: Allow Configuration of the Pendulum via the Graphical Interface](config.md).