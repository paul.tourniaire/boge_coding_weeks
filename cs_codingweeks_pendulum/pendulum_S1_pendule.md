# Feature 1: The pendulum model


The aim of this first function is to be able to **model the main object of the simulation**, i.e. the **pendulum** itself. 

This involves :

+ Ask the user to choose the parameters of the pendulum: its mass, the length of the rod,....
+ Write a program to generate a model of the desired pendulum.



Here, we're going to familiarize ourselves with the [**TDD (Test Driven Development)**](https://fr.wikipedia.org/wiki/Test_driven_development) development approach, which consists in specifying the expected behavior via a test before actually implementing it. The principle is therefore to first write the test and then the simplest possible code that allows the test to pass and thus satisfy the specified behavior. The code can then be improved. The idea is to focus on functionality rather than code.

We'll start with a step-by-step approach, and you'll gradually gain autonomy in this area. 


## Create a pendulum

To create the pendulum, we need to identify the data to be manipulated, i.e. the data required to represent the pendulum object. The next step is to select the data structures to be used.

1. **Acceptance criteria.**

One of the first tasks here is to research and list all the criteria that will enable us to correctly meet the needs that the *Create a pendulum* functionality is supposed to cover. These criteria are called **acceptance criteria**. Here, it's very simple: the acceptance criterion for *Create a pendulum* is to **have a computer representation of a pendulum** enabling us to act on it, i.e. an object composed of an end, a mass, a rod defined by its length, and an initial orientation.
  
 
2. **TDD**

By TDD **(Test Driven Development)**, we mean that the first line of your program must be in a test file. In our case, we'll be using the [`pytest`](https://docs.pytest.org/en/latest/) module, which must be added to your project. The principle of TDD is based on 3 complementary steps.
 
+ First step (**<span style='color:red'>RED</span>**): Write a test that fails.
+ Second step (**<span style='color:green'>GREEN</span>**): Write the most simple code that secures a success to the test.
+ Third step (**REFACTOR**): Improve the source code.


We will apply this method to pendulum creation feature.

#### **<span style='color:red'>RED STEP</span>**

Our first test will be to ensure that, given a given mass, size and orientation, our pendulum has been built correctly.

```python
from create_pendulum import *
from pytest import *

def test_create_pendulum():
    assert create_pendulum(10,10,10) == (10,10,10)
```

Copy this code into a `test_create_pendulum.py` file. This test should fail, since in the current state of the project, the code for the function and even the `create_pendulum` module does not exist, and we therefore get the following error when executing the code <span style='color:red'>`ImportError: cannot import name 'create_pendulum'`<span>


#### **<span style='color:green'>GREEN STEP</span>**

We're now going to write the code to make this test pass as quickly as possible.

All we need to do is :

+ Create the `create_pendulum.py` file in the project.
+ Create and complete a `create_pendulum` function in the `create_pendulum.py` file so that the previous test passes.

For example, you could define the following function in the `create_pendulum.py` file
 
```python
def create_pendulum(a,b,c):
    a = a
    b= b
    c = c
    return (a,b,c)
    
```
 
Your test should turn green after this **<span style='color:green'>GREEN STEP</span>**

<img src="./Images/pytestpendulum.png" alt="drawing" width="600"/>
 

#### **<span style='color:black'>REFACTOR STEP</span>**
   
The last step is a [refactoring](https://refactoring.com/) one, to put in place if necessary.

[Refactoring](https://en.wikipedia.org/wiki/Code_refactoring) (or _code_ refactoring) is a programming principle that consists in changing the internal structure of a program without changing its observable behavior. This step should always be carried out when the various tests are green, but is not mandatory. Its main purpose is to improve the **quality of the code**, for example by improving:
 
+ **the design**: break down your code into functions, modules and classes to make it as simple as possible.
+ **code readability**: here, you need to take the time to apply the principles of [clean code](https://cleancoders.com/) introduced by Robert C. Martin in his book of the same name, one of which is that of the Boy Scouts (*"The Boy Scout Rule "*): *Always leave a place in better condition than you found it "*.
 
In our case, one of the first principles is to check the correct naming (variables, functions, packages, classes and so on) and the presence of comments in our code. 
 
You'll find [here](https://github.com/zedr/clean-code-python#objects-and-data-structures) some principles of clean code transposed to the Python language. Take the time to read this site quickly and apply these principles to the code you're about to write.
 
In this **<span style='color:black'>REFACTOR STEP</span>**, we can also work on optimizing program performance if really necessary.
 
 
For instance, in our case, we can proceed to rename the variable `a`. Indeed, it could be more explicit to name it `mass`. Likewise, `b` could be renamed `length` and `c` `initial_orientation`. To carry out this renaming step, we strongly advise you to use your IDE's Refactoring functions. It may not seem very useful here, but with more complex code, it can be very useful.


This also raises the question of the choice of data structure used to represent the pendulum. We want to be able to easily access the various properties of a pendulum, and we'd also like to be able to evolve this representation. 
In object-oriented programming, one approach might have been to represent the pendulum by a class. To avoid having to use object-oriented programming, we'll represent a pendulum here as a dictionary.

So modify your code (and associated tests) to represent a pendulum as a python dictionary (`dict`).

```python
def create_pendulum(mass,length,orientation):
    pendulum= {}
    pendulum["mass"] = mass
    pendulum["length"] = length
    pendulum["initial_orientation"] = orientation
    return pendulum
    
```


**Complete this refactoring step** and you will be done with feature 1.


#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 3: Write code in a TDD fashion </span> 


**<span style='color:blue'>Commit your changes using git. It is also time to share and review your code with the other members of the team.</span>**


#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 4: a first real use of git and gitlab with my group</span> 


## On code coverage

Code coverage corresponds to the percentage of code that is covered by tests. Therefore, it lets us know what remains in the shadows in our project.

As a rule of thumb, we consider that a code coverage of over 80% is a sign of a well-tested project, to which it will then be easier to add new features.

To calculate your project's code coverage, you can use the Python libraries [`coverage`](https://coverage.readthedocs.io) and [`pytest-cov`](https://pypi.org/project/pytest-cov/). You should install these libraries either via the command line or from your IDE.

Install `coverage` by running:

```bash
pip3 install coverage
```

or

```bash
pip install coverage
```

Install `pytest-cov` by running:

```bash
pip3 install pytest-cov
```

or

```bash
pip install pytest-cov
```

Then, you need to go in the project working directory and run::

```bash
pytest --cov=pendulum --cov-report html test_*.py
```

This command tests the files contained in the `pendulum` folder. It creates an html report and places it in the `htmlcov` directory, using the tests in this directory of the form `test_<foo>.py`.

When you open the `index.html` file in the `htmlcov` directory, you'll see a summary of the coverage test, which should be good given the TDD approach we've used. Clicking on each of the files will also display a report for that particular file.

The screenshots below show an example of what we obtain on a different project.

![testcoverage](./Images/coverage.png)



![testcoverage](./Images/coveragebis.png)


#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 5: a first code coverage of my project </span> 


## On version control

<span style='color:blue'> For the remainder of the project, you are asked to:</span> 

+ <span style='color:blue'>Commit as soon as a feature or a sub-feature is implemented.</span> 
+ <span style='color:blue'>Tag your last commit at the end of the day </span> 
+ <span style='color:blue'>Review you code as a team for each feature.</span>
+ <span style='color:blue'>Put your code in the stable `main` branch.</span>
+ <span style='color:blue'>Push your code on your remote repo on GitLab.</span> 
+ <span style='color:blue'>Make a code coverage test at the end of each day, and push the summary on your distant GitLab repo.</span>


You can now move on to [**Feature 2: Display and visualize the pendulum**](./pendule_S1_visualisation.md)





