# Feature 4: A first animation with matplotlib


Here, we want to visualize the simulation of the pendulum's movement. For the display of the pendulum without motion, we've based ourselves on `matplotlib`, so we'll see how we can generate animations with this library.


## Step 1: Familiarize yourself with matplotlib's animation features.

The display of your pendulum has, so far, relied on the `matplotlib` library. We now want to be able to display an animation showing the pendulum in motion.

Don't know how to do it? Start by checking if the tools at your disposal, specifically `matplotlib`, allow you to do this.

In particular, `matplotlib` has an animation utility described [here](https://matplotlib.org/stable/api/animation_api.html).

So, your first task is to become familiar with this utility. You can use the official documentation [here](https://matplotlib.org/stable/api/animation_api.html), which provides many examples, or look for other tutorials. There are plenty available:

+ [https://www.courspython.com/animation-matplotlib.html](https://www.courspython.com/animation-matplotlib.html)
+ [https://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/](https://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/)
+ [https://brushingupscience.com/2016/06/21/matplotlib-animations-the-easy-way/](https://brushingupscience.com/2016/06/21/matplotlib-animations-the-easy-way/)

Take the time to read and, most importantly, **test** the capabilities of `matplotlib` to understand how it works.

## Step 2: Create a function to visualize the evolution of the pendulum.

Based on what you have learned in the previous step, write a function to visualize the simulated motion of the pendulum with `matplotlib`. You can, for example, refer to [this example](https://matplotlib.org/stable/gallery/animation/double_pendulum.html), which is very similar to our problem.

## Step 3: Save your animation as a video file.

The final step is simply to save your animation as a video file.

**Don't forget to update your local and remote repository.**

You can proceed to [**Feature 5**: Visualization of Pendulum Motion and Velocity Over Time](./S2_motionvisualisation.md)

