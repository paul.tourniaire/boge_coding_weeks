# Feature 6: Introduction to the `argparse` Module

In this feature, the goal is to allow a user to choose simulation parameters via the command line, such as:

+ Pendulum length.
+ Gravity.
+ Initial pendulum position.

To achieve this, we will use Python's `argparse` module. Documentation for this module is available here:

+ Official Documentation [here](https://docs.python.org/3.11/library/argparse.html?highlight=argparse#module-argparse)
+ RealPython Tutorial [here](https://realpython.com/command-line-interfaces-python-argparse/)

Get acquainted with this module quickly!

To get started with this module, you will first allow the user to choose the pendulum's length.

To do this, you should add a `launch_simulation_pendulum` module in the `pendulum` package, which will be called from the command line as follows:

```bash
python launch_simulation_pendulum.py -h
# OR
python launch_simulation_pendulum.py --help
# OR
python launch_simulation_pendulum.py -l length
# OR
python launch_simulation_pendulum.py --length length
```

Calling this program:

+ Will generate a pendulum of the requested length.
+ Will simulate the motion of this pendulum using the utilities developed previously.
+ Will launch a visualization of the pendulum's motion in the form of a `matplotlib` animation.

#### <span style="color: #26B260"> :clap: At this stage of the project, you have reached MILESTONE 7: Managing program parameters via the command line.</span>

Once you have completed and tested this feature, **don't forget to update your local and remote repository.**

You can proceed to [**Feature 7**: A Main Program](./S4_gamemain.md)