# Feature 2: Display the pendulum

The aim of this second function is to be able to **visualize the pendulum**. This visualization will enable you to see a pendulum in motion, a necessary step in understanding how it works.

For this MVP, we've chosen to display the pendulum using the `matplotlib` library, which you've already seen in class.

You should have, at the very least, this type of visualization for a pendulum in its initial position. 

![visu](./Images/pendulumvisu.png)

We leave it up to you to decide how you want to implement this feature, so **IT IS YOU TURN TO PLAY!**.

Please refer to the matplotlib documentation [here](https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py).


You've just completed a feature, so don't forget to:

+ <span style='color:blue'>Commit.</span> 
+ <span style='color:blue'>Review and synchronize your code.</span> 


#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 6: a first full development sprint with your group</span>


Now that we've got the basic building blocks for generating, initializing and displaying a pendulum, let's try setting it in motion.


It corresponds to [**Feature 3**: simulating the movement of a pendulum](./S2_pendule_motion.md) 