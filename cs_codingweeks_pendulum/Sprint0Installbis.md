# Pendulum - Sprint 0: Technical base installation


The first task is to prepare the technical base needed to develop the project. The `physicalsimulation` project will be :

+ A Visual Studio Code project 
+ Using [git](https://git-scm.com/) as a version manager
<!-- + Qui sera déposé sur le [GitLab pédagogique de CentraleSupélec](https://gitlab-ovh-07.cloud.centralesupelec.fr/) dédié dans votre cas `https://gitlab-ovh-07.cloud.centralesupelec.fr/`. -->



## Version with git

As you have seen in class, git is a decentralized Version Control System (VCS).

Here are additional resources to learn git:

+ Quick tutorial [here](http://rogerdudler.github.io/git-guide/index.html).
+ [http://marklodato.github.io/visual-git-guide/index-en.html](http://marklodato.github.io/visual-git-guide/index-en.html)
+ [https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)
+ ...




### Git set-up on the **`physicalsimulation`** project


You should already have git installed and configured on your machine as it was done during class.

In case you want to see it again, the instructions for setting up git and getting to grips with it are given in [**this tutorial**](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/Git_install.md) (in French) or the [same one](../Utils/git_install_en.md) in English, which you should follow to the end.

### <span style="color: #26B260"> :clap: At the end of this tutorial, you'll have reached MILESTONE 1: Set up git and GitLab for a collaborative development work</span> 


## Create a python project with VSCode: **`physicalsimulation`**


We recommend you use [Visual Studio Code](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/VisualStudioCode.md) which you have already seen in class. You just need to open the local `physicalsimulation` directory that you just created in the previous step.

At this stage of the project, you should have:

+ A `physicalsimulation` project on the remote gitlab repository.
+ Each group member should have a local clone of this project on his or her computer, opened via VSCode.

In the rest of the project, you will work as follow:


**For each feature**: 

+ **Each group member works on his own local repository. In this case, it's preferable not to work on the `main` branch, but on your own working branches, which you'll have to create.**

+ **You'll need to agree on a timeframe that's sufficient for everyone to come up with a solution for this feature.**

+ **At the end of this set time, you'll need to plan a time for pooling and reviewing each of the functionalities between yourselves. This process is similar to [code review](https://en.wikipedia.org/wiki/Code_review)**.

+ **After this review, you can then decide which version of the feature to put on the `main` branch, which should always contain the stable version of your project.** 

+ **And, of course, you'll need to push the remote repository to enable you to share this stable version among yourselves.**

+ **The transition and work on a new feature will therefore be based on a `main` branch synchronized between you all** .

 

You can now continue with the [Sprint 0: Problem analysis](./Sprint0Analyse.md). 

