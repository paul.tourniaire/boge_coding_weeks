# Feature 3: Simulating the movement of a pendulum

This function sets the pendulum in motion. The aim is to program the equation of the pendulum motion. For this function, we'll be using differential equation solvers from the [scipy](https://www.scipy.org/) library, and in particular this [solver](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html#scipy.integrate.solve_ivp).

The principle is well explained in this [tutorial](https://python-prepa.github.io/systemes_dynamiques.html)

Create a `pendulum_simulation_motion.py` module in the `pendulum` package, in which we'll write the functions required for this simulation.


## Step 1: Definition of initial simulation parameters.

In the `pendulum_simulation_motion.py` module, write an `initiate_simulation` function that initializes the various simulation parameters, such as :

+ acceleration due to gravity, default value `9.8`.
+ angle and initial speed.
+ ...


## Step 2: Writing equations to be solved in a function.


Add and write the function `simple_pendulum_ODE` in the module `pendulum_simulation_motion.py` which, given a pendulum of length *L* and a value of acceleration due to gravity, transforms the 2nd-order differential equation governing the pendulum's motion into a system of first-order equations as explained [here](https://python-prepa.github.io/systemes_dynamiques.html).


We leave it up to you to decide how to implement this feature. You are expected to provide commented code to implement this functionality and the associated tests. You are free to apply the TDD methodology or not.

Here, for example, a test to be passed, among others, could be

```python
from pendulum.pendulum_simulation_motion import *
from pytest import *
import numpy as np


def test_simple_pendulum_ODE():
    #Given
    gravity = 9.8
    length = 10
    Y0 = np.array([np.pi / 2.0, 0])
    t=0
    #When
    second_term = simple_pendulum_ODE(Y0,t,length,gravity)
    #Then
    assert np.allclose(second_term,np.array([ 0.,-0.98])) == True
```


## Step 3: Search for a solution by integrating a trajectory from an initial condition.


Add the `find_solution` function, which finds a solution by numerically integrating the equations of motion to obtain the evolution of the pendulum's position and angular velocity over time. The solvers in the `scipy` library are used for this step, in particular the solver [`solve_idp`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html#scipy.integrate.solve_ivp) or [`ode_int`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.odeint.html#scipy.integrate.odeint). `ode_int` is widely used, but is obsolete in the latest versions of scipy, so it is preferable to use `solve_idp`.

Other functions are available in scipy's [`integrate`](https://docs.scipy.org/doc/scipy/reference/integrate.html#solving-initial-value-problems-for-ode-systems) module.

To write this function, you'll also need to use numpy's `linspace` function, which allows you to obtain a 1D array from a start value to an end value with a given number of elements, and which will be used to define the time sampling. We'll need to make this function as generic and configurable as possible.


**Don't forget to update your local and remote repositories as we saw in the git tutorial.** 


You can now move on to [**Feature 4**: A first animation with matplotlib](./S2_simpleanimation.md)
