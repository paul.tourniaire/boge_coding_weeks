# Feature 12: Allow Configuration of the Pendulum via the Graphical Interface

In this feature, we will add several widgets to our main window (the empty white window) to allow:

- Choosing the initial velocity of the pendulum.
- Choosing its mass.
- ...

If you have completed this task, you can now add other features of your choice to your graphical simulation.

## Finally

- <span style='color:blue'>Commit your latest changes.</span>
- <span style='color:blue'>Tag this last commit.</span>
- <span style='color:blue'>Complete the synchronization step.</span>

You can now move on to [**Objective 3**](./TemplateProject_pendulum.md#objective-3-a-pendulum-with-pymunk).