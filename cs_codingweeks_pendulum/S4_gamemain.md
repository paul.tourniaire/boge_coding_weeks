# Feature 7: Bringing it all together in a main program

Now it's time to put everything we've done into a main program.

To do this, you need to add a main function as follows:

```python
if __name__ == '__main__':
...
```

Some documentation on this topic can be found [here](https://docs.python.org/fr/3/library/__main__.html) and [here](https://www.guru99.com/learn-python-main-function-with-examples-understand-main.html).

Or, in general, you can refer to the [RealPython](https://realpython.com/) website, which has excellent tutorials, including one that is relevant to us [here](https://realpython.com/python-main-function/).

#### <span style="color: #26B260"> :clap: At this stage of the project, you have reached MILESTONE 8: Orchestrating a project with a main function.</span>

You can proceed to [**Feature 8**: A Command-Line Pendulum](./S3_pendulum_mvp.md)