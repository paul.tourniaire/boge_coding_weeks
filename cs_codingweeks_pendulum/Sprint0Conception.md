# Pendulum simulation - Sprint 0: Design thinking

After this analysis phase, we can take an initial design approach and try to identify the main objects of our MVP.

By reading the [description of the simple pendulum on Wikipedia](https://en.wikipedia.org/wiki/Pendulum_(mechanics)), we identify a number of concepts such as **pendulum**, **speed**, **mass**, **position**, **mechanical energy**, **oscillation**, as well as all the concepts referring to the physical phenomenon whose behavior we wish to observe. 


## Towards a common language

In this phase of design thinking, to encourage collaborative work and a common understanding between all project members, it's important to define, right from the start of the project, a **common vocabulary** around business terms. In software development jargon, this is known as [**ubiquitous language**](https://tanzu.vmware.com/developer/blog/ubiquitous-language/). This principle stems from the *Domain Driven Design* approach described in the book of the same name by Eric Evans, and consists in identifying and defining a common language around business terms.

The process consists of producing what are known as [**User Stories**](https://en.wikipedia.org/wiki/User_story) (with all project stakeholders), which represent the user needs to be implemented. This work also helps define the shared language.

A **user story** is nothing more than a simple sentence, in natural language, describing the content of a feature to be developed, specifying the *Who*, the *What* and the *Why*.

 `As <who>, I want <what> so that <why>`

 Here, a typical example would be:

+ As a user, I want to be able to **configure the simulation** of the pendulum
+ As a user, I want to be able to **configure the pendulum model**.
+ As a user, I want to be able to **be told** when an interesting phenomenon happens.
+ As a teacher, I want to be able to **generate, configure and save animations** of the pendulum simulation.
+ ...

The aim of this phase is mainly to think about the purpose of the application we're developing and its appropriation by the intended users.


Concerning the shared language, in the case of the pendulum, several terms are used to refer to the rod, for example: *wire*, *rod*,.... The very concept of a simple pendulum may need to be defined.

It is therefore necessary to choose a precise term for each object in the application.


## Your application's main objects and modules

This analysis phase should also get you thinking about the design of your application, and in particular its various objects and modules, while trying to separate responsibilities. 

To do this, try to think about the functional architecture of your application and how the different actors, modules and objects interact with each other. Do this with paper and pencil, in a schematic way. Group work and reflection can be interesting here!

Take a little time to do this. When you've finished, take a photo of what you've put down on paper and store it in a directory called `WorkingDocs` in your git repository. Don't forget to `commit` and update your remote repository.

 
The aim of this work is to enable you to model your problem from an IT point of view, but it is also essential for the organization of your work, i.e. its division into different sprints and functionalities, for example. 

Here, this work has been partially done for you, but you'll need to do it for your Week 2 project.

#### <span style="color: #26B260"> :claps: At this stage, you have reached MILESTONE 2: Analysis and Conception of my product </span> 

You can now move on to [**Feature 1**: Pendulum representation.](./pendulum_S1_pendule.md)