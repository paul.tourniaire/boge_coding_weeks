# Physics simulation - Sprint 0: Problem analysis

One of the first steps in any software programming and development project, whatever the development methodology used, is to carry out a quick **problem analysis**, before any implementation phase.

The aim of this analysis is to identify the main functionalities to be developed in order to achieve the desired behavior of the system being developed. This initial list of functionalities need not be exhaustive or fixed, but it will enable you to build your first developments.


## Requirements analysis: the main functionalities

The aim here is to enable a user to study the behavior of a physical system through numerical simulation and visualization. The aim is to design software that enables users to interact with one or more physical simulations, initially in console mode, without a graphical interface.



The **[MVP (Minimum Viable product)](https://en.wikipedia.org/wiki/Minimum_viable_product)** of this project will be to deliver a first terminal-based simulation of a physical phenomenon—in our case the pendulum. Your solution will

+ **Allows a user to choose the various parameters of the pendulum simulation: initial velocity, length of rod...**
+ **Enable simulation and visualization of the simulation according to the parameters entered by the user**.
+ **Display the simulation as an animation using the `matplotlib` animation module**.
+ **Highlight the various parameters and phenomena known about the pendulum model**.


Other features can of course be added later to enhance the simulation once this MVP has been completed. By enabling a rapid simulation prototype, you can quickly test your product on its users. Imagine that your users are high school students who want to understand a physical phenomenon by observing it through simulations. They'll be able to give you rapid feedback on the features you need to achieve your goal.


You can now continue with the [Sprint 0: Design thinking](./Sprint0Conception.md).