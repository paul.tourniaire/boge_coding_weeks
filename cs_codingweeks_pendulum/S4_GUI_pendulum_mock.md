# Feature 10: A graphical interface for the pendulum: mockup


The task at hand is to create a paper and pencil mockup of what you would like the graphical interface for the pendulum to look like. There are also digital tools available for this, such as [Pencil](https://pencil.evolus.vn/).

Work on this task as a group and upload your mockup to your local and remote Git repositories.

After completing this specification work, you can proceed to [**Feature 11**: A Graphical Interface for the Pendulum](S4_GUI_pendulum.md).

