# Feature 9: Skill Enhancement - Python Graphical User Interfaces

There are several frameworks for developing graphical user interfaces (GUIs) in Python. A list of these different tools is available [here](https://wiki.python.org/moin/GuiProgramming).

In particular, Python's standard library includes a module called **Tkinter (Tk interface)**, which allows you to develop graphical interfaces. We will use this module in this part of the project, which does not require any installation.

Documentation for this module is available [here](https://wiki.python.org/moin/TkInter).

You will now create a quick tutorial (inspired by OpenClassRooms' course [Learn Python Programming](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/234859-des-interfaces-graphiques-avec-tkinter) and this [tutorial](https://www.python-course.eu/python_tkinter.php)) to familiarize yourself with this module and tool.

Vincent Maillol's [tutorial](https://vincent.developpez.com/cours-tutoriels/python/tkinter/apprendre-creer-interface-graphique-tkinter-python-3/) is also highly recommended.

<!-- Another well-made French documentation for Tkinter is available [here](http://tkinter.fdex.eu/index.html). -->

## Creating Your First Tkinter GUI

Add a `tuto_GUI.py` file to your project and copy the code below, which creates a window that displays the message *Hello World!*.

```python
from tkinter import *

# Window creation, the root of the interface
window = Tk()

# Creation of a label (text line) that says Hello World! with the previous window as the first parameter
label_field = Label(window, text="Hello World!")

# Display the label
label_field.pack()

# Run the Tkinter loop that ends when we close the window
window.mainloop()
```

Executing this code should display the following window.

![Tkinter Hello World](./Images/hello.png)

The `config()` method allows you to configure widgets. For example, in the previous example, the following two lines:

```python
label_field = Label(window, text="Hello World!")
label_field.config(text="Hello World!")
```

are equivalent to:

```python
label_field = Label(window, text="Hello World!")
```

`pack` is a geometry manager that manages the composition of widgets.

Widget placement is relative to their container (using the `side` configuration option) and can be positioned with a cardinal direction (`Tkinter.TOP`, `Tkinter.LEFT`, `Tkinter.RIGHT`, `Tkinter.BOTTOM`). By default, a widget is attached at the top or below existing widgets.

If you want to occupy the maximum available space, use the `expand=YES` configuration option. The direction of maximum occupation is specified by `fill`, which can be width (`Tkinter.X`), height (`Tkinter.Y`), or both (`Tkinter.BOTH`).

Tkinter offers other positioning managers, including the `grid` manager, which divides the container into a grid and places the widget in a cell of the grid, and the `place` manager, which positions the widget precisely in pixels but is challenging to use in practice.

## Exploring the Main Tkinter Widgets

Tkinter has a number of widgets, which are graphical objects, such as buttons, text fields, checkboxes, progress bars, etc. Here, we will introduce the main ones.

### Label

In the previous example, we saw the `Label` widget, which is used to display text in the main window. This text cannot be modified by the user.

### Button

Buttons (`Button`) are widgets that can be clicked and trigger actions or **commands**.

Copy the previous code to your test file and run it. What does it do?

```python
import tkinter as tk


def write_text():
    print("Hello CentraleSupelec")

root = tk.Tk()
frame = tk.Frame(root)
frame.pack()

button = tk.Button(frame,
                   text="QUIT",
                   activebackground="blue",
                   fg="red",
                   command=quit)
button.pack(side=tk.LEFT)
slogan = tk.Button(frame,
                   fg="blue",
                   text="Hello",
                   command=write_text)
slogan.pack(side=tk.LEFT)

root.mainloop()
```

Please note that for macOS users, colors may not be recognized. You can find instructions [here](https://stackoverflow.com/questions/1529847/how-to-change-the-foreground-or-background-colour-of-a-tkinter-button-on-mac-os).

This example demonstrates how to initiate an action from a GUI. Here, the `quit` command is associated with the first button to close the window, and the command defined by the `write_text()` function is associated with the second button.

### Entry

The `Entry` widget is an input widget used to collect user input. It takes a `StringVar` object as a parameter, which is used to retrieve the text entered. If the `StringVar` is updated, the input field is also modified.

Test the code below:

```python
from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial

def update_label(label, stringvar):
    """
    Updates the text of a label using a StringVar.
    """
    text = stringvar.get()
    label.config(text=text)
    stringvar.set('merci')

root = Tk()
text = StringVar(root)
label = Label(root, text='Your name')
entry_name = Entry(root, textvariable=text)
button = Button(root, text='clic',
                command=partial(update_label, label, text))

label.grid(column=0, row=0)
entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)

root.mainloop()
```

The `Button` widget takes a text to display and a function to execute. The function to execute does not take any parameters, so if you want to use an existing function, you should wrap it with the [`partial`](https://docs.python.org/2/library/functools.html) module.

Explore the other Tkinter widgets, such as [`Radiobuttons`](https://www.python-course.eu/tkinter_radiobuttons.php) or [`Checkbuttons`](https://www.python-course.eu/tkinter_checkboxes.php), using the tutorial available [here](https://www.python-course.eu/tkinter_radiobuttons.php).

## Containers

Containers are widgets designed to contain other widgets.

In our examples, we have already used windows (Toplevel) using the `root = Tk()` statement.

Among the containers, there is also the `Frame` widget, which allows you to group multiple widgets together. This can simplify their placement, as shown in the example below.

```python
from tkinter import Tk, Label, Frame

root = Tk()
f1 = Frame(root, bd=1, relief='solid')
Label(f1, text='I am in F1').grid(row=0, column=0)
Label(f1, text='Me too in F1').grid(row=0, column=1)

f1.grid(row=0, column=0)
Label(root, text='I am in root').grid(row=1, column=0)
Label(root, text='Me too in root').grid(row=2, column=0)

root.mainloop()
```

## Events

It is possible to capture events, such as pressing a key or clicking the mouse, to perform special actions. For a widget to process events, it must have the focus. Usually, a widget gets the focus when you click on it. Events can then be processed by a function.

The code below, which you should test, displays *Hello* based on clicks or keypresses.

```python
from tkinter import *
from pprint import pformat

def print_bonjour(i):
    label.config(text="Hello")

root = Tk()
frame = Frame(root, bg='white', height=100, width=400)
entry = Entry(root)
label = Label(root)

frame.grid(row=0, column=0)
entry.grid(row=1, column=0, sticky='ew')
label.grid(row=2, column=0)

frame.bind('<ButtonPress>', print_bonjour)
entry.bind('<KeyPress>', print_bonjour)
root.mainloop()
```


You can now work on your pendulum with a graphical user interface. Don't hesitate to revisit the various tutorials mentioned to deepen your knowledge of Tkinter.

We can now move on to the next feature: [**Feature 10**: A graphical interface for the pendulum: Mockup](S4_GUI_pendulum_mock.md).