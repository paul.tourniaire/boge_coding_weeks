# Feature 5: Visualization of pendulum motion and velocity over time

Here, the goal is to complete the program and the study of the physical model by plotting and visualizing:

+ The evolution of angular velocity over time.
+ The evolution of the angle over time.
+ The phase diagram of the simple pendulum, which is the angular velocity as a function of the angle value.

**Don't forget to update your local and remote repository.**

You can proceed to [**Feature 6**: Getting Started with the `argparse` Module](./S3_argparse.md)