# Feature 1: Getting started with the IMDB corpus

For this project, we will work with a well known corpus of movie reviews for natural language processing or Text analytics, the IMDB dataset.

IMDB dataset has 50K movie reviews for natural language processing or Text analytics.

This is a dataset for binary sentiment classification containing substantially more data than previous benchmark datasets. It provides a set of 25,000 highly polar movie reviews for training and 25,000 for testing. 

Your first task is therefore to look at and understand this corpus. It is given in two different forms. The first is a csv file containing the 50,000 reviews in the first column and the positive or negative sentiment in the second column.

This file is provided [here](./Data/IMDB_Dataset.csv).

A second form is the complete dataset available [here](./Data/aclImdb) and that corresponds to the [original dataset](https://ai.stanford.edu/~amaas/data/sentiment/).

The IMDB movie review data consists of 50,000 reviews -- 25,000 for training and 25,000 for testing. The training and test files are evenly divided into 12,500 positive reviews and 12,500 negative reviews. Negative reviews are those reviews associated with movies that the reviewer rated as 1 through 4 stars. Positive reviews are the ones rated 7 through 10 stars. Movie reviews that received 5 or 6 stars are considered neither positive nor negative and are not used.


The root aclimdb directory contains subdirectories named test and train, plus three files that you can ignore. The test and train directories contain subdirectories named neg and pos, plus five files and one directory named unsup (50,000 unlabeled reviews for unsupervised analysis) that you can ignore. The neg and pos directories each contain 12,500 text files where each review is a single file.

The 50,000 file names look like 102_4.txt where the first part of the file name is the [0] to [12499] review index and the second part of the file name is the numerical review rating (0 to 4 for negative reviews, and 7 to 10 for positive reviews).

The following screenshot shows the directory structure of the IMDB movie review data. The contents of the first positive sentiment training review (file 0.9.txt) is displayed in Notepad.


<img src="./Images/imdb.png" alt="drawing" width="500"/>


Once the corpus has been understood, you need to associate the data with your `datavisualization` project. Create a `Data` directory at the root of your project and put the downloaded data there.


This step can also be an interesting opportunity to think about the structure of your project's code. How many modules and packages, for example ? We will discuss this point latter.

We've finished Feature 1 here. There was no real code writing in this feature, but rather data retrieval. However, you will normally have added a `Data` directory to your project with the retrieved reviews.


An important question to ask here is the status of these files in relation to your git repository and its clone on gitlab. For several reasons, the main one being their confidentiality, it's not advisable to add the data to our repository. In fact, it's sometimes illegal to put this data on a public gitlab repo. It is not the case for the IMDB dataset but it can be for other datasets.

So we're going to tell git to ignore part of this data. To do this, we'll associate a `.gitignore` file with the root of our repository.




This file will contain this type of information:

```
# Lines beginning with '#' are comments.
# Ignore all files named foo.txt
foo.txt
# Ignore all html files
*.html
# with the exception of foo.html, which is maintained by hand
!foo.html
# Ignore objects and archives
*.[oa]
```


Write the `.gitignore` file to the root of your repository and complete it so as to ignore the `Data/aclImdb` directory.


You can now move on to [**Feature 2** : A data access utility](./S1_dataacess_en.md)






