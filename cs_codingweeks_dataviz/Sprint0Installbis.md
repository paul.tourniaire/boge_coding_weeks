# Datavisualization - Sprint 0 : Setting Up the Technical Foundation

The first task is to prepare the technical foundation required for the development of the project. The **dataviz** project will be:

- A Visual Studio Code project.
- Using [git](https://git-scm.com/) as a version control system.
- That will be stored on the [CentraleSupélec's pedagogical GitLab](https://gitlab-student.centralesupelec.fr/users/sign_in), in your case  `https://gitlab-student.centralesupelec.fr`


## Version Control with Git

As you have seen in class, git is a decentralized Version Control System (VCS).

Here are additional resources to learn git:

+ Quick tutorial [here](http://rogerdudler.github.io/git-guide/index.html).
+ [http://marklodato.github.io/visual-git-guide/index-en.html](http://marklodato.github.io/visual-git-guide/index-en.html)
+ ...

### Setting Up Git for the **`dataviz`** Project

You should already have git installed and configured on your machine as it was done during class.

In case you want to see it again, the instructions for setting up git and getting to grips with it are given in [**this tutorial**](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/Git_install.md) (in French) or the [same one](../Utils/git_install_en.md) in English, which you should follow to the end.

#### <span style="color: #26B260"> :clap: At the end of this tutorial, you will have reached MILESTONE 1: Setting up Git and GitLab for collaborative development.</span>


## Create a Python Project with Visual Studio Code: **`dataviz`**

We recommend using the Visual Studio Code editor, which was also recommended during the programming class. Just open the local project directory **`dataviz`** you created in the previous step using Visual Studio Code.

At this stage of the project, you should have:

- A **dataviz** project on the remote GitLab repository.
- Each member of the group should have a local clone of this project on their computer, which is opened using VSCode.

In the following stages of the project, you will work as follows:

**For each feature**:

- **Each member of the group works on their local repository. In this case, it is preferable not to work on the `main` branch but on working branches, which will be your own and should be created.**

- **You should agree on a sufficient amount of time for each member to propose a solution for the feature.**

- **After this set period of time, you should plan for a review and merging session between all group members for each feature. This process can be considered as a form of [code review](https://en.wikipedia.org/wiki/Code_review).**

- **After this review process, you can decide on the version of the feature to put on the `main` branch, which should always contain the stable version of your project.**

- **Of course, you will need to push to the remote repository to share this stable version among yourselves.**

- **The transition and work on a new feature will be based on a main branch that is synchronized among all of you.**

You can now continue with [Sprint 0: Needs Analysis](./Sprint0Analyse.md).