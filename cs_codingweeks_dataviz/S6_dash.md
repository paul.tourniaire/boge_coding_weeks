# Feature 11: Your first app with Dash



[Dash](https://plot.ly/) is a framework for quickly prototyping visualization web applications.

## Step 1: Install Dash.

To install it, type the commands below:


```bash
pip install dash
```



## Step 2: Create a Dash Layout


A Dash application is made up of two parts. The first part is the **layout** of the application and it describes what the application looks like. The second describes interactions with the application.

To create your first Dash application, we will follow the site manual found [here](https://dash.plotly.com/layout).

If you copy and execute the code given in this tutorial, you will obtain this visualization at the address mentioned during execution, here `http://127.0.0.1:8050/`.


![hellodash](./Images/dash.png)



It is an interactive application. Try hovering your mouse over it.
Take a good look at the different types of graphs or visualizations offered by DASH by taking the time to watch the [tutorial](https://dash.plotly.com/)).



## Step 3: Create a dashboard layout for opinion analysis and for the IMDB 5000 dataset.

The objective now is to create, with `dash`, a layout for your opinion analysis application. This is an open step which will most certainly require adapting one of the examples given in the dash tutorial to your application.



We will now use a final word cloud representation to display the vocabulary of our tweets. This is the feature: [**Feature 13**: Getting started with WordCloud](./S7_wordcloud.md)











