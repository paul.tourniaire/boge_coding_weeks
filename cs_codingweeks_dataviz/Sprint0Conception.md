# DataVisualization - Sprint 0: Reflection on design

After this analysis phase, we can have a first design approach and try to identify the main objects of our MVP.

The question to ask is *What are we talking about?*

## Towards a common language

In this phase of design reflection, to promote collaborative work and a common understanding between all members of the project (in the sense all stakeholders), it is important to define, from the start of the project, a **vocabulary common** around business terms. In software development jargon, we talk about [**ubiquitous language**](http://referential.institut-agile.fr/ubiquitous.html) or **ubiquitous language**. This is a principle from the *Domain Driven Design* approach described in the [book](https://github.com/p0w34007/ebooks/blob/master/Eric%20Evans%202003%20-%20Domain- Driven%20Design%20-%20Tackling%20Complexity%20in%20the%20Heart%20of%20Software.pdf) of the same name and which consists of identifying and defining a common language around business terms which can be [useful](https ://promyze.com/pourquoi-lire-red-book-domain-driven-design/) in many situations.


In the case of the `datavisualization` project, this is work that may seem tedious and most certainly useless to you but which will be very useful to you for many other projects, especially when your project involves developers and business experts. This project is a school project and there are therefore no professional experts but to try to act as if, you can for example imagine that your client is the organizing committee of the Miss France competition which wishes to follow the evolution of the popularity of its competition on social media and which therefore asks you to create an application allowing it.

The process consists of producing what we call [**User Stories**](https://en.wikipedia.org/wiki/User_story) (with all the project stakeholders) which represent the needs users to implement. This work also makes it possible to define the shared language.

A **user story** is nothing other than a simple sentence, in natural language, which allows you to describe the content of a functionality to be developed by specifying the *Who?*, the *What?* and the *Why?*

  `As <who>, I want <what> in order to <why>`

Here, typically, let's assume that your application is intended for the media provider: 

+ As a media provider, I want to be able to **launch and configure a data collection** on reviews in order to be able to analyze the media's **opinion** of my catalog.
+ As a media provider, I want to be able to **choose a review* and have a **fine analysis** of the **opinion** on this review.
+ As a media provider, I want to be able to have an **alert** if the **opinion** of my catalog.
+ ...


Here, the terms and expressions **review**, **opinion**, **collection** for example, are part of the **shared language** of our context. They can be used indifferently by all project stakeholders and will always designate the same concepts.

The aim here is to simplify exchanges between the different actors.

They must also allow you to properly name the different objects, variables, functions of your application.

## The main objects, modules of your application

This analysis phase should also make you think about the design of your application and in particular the different objects and modules of the latter while trying to separate responsibilities.

To do this, try to think about the functional architecture of your application and how the different actors, modules, objects interact with each other. Do it with paper and pencil, in a schematic manner. Group work and reflection is interesting here!

Take some time to do this work. When finished, take a photo of what you put on paper and store this in a directory called `WorkingDocs` in your git repository. Don't forget to commit and update your remote repository.

 
The objective of this work is to allow you to model, from an IT point of view, your problem but it is also essential for the organization of your work, that is to say, its division into different sprints and functionalities by example.

Here, this work has been done for you but you will have to do it for your week 2 project.

#### <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 2: Analysis and Design of my product </span>

We will now move on to the development stage by creating [**Feature 1**: Getting started with the IMDB corpus.](./S1_corpuspriseenmain_en.md)