# Feature 6: We put all this in a main program


At this stage, we have almost finished our MVP, that is, a program that can load some data, do a statistical analysis of this data and visualize it with graphical representations.

Imagine that you need to prepare a demonstration for your client. You will need to *package* your code a little to make it a little more professional.

One of the first steps is to associate a `main` program with your project. For example, you can add a `dataviz_main.py` file to the root of your project which will contain the entry point for the scripts.

In other words, we want to be able to launch the demonstration of our MVP, by running the `dataviz_main.py` file.

On the command line, this would give:

`python dataviz_main.py`



To do this, you must add a main function in `dataviz_main.py` as follows:

```python
if __name__ == '__main__':
...

```

Some documentation [here](https://realpython.com/python-main-function/) and [there](https://www.guru99.com/learn-python-main-function-with-examples-understand-main.html).

More generally, you can refer to the site [RealPython](https://realpython.com/) which has very good tutorials.



Inside the main function, it will therefore be necessary to:

  + Load data
  + Preprocess them
  + Analyze them
  + View analysis results by genarating some visualizations.



#### :clap: <span style="color: #26B260">At this point in the project, you have reached MILESTONE 8: orchestrating a project with a main function</span>


At this stage, you can also ask yourself the question of reusing your code.
Typically, for this project, the supervisory team should be able to clone your project from the remote repository and run your programs without bugs.


+ To avoid any problems, your code must be well structured or architected and well written (for example, favoring relative file access paths over absolute paths).
+ It is also necessary to say what the dependencies of your project are, typically, all the external libraries that are necessary. The typical way to do this is to create a `requirements.txt` file listing all the necessary packages with their version.

```
matplotlib 
pandas
...
...
```
It is therefore necessary to list all dependencies and their version in this file. A user will then just need to run the command below to install the dependencies.

`pip install -r requirements.txt`


On this aspect, if you want to go further, you can also watch this [tutorial](https://realpython.com/lessons/using-requirement-files/).


+ Finally, it will be necessary to document your `README.md` a little to write the execution instructions. For this README file, we can look [this documentation](https://www.makeareadme.com/)

Take the time to do this work, document your code and of course:

+ <span style='color:blue'>Commit your latest changes.</span>
+ <span style='color:blue'>Tag this last commit </span>
+ <span style='color:blue'>Push your code to your remote repository on GitLab.</span>

A good test here is to ask someone outside your group to clone your code to your remote repository and run your project following the instructions given in your README.

It is a know-how that corresponds to <span style="color: #26B260"> MILESTONE 13: making your project usable and reusable </span>


To finalize this MVP, a final step is to provide our MVP with a command line interface. This is the subject of [**Feature 7**: A command line interface with `argparse`.](./S3_argparse.md)



