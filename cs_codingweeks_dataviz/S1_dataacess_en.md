# Feature 2: A utility for accessing corpus data.

To correctly manipulate the information contained in the corpus, we need to program a set of tools to retrieve useful information. To do this, we're going to create a `corpusutils` module inside the `datavisualization` project. Remember that the best way to do this is to add an empty `__init.py__` file to the root of the `datavisualization` directory. If you're not very familiar with this notion of modules and packages, we invite you to consult the tutorial at the beginning [here](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/modulespackagespython.md) or this [documentation](https://chrisyeh96.github.io/2017/08/08/definitive-guide-python-imports.html).

We'll build this `corpusutils` module iteratively.


## Iteration 1: a movie review reader.


Your first job will consist of building a first small set of data to work on. This dataset will consist of the first 100 positive reviews and the first 100 negative reviews.

Your first job is therefore to create a .txt file reader. This reader will be implemented in a function `read_and_load_txt_review_file(filename)` which, given the filename path of a review file, opens this file and returns the useful information in the form from a python `dict` dictionary.

For example, for the file `0_3.txt` in the `train/neg` repertory which content is 

```
Story of a man who has unnatural feelings for a pig. Starts out with a opening scene that is a terrific example of absurd comedy. A formal orchestra audience is turned into an insane, violent mob by the crazy chantings of it's singers. Unfortunately it stays absurd the WHOLE time with no general narrative eventually making it just too off putting. Even those from the era should be turned off. The cryptic dialogue would make Shakespeare seem easy to a third grader. On a technical level it's better than you might think with some good cinematography by future great Vilmos Zsigmond. Future stars Sally Kirkland and Frederic Forrest can be seen briefly.
```



the function `read_and_load_txt_review_file('./Data/aclImdb/train/neg/0_3.txt')` should return the following `dict` in python.

```
{ 'opinion' : 'negative' , 'opinion_grade' : 3,  'id_neg' : 0, 'review_content' : 'Story of a man who has unnatural feelings for a pig. Starts out with a opening scene that is a terrific example of absurd comedy. A formal orchestra audience is turned into an insane, violent mob by the crazy chantings of it's singers. Unfortunately it stays absurd the WHOLE time with no general narrative eventually making it just too off putting. Even those from the era should be turned off. The cryptic dialogue would make Shakespeare seem easy to a third grader. On a technical level it's better than you might think with some good cinematography by future great Vilmos Zsigmond. Future stars Sally Kirkland and Frederic Forrest can be seen briefly.'}
```

To write this review reader, we will use the development approach [**TDD (Test Driven Development)**](https://en.wikipedia.org/wiki/Test-driven_development) which consists of specifying the expected behavior via a test before actually implementing it (this is what we typically just do).


The principle is therefore to first write the test and then the simplest possible code which allows the test to pass and therefore satisfy the specified behavior. The code can then be improved. The idea is therefore to focus on functionality rather than code.

We will first work step by step then you will gradually gain autonomy in this approach.

The principle is therefore to first write the test and then the simplest possible code which allows the test to pass and therefore satisfy the specified behavior. The code can then be improved. The idea is therefore to focus on functionality rather than code.

We will first work step by step then you will gradually gain autonomy in this approach.

1. **Acceptance criteria.**

One of the first tasks to do here is to search for and list all of the criteria that will correctly meet the needs that the *Parse a txt review file* functionality is supposed to cover. These criteria are **acceptance criteria**. Here, it's very simple, the acceptance criterion of *Parse a txt review file* is to **have as output a dictionary containing the useful information**.
 
 
 
 
2. **Development in TDD mode**

**TDD (Test Driven Development)** is test-driven development and therefore the first line of your program must be in a test file. In our case, we will use the module [`pytest`](https://docs.pytest.org/en/latest/) which must therefore be added to your project. You can look [here](https://code.visualstudio.com/docs/python/testing). The principle of TDD is based on 3 complementary steps.
 
+ First step (**<span style='color:red'>RED</span>**): Write a first test which fails.
+ Second step (**<span style='color:green'>GREEN</span>**): Write the simplest code that allows you to pass the code.
+ Third step (**REFACTOR**): Improve the source code.

 
We will thus apply this methodology to the writing of the `read_and_load_txt_review_file(filename)` function.
 



#### **<span style='color:red'> STEP RED</span>**

Our first test will consist of testing that the parsing returns a well-founded dictionary when reading a txt file.

```python
from datavisualization.corpusutils import read_and_load_txt_review_file
from pytest import *


def test_read_and_load_txt_review_file():
     #Given
     filename = "./Data/aclImdb/train/neg/0_3.txt"
     # When
     annotations = read_and_load_txt_review_file(filename)
     #Then
     assert annotations = { 'opinion' : 'negative' , 'opinion_grade' : 3,  'id_neg' : 0, 'review_content' : 'Story of a man who has unnatural feelings for a pig. Starts out with a opening scene that is a terrific example of absurd comedy. A formal orchestra audience is turned into an insane, violent mob by the crazy chantings of it's singers. Unfortunately it stays absurd the WHOLE time with no general narrative eventually making it just too off putting. Even those from the era should be turned off. The cryptic dialogue would make Shakespeare seem easy to a third grader. On a technical level it's better than you might think with some good cinematography by future great Vilmos Zsigmond. Future stars Sally Kirkland and Frederic Forrest can be seen briefly.'}
     #Given
     filename1 = "./Data/aclImdb/train/pos/8_7.txt"
     # When
     annotations1 = read_and_load_txt_review_file(filename1)
     #Then
	assert annotations1 = { 'opinion' : 'positive' , 'opinion_grade' : 7,  'id_pos' : 8, 'review_content' : 'Very good drama although it appeared to have a few blank areas leaving the viewers to fill in the action for themselves. I can imagine life being this way for someone who can neither read nor write. This film simply smacked of the real world: the wife who is suddenly the sole supporter, the live-in relatives and their quarrels, the troubled child who gets knocked up and then, typically, drops out of school, a jackass husband who takes the nest egg and buys beer with it. 2 thumbs up.'}
```

Copy and paste this code in a file `test_read_and_load_txt_review_file.py`. 

This test must fail, since in the current state of the project, the code for `read_and_load_txt_review_file` does not exist and we therefore have an error when executing the code.

#### **<span style='color:green'> STEP GREEN</span>**

We will now write the code that allows this test to pass as quickly as possible.

All you need to do is:

  + Create and complete a `read_and_load_txt_review_file(filename)` function in the `corpusutils.py` file in such a way that the previous test passes.
 
Your test should turn green with this step **<span style='color:green'> GREEN STEP</span>** 

#### **<span style='color:black'> STEP REFACTOR</span>**
   
The last step consists of a [refactoring](https://refactoring.com/) step, to be implemented if necessary.

[Refactoring](https://en.wikipedia.org/wiki/Code_refactoring)(or code refactoring) is a programming principle which consists of changing the internal structure of software without changing its observable behavior. This is a step that must always be carried out when the various tests are green and which is not obligatory. Above all, it must make it possible to improve the **quality of the code**, for example by improving:
 
  + **design**: division into functions, modules or classes to make your code as simple as possible.
  + **the readability of the code**: here you need to take the time to apply the principles of [clean code](https://cleancoders.com/cart) introduced by Robert C. Martin in the work of the same name and one of the principles of which is that of the boy scouts (*“The Boy Scout Rule”*): *“Always leave a place in a better condition than the one where you found it”*.
 
In our case, one of the first principles is to check good naming (variables, functions, packages, classes, etc.) and the presence of comments in our code.
 
 You will find [here](https://github.com/zedr/clean-code-python#objects-and-data-structures) some principles of clean code transposed to the python language. Take the time to quickly read this site and apply these different principles to the code you are going to write.
 
In this step **<span style='color:black'> REFACTOR STEP</span>**, we can also work on optimizing the performance of the program if this proves really necessary. 
 
#### **CAUTION**

1. **After this step, don't forget to run the tests again to check that the behavior of your code has not changed and that everything is still GREEN!**

2. We have just finished carrying out the step *a movie review reader* and it is therefore appropriate to **commit this change to your version manager with an explicit commit message stating the objective of the step **. Also remember to update your remote repository.
 
 
In the following, we advise you to use the TDD approach but it is not obligatory. However, you must always take care **to test your programs**.
 
 
 

## Iteration 2: building a small opinion mining dataset

You will know build a small opinion mining dataset composed of the first 100 positive reviews and the first 100 negative reviews. 

Write and test the function `load_and_build_opinion_dataset(num_pos, num_neg)` that takes the targeted number of positive reviews (here 100) and the number of targeted number of negative reviews (here 100) and that builds the resulting dataset in the form of the dictionary `dict` in python of the following form :

```
{ `positive_reviews` : [ list of the dict of the loaded positive reviews], 'negative_reviews : [list of the dict of the loaded negative reviews] }
```


Try to do it with the TDD approach but just in asserting the good number of positive and negative reviews (i.e. the size of the list) and the format of the obtained dictionary.



#### :clap: <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 3: Write code in a TDD approach </span>



## On code coverage

Code coverage corresponds to the percentage of code that is covered by tests. Therefore, it lets us know what remains in the shadows in our project.

As a rule of thumb, we consider that a code coverage of over 80% is a sign of a well-tested project, to which it will then be easier to add new features.

To find out the coverage rate of your project, you can use the python libraries [`coverage`](https://coverage.readthedocs.io/en/7.3.2/) and [`pytest_cov`](https://pytest-cov.readthedocs.io/en/latest/), which you need to install either from the command line or from your IDE.

```bash
# Either
pip3 install coverage
# or
pip install coverage
```
```bash
# Either
pip3 install pytest-cov
# or
pip install pytest-cov
```

Then, you need to go in the project working directory and run:

```bash
pytest --cov=dataviz --cov-report html test_*.py`
```

This command tests the files contained in the `dataviz` folder. It creates an html report and places it in the `htmlcov` directory, using the tests in this directory of the form `test_<foo>.py`.

When you open the `index.html` file in the `htmlcov` directory, you'll see a summary of the coverage test, which should be good given the TDD approach we've used. Clicking on each of the files will also display a report for that particular file.

![testcoverage](./Images/testcoverage.png)

![testcoverage](./Images/testcoveragebis.png)


#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 5: a first code coverage of my project </span> 


## On version control

<span style='color:blue'> For the remainder of the project, you are asked to:</span> 

+ <span style='color:blue'>Commit as soon as a feature or a sub-feature is implemented.</span> 
+ <span style='color:blue'>Tag your last commit at the end of the day </span> 
+ <span style='color:blue'>Review you code as a team for each feature.</span>
+ <span style='color:blue'>Put your code in the stable `main` branch.</span>
+ <span style='color:blue'>Push your code on your remote repo on GitLab.</span> 
+ <span style='color:blue'>Make a code coverage test at the end of each day, and push the summary on your distant GitLab repo.</span>

You can now move on to [**Feature 3**: Getting started with pandas.](./S1_pandas.md)






