# Feature 8: Getting started with Textblob


[Textblob](https://textblob.readthedocs.io/en/dev/quickstart.html) is a python library that allows you to do text analysis and sentiment analysis very quickly. An alternative is the [spacy](https://spacy.io/) library which also has many interesting features. These are two libraries that rely on modern machine learning and natural language processing tools (language models, distributed representations, deep learning).


To begin, you must install it and attach it to your project with, for example, the command.

`pip3 install textblob` or `pip install textblob`

## Getting started with `textblob`

Watch and do [the official tutorial](https://textblob.readthedocs.io/en/dev/quickstart.html) to understand the main functions.

To practice with this library, write a function `review_to_words` which allows you to extract the vocabulary from a review by retrieving the [words](https://textblob.readthedocs.io/en/dev/quickstart.html #tokenization), unique and [lemmatized](https://textblob.readthedocs.io/en/dev/quickstart.html#words-inflection-and-lemmatization). We can also delete frequent words or `stop-words from the list obtained, for example using the list provided [here](https://gist.github.com/sebleier/554280).

From this function, write a function `corpus_frequent_words` which displays the histogram of the 40 most frequent words in your small review corpus.




**Remember to test and document your code!!!**

This step will complete your feature.

We can now move on to [**feature 9**: Opinion analysis](./S4_opinion.md)
