
#Feature 14: WordCloud on your review set


Here, it involves applying a WordCloud type visualization to a set of reviews (your small corpus). This will therefore involve displaying the vocabulary, i.e. all the unique words from a set of reviews while trying to take their frequency into account.
It will therefore be necessary to use the functionalities of `pandas`, `textblob` and the previous library. For example, you can take inspiration from this [example](https://amueller.github.io/word_cloud/auto_examples/frequency.html).

After having done this work and as it closes an objective, it will be necessary to redo the work done at the end of the MVP, that is to say all the work around making this project reusable and executable by a third party. Typically, at this stage, one of your supervisors could clone your repository and be able to run your code without problem. This requires having well documented the README, with instructions for use and all the machinery allowing this possible execution.

Little reminder as usual.

+ <span style='color:blue'>Make a commit as soon as the development of a feature or sub-feature is finished.</span>
+ <span style='color:blue'>Tag your last commit at the end of each day </span>
+ <span style='color:blue'>Push your code to your remote repository on GitLab.</span>
+ <span style='color:blue'>Do a code coverage test at the end of each day and push the results obtained to your remote repository on GitLab.</span>


We have just completed an objective so as at the end of objectives 1 and 2, it is necessary to carry out a code review phase and its packaging so that it can be reused by others but also in the objective of a demonstration.

We can now move on to the last part which aims to set up a learning chain for sentiment analysis in documents. In our case, we will consider that the tweets are the documents.



You can therefore go to [Objective 4 of the project](https://gitlab-student.centralesupelec.fr/paul.tourniaire/boge_coding_weeks/-/blob/main/cs_codingweeks_dataviz/TemplateProject_dataviz.md?ref_type=heads#objective-4-add-sentiment-analysis-through-machine-learning).

