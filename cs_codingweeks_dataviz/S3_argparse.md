# Feature 7: A command line interface with `argparse`

The objective here is to allow a given user to be able to launch the data visualization program from the command line by specifying the different parameters (for example the type of visualization desired, the type of analysis, or even the subject for which he wishes to have statistics or visualizations).

To do this, we will use the Python `argparse` module, documentation for which is available [here](https://docs.python.org/fr/3/howto/argparse.html).

Quickly learn this module and use it to achieve this functionality.


#### :clap: <span style="color: #26B260">At this point in the project, you have reached MILESTONE 7: Managing a program's parameters from the command line</span>


When you have finished and tested this feature, **don't forget to update your local and remote repository.**

This includes the end of objective 1 since we have our MVP here. A final step then consists of consolidating the developments using tests, comments, possible refactoring work and code review work.

#### :clap: <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 9: consolidate developments as well as MILESTONE 10: Design and implement an MVP</span>


It is therefore possible to have initial user feedback on this data visualization application.


## User feedback

We will try to get user feedback on this MVP. You will therefore take this role and test your application by placing yourself as a user.

What is your user feedback on this MVP? Are there any missing features that you would like to add to your project?

Take the time to discuss it as a group to list the features you would like to add. Give them a priority too.

Following this brainstorming, you will add or update a `TO_DO.md` (or `TO_DO.md`) file to your project in which you will list, in order of priority, these different features.

#### :clap: <span style="color: #26B260">At this stage of the project, you have reached MILESTONE 11: Obtain and analyze initial user feedback with a first MVP </span>

You can now move on to [objective 2](https://gitlab-student.centralesupelec.fr/paul.tourniaire/boge_coding_weeks/-/blob/main/cs_codingweeks_dataviz/TemplateProject_dataviz.md?ref_type=heads#objective-2-more-advanced-analysis-and-visualization-mvp-enhancement)



