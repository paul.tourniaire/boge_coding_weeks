# Feature 14: Setting up a supervised learning chain


## Getting started


You must now have acquired a little more autonomy and therefore for this functionality, we invite you to read one of the many tutorials on this subject on the web such as for example:

+ [tutorial 1](https://stackabuse.com/text-classification-with-python-and-scikit-learn/)
+ [tutorial 2](https://marcobonzanini.com/2015/01/19/sentiment-analysis-with-python-and-scikit-learn/)

Read one of these tutorials and apply it. You will have to try to apply the methodology of dividing into sprints and functionalities seen so far before starting to code. You will upload your breakdown in the form of a `.txt` file to your git repository.