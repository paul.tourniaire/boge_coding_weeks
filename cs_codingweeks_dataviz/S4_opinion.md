# Feature 9: Opinion mining

You will used here to begin your first small review corpus (the one with 100 positive and 100 negative reviews).

You will use here the library `Textblob` to write some analysis of the opinion given in the text of the review. 

Using `Textblob`, the objective is to write the code necessary to analyze opinion for  a given movie. 

To do this, you will need to manage the TextBlob library. Some documentation:

  + Reference documentation [here](https://textblob.readthedocs.io/en/dev/quickstart.html#sentiment-analysis).
  + [Example](https://medium.com/@rahulvaish/textblob-and-sentiment-analysis-python-a687e9fabe96)

Add a package named `opinion_mining` in the `dataviz` project and add a module named `opinion_mining_with_textblob`.

In this module :

+ write a function that given the number of a review, take the text of the review and use textblob in order to infer the sentiment associated to the review.  You will have to return a categorical value, i.e. `negative`, `neutral` or `positive ` according the score of the polarity given by  `textblob`.
+ write a function that infers the sentiment for all the reviews of the corpus
+ write a function that compares the textblob analysis with the annotations that are present in the corpus to evaluate the precision of the analysis approach. This function can for instance return the number of differences between the opinion given by textblob and the one annoated in the corpus.

**Remember to test and document your code!!!** 

Now that we have new analysis tools, we can move on to [**Feature 10**: Display analysis result.](./S5_displayresult.md)



