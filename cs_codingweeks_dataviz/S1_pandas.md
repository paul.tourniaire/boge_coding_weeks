# Feature 3: Getting started with pandas


Now it's a matter of using [pandas](https://pandas.pydata.org/) to create a `dataframe` containing the information that we will use for the analysis itself. Being able to have the data in the form of a dataframe will make our work easier.

To do this, you must first install [`pandas`](https://pandas.pydata.org/) either from the command line using the `pip3 install pandas` command or directly from your editor.

If you don't know this library, take the 15 min needed to do this [very short tutorial](https://pandas.pydata.org/pandas-docs/stable/10min.html).
You can also take the time to create this [python notebook](./Notebooks/LAB1_data_preprocessing.ipynb).

Once this increase in skills is done, add a `load_review_corpus_in_dataframe` function in the `corpusutils` module which allows you to load the corpus in the form of a pandas `dataframe`. 

Here, we will use two different corpus or dataset in order to have enough information for both visualization and information mining.








Don't forget to test your code, its coverage by tests and:

+ <span style='color:blue'>Make a commit as soon as the feature or sub-feature is finished.</span>
+ <span style='color:blue'>Push your code to your remote repository on GitLab.</span>

You can then move on to [Feature 4](S2_statistics.md).


