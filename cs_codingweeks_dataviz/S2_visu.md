# Feature 5: Visualization of corpus statistics

This involves visually displaying the results of your analyzes on the corpus. For this MVP, we will take advantage of the `matplotlib` python library that you have already used in the first programming course.

Tutorials are available:

+ [tutorial 1](https://scipy-lectures.org/intro/matplotlib/index.html)
+ [tutorial 2](https://realpython.com/python-matplotlib-guide/) and [here](https://realpython.com/courses/python-plotting-matplotlib/)


and of course the official documentation [here](https://matplotlib.org/tutorials/index.html).

This part is free. You can make it however you want.
It is expected at least:

* a visualization in the form of a pie chart of the corpus showing the number of movies by genre 
* a representation in the form of a histogram which shows the average imdb scores for different genres.
* a representation of the average gross earnings for different genres and for different countries. 
 
The first visualization will be quite easy to do. For the second, you will most certainly need to add functions and most certainly **add an `data_analysis` module.** Indeed, to be able to construct the representation, you must already:

* For a given genre, count the number of movies, compute the average of the imdb scores and so on.
*...

This step shows us that the division into sprints and functionality of our project could be revised, in particular to add an analysis functionality.


You will need to carefully document your programs, test them and, as usual, take the necessary steps to manage your version with git and gitlab.


Now we have almost all the functional building blocks of the MVP. It remains to allow easy execution of our visualization tool, by allowing its configuration but also its easy execution.

This is sprint 3 which consists of finalizing the MVP with [**Feature 6: A main program**](./S3_main.md)















