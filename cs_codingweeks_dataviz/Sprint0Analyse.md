# DataVisualization - Sprint 0: Problem analysis

One of the first steps in any programming and software development work, whatever the development methodology used, consists of carrying out a quick **needs analysis**, before any implementation phase.

This analysis aims to identify the main functionalities to be developed to achieve the desired behavior of the developed system. This first list of features does not need to be exhaustive or fixed, but it will allow you to build your first developments.


## Needs analysis: the main functionalities

The objective here is to be able to promote, through a visualization dashboard, the results of an analysis. This is a very important need and one which finds numerous applications: see for example [this article](https://www.freecodecamp.org/news/why-data-visualization-is-important-for-ux-design/).

We are interested here in social data, a set of reviews relating to movies. Allowing a visualization of the opinion of the social sphere concerning this type of reviews could, for example, be useful to media providers to improve future evolutions of their platform.


The **[MVP (Minimum Viable product)](https://medium.com/creative-wallonia-engine/un-mvp-nest-pas-une-version-simplifi%C3%A9e-de-votre-produit- 89017ac748b0)** of this project will consist of delivering a first version of the visualization tool, that is to say a tool implementing the classic data analysis chain: their recovery, their analysis and the visualization of this analysis with one or two [common visualization techniques](https://www.datacamp.com/blog/data-visualization-techniques). It is a classic chain of **data valorization**.


In particular, the MVP:

+ **Will allow you to retrieve still existing tweets given in the corpus**.
+ **Will use the annotations provided in the corpus for a very simple first data analysis**.
+ **Will be based solely on the analysis of the textual content of reviews and others informations in the datasets**
+  **Will allow processing textual data.**
+ **Will display the analysis results in the form of common visualizations such as histograms or pie charts**.

 
Other features can of course be added later to improve your project after finishing this MVP. By quickly building an end-to-end chain, however, you will be able to quickly get feedback on your product and its features and use this feedback to improve it.

You can now continue with [Sprint 0: Reflection on design](./Sprint0Conception.md).