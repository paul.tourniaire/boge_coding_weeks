# Feature 13: Getting started with WordCloud


We will use [this library](https://github.com/amueller/word_cloud).

Instructions for installing it are in [`README.md`](https://github.com/amueller/word_cloud/blob/master/README.md).

Install this library and read the associated documentation to get started. It's [here](https://amueller.github.io/word_cloud/).


In particular, you can look at the [gallery of examples](https://amueller.github.io/word_cloud/auto_examples/index.html) and for each example, look at the associated code to familiarize yourself with it.

If you think you can use it, you can move on to [**Feature 14**: WordCloud on your review corpus](./S7_wordcloudtweet.md)