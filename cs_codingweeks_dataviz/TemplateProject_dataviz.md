# Program a data analysis and visualization application with the python ecosystem.


The **objective** of this mini-project is to develop, in a very incremental way, a data analysis and visualization application with the python ecosystem. In particular,  you will on an emotion analysis application and a visualization of the latter using a well-known corpus, the [IMDB dataset](https://developer.imdb.com/non-commercial-datasets/).

This dataset is available [here](./Data/).


The objective could be, for example, to visualize this corpus in the form of a dashboard like this:

![Opinion Mining](./Images/sentiment.png)


Through this project, you will discover the Python ecosystem for data science, as well as several principles of the [*Software Craftsmanship* paradigm](https://github.com/jnguyen095/clean-code/blob/master/Clean.Code.A.Handbook.of.Agile.Software.Craftsmanship.pdf). 

## Organization of the mini-project

This mini-project is broken down into several objectives, themselves broken down into **sprints** and **features**. The notion of sprint refers to the [agile method](https://en.wikipedia.org/wiki/Agile_software_development). A sprint corresponds to an interval of time during which the project team will complete a certain number of tasks.

This breakdown has been done for you for this project, but it's one of the first steps to be taken for any software development project, at least macroscopically. Think about it next week!


### **Objective 1 (MVP): A first visualization with `matplotlib`** 



The aim of the first two days is to build and implement a simple version of the IMDB data analysis and visualization tool that could be described as a **[MVP (Minimum Viable product)](https://en.wikipedia.org/wiki/Minimum_viable_product)**. 

This MVP concept was popularized by Eric Ries, author of [The Lean Startup](http://theleanstartup.com/), a specific approach to starting up a business and launching a product. The figure below explains the concept.

![MVP](./Images/mvp.png)

 + **Sprint 0** :
	 + [Technical base installation](./Sprint0Installbis.md)
	 + [Requirement analysis](./Sprint0Analyse.md) 
	 + [Design review](./Sprint0Conception.md)

 + **Sprint 1 : Data manipulation and pre-processing** 
 	+ [**Feature 1** : Getting started with the IMDB dataset](./S1_corpuspriseenmain_en.md)
 	+ [**Feature 2**: Data utils : some useful tools to collect important and useful information](./S1_imdbdataacess.md)
 	+ [**Feature 3**: Getting started with the `pandas` library](./S1_pandas.md)
 		
 + **Sprint 2** : **Corpus statistics**.
 	+ [**Feature 4**: Corpus statistics](./S2_statistics.md)
 	+ [**Feature 5**: Visualisation of corpus statistics](./S2_visu.md)

 + **Sprint 3** : **Our first MVP**
 	+ [**Feature 6**: A `main` program](./S3_main.md)
 	+ [**Feature 7**: A command-line interface with `argparse`](./S3_argparse.md)


### Objective 2: More advanced analysis and visualization (MVP enhancement) 

 + **Sprint 4**: **Add automatic opinion analysis with [Textblob](https://textblob.readthedocs.io/en/dev/)**

 	+ [**Feature 8**: Getting started with `Textblob`](./S4_textblob.md)
 	+ [**Feature 9**: Opinion analysis](./S4_opinion.md)

+ **Sprint 5** : **A Simple visualization with [`seaborn`](https://seaborn.pydata.org/)**	

 	+ [**Feature 10** : Display analysis result with `seaborn`](./S5_displayresult.md)



### Objective 3: A first dataviz app (MVP enhancement)

+ **Sprint 6**: **A visualization application with [Dash](https://plot.ly/)**
	+ [**Feature 11**: Your first application with Dash PlotLy](./S6_dash.md) 


+ **Sprint 7**: **Word tagging with [`WordCloud`](https://github.com/amueller/word_cloud)!** 
	+ [**Feature 12**: Getting started with `WordCloud`](./S7_wordcloud.md)
	+ [**Feature 13**: `WordCloud` on IMDB reviews](./S7_wordcloudtweet.md)


### Objective 4: Add sentiment analysis through machine learning 

This is additional work. It goes beyond the objectives of the first week of coding weeks.

Typically here, in order not to be dependent on content analysis libraries such as `textblob` or `spacy`, you might want to develop your own tool for predicting the opinion of a piece of text. 

Since we have the `IMDB` corpus, which is annotated, we could then implement a supervised learning chain using the `scikit-learn` library. 


We'll now turn our attention to the latter feature:

+ [**Feature 14** : Setting up a supervised learning chain](./S8_learning.md)






 



 





