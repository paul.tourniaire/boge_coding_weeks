#Feature 10: Opinion mining visualization


This involves visualizing the result of our analysis visually. For this, we will use the library [`seaborn`](https://seaborn.pydata.org/examples/index.html) which has many visualization tools and methods and which will improve our visualization module built for the moment with `matplotlib`.


You can take a look at the [gallery](https://seaborn.pydata.org/examples/index.html) of [`seaborn`](https://seaborn.pydata.org/examples/index.html) to get an idea of the type of visualizations available to you.

Take control of this library and create different visualizations from your review analysis and imdb 5000 corpus with Seaborn.



To do this, you must choose the type of visualization desired [here](https://seaborn.pydata.org/tutorial.html) and reproduce by adapting the code for the creation of a given type of graph, for example [here](https://seaborn.pydata.org/tutorial/categorical.html#distributions-of-observations-within-categories).





Before finishing:

+ <span style='color:blue'>Make a commit.</span>
+ <span style='color:blue'>Tag your latest commit </span>
+ <span style='color:blue'>Push your code to your remote repository on GitLab.</span>
+ <span style='color:blue'>Perform a code coverage test and push the resulting report to your remote repository on GitLab.</span>

We now have a new version of the product including a review analysis tool that can be evaluated and qualified with annotations from the `imdb` corpus. Take the time to take time to discuss it and suggest how to improve it. For example, you can put a `TO_DO.md` file in your `git` repository listing the different features you want.


The heart of our application being visualization, we will see how to further improve this visualization, in particular by making it a WEB application using [dash](https://dash.plotly.com/introduction).

This is [**objective 3**](https://gitlab-student.centralesupelec.fr/paul.tourniaire/boge_coding_weeks/-/blob/main/cs_codingweeks_dataviz/TemplateProject_dataviz.md?ref_type=heads#objective-3-a-first-dataviz-app-mvp-enhancement).

