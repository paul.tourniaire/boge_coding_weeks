# Feature 4:  Statistics on the corpus


The objective here is to take advantage of pandas dataframes to carry out initial statistical analyzes and obtain initial information on the corpus.

For this feature, to make it interesting, you will work on two different dataset. A first dataset of the review dataset (the small ones you have built in the previous steps) but also the one available [here](./Data/IMDB_Dataset.csv).

We will also work on this dataset, the Imdb 5000 Movie Dataset, available [here](./Data/5000_movies.csv) and that can be used in order to build [this kind of visualisation](https://peaceful-tesla-ed60e6.netlify.app/datavis/imdb-genres).
This dataset contains 28 variables for 5043 movies, spanning across 100 years in 66 countries. There are 2399 unique director names, and thousands of actors/actresses.

The variables are the following

![Variables](./Images/imdb5000.png)




## Feature 4.1 : Adding the load functions for the different datasets.


In the  `corpusutils` module, add the function `load_5000_movie_corpus_in_dataframe` function in the `corpusutils` module which allows you to load the Imdb 5000 Movie Dataset in the form of a pandas `dataframe`. 

Identically, add the function `load_full_review_corpus_in_dataframe` function in the `corpusutils` module which allows you to load the corpus available [here](./Data/IMDB_Dataset.csv) in the form of a pandas `dataframe`. 

:point_right : Hint : See the [ `read_csv`](https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html) function of the pandas library for these data loading functions. 



Take time to check the obtained dataframes and test your loading functions


## Feature 4.2 : Cleaning the corpus.

First, add a module `data_cleaning` in the `dataviz` project.  

For this feature, you will first work on the Imdb 5000 Movie Dataset. Your work here is to write the function needed to clean the data and thus the corpus. This step is a very important step in every data science project.

Here, in the module `data_cleaning`, add the following functions :

+ `remove_duplicate` : function to remove Duplicates. Indeed, in the IMDB data, we have some duplicate rows (to check : 45 duplicated rows). The function should return a dataframe with unique rows. You can test your code by checking that 45 rows have been removed.
+ `check_missing_values`: function that checks how many values are null in each column and that return a `dict` which keys are the names of the column and which values are the number of missing values. 
+ `split_genres`: function that build a new dataframe with a column for each movie genre. Each record of genres is combined with a few types, which will cause the difficulty of analyzing. To prevent that, this function must create a new column for each genre with the value 0 or 1 according to the tagging of the movie with the targeted genre.


## Computing some statistics for the Imdb 5000 Movie Dataset

First, add a module `corpus_statistics` in the `dataviz` project. Then, in this module write the functions that enable to compute some statistics on the corpus as for instance :  

  + the number of unique film directors
  + the number of unique actor_1_name
  + the number of movies by genre
  + the average of the budget by genre, by film director, by actor_1_name
  + and many others 

 
Here, it is necessary to redo a design stage with all the stakeholders to be certain of extracting the right information.
Take the time to do this design work as a group, document it and propose a breakdown of this functionality into sub-steps. It will be necessary to commit and synchronize this documentation work.

Then write the programs necessary for this statistical analysis of the corpus. This code will be put in a `corpus_statistics` module that you will add to your project.

## To finish

+ <span style='color:blue'>Make a commit of this module.</span>
+ <span style='color:blue'>Tag this last commit </span>
+ <span style='color:blue'>Do the synchronization step</span>
+ <span style='color:blue'>Perform a code coverage test and push the resulting report to your remote repository on GitLab.</span>



You can then move on to the functionality [**Feature 5**: Visualization of corpus statistics.](./S2_visu.md)
