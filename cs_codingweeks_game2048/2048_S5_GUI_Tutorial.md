# Skill enhancement: Graphical interfaces in python


There is a vast number of frameworks for developing graphical user interfaces in python. A list of these tools is available [here](https://wiki.python.org/moin/GuiProgramming).


In particular, the standard python library includes a module called **Tkinter (Tk interface)** which can be used to develop graphical interfaces. We'll just use this module in this part of the project, which doesn't require installation.

Documentation for this module is available [here](https://wiki.python.org/moin/TkInter).

We'll now take a quick tutorial (inspired by the OpenClassRooms course [Learn to program in python ](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/234859-des-interfaces-graphiques-avec-tkinter) and this [tutorial](https://www.python-course.eu/python_tkinter.php)) to familiarize ourselves with this module and tool.

Vincent Maillol's [tutorial](https://vincent.developpez.com/cours-tutoriels/python/tkinter/apprendre-creer-interface-graphique-tkinter-python-3/) is also highly recommended.

<!-- Another well-written Tkinter documentation in French is available [here](http://tkinter.fdex.eu/index.html) -->


## A first graphical interface using tkinter

Add a `tuto_GUI.py` file to your project and copy the code below to create a window displaying the message *Hello World!*.

```python
from tkinter import *

# Window creation, root of the interface
window = Tk()

# Creation of a label (text line) that says Hello World ! and with as first parameter the previous window
label_field = Label(window,text="Hello World !")

# Display of the label
label_field.pack()

# Running of the Tkinter loop that ends when we close the windw
window.mainloop()
```

Run this code and you should get the following window:

![Tkinter Hello World](./Images/hello.png)

The `config()` method is used to configure widgets. For example, in the previous example, the following two lines

``` python
label_field = Label(window,text="Hello World !")
label_field.config(text="Hello World !")
```

are equivalent to:

```
label_field = Label(window,text="Hello World !")
```

`pack` is a geometry manager that manages widget composition.

Widget placement is relative to its container (with the `side` configuration option), using cardinal placement (`Tkinter.TOP`, `Tkinter.LEFT`, `Tkinter.RIGHT`, `Tkinter.BOTTOM`). By default, a widget is attached at the top of, or below existing widgets.

If maximum space occupation is required, use the `expand=YES` configuration option. The direction of maximum occupation is specified by `fill`. It can be width (`Tkinter.X`), height (`Tkinter.Y`) or both (`Tkinter.BOTH`).

Other position managers are available in Tkinter: the `grid` manager, which divides the container into a grid and places the widget in a cell of this grid, and the `place` manager, which positions the widget to the nearest pixel, but is difficult to use in practice.


## Discovery of the main Tkinter widgets

Tkinter features a number of widgets (graphical objects such as buttons, text fields, checkboxes, progress bars, etc.). Here, we present the main ones.

### Label

In the previous example, we saw the `Label` widget, which displays text in the main window. This text cannot be modified by the user.

### Button

Buttons are clickable widgets that can trigger actions or **commands**.

Copy the following code into your test file and run it. What does it do?


``` python
import tkinter as tk


def write_text():
    print("Hello CentraleSupelec")

root = tk.Tk()
frame = tk.Frame(root)
frame.pack()

button = tk.Button(frame,
                   text="QUIT",
                   activebackground = "blue",
                   fg="red",
                   command=quit)
button.pack(side=tk.LEFT)
slogan = tk.Button(frame,
                   fg="blue",
                   text="Hello",
                   command=write_text)
slogan.pack(side=tk.LEFT)

root.mainloop()

```

Please note that for MAC OSX users, colors may not be taken into account. Guidance is available [here](https://stackoverflow.com/questions/1529847/how-to-change-the-foreground-or-background-colour-of-a-tkinter-button-on-mac-os).

This example illustrates how to initiate processing from a graphical interface. Here, the `quit` command is associated with the first button to exit the window and the command defined by the `write_text()` function is associated with the second button.


### Entry

The `Entry` widget is a data entry widget. It is used to collect user input. It takes as parameter an object of type `StringVar`, which will be used to retrieve the input text. If the `StringVar` is updated, the input field is also modified.

Test the code below:

``` python
from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial

def update_label(label, stringvar):
    """
    Updates the label text using a StringVar.
    """
    text = stringvar.get()
    label.config(text=text)
    stringvar.set('thank you!')

root = Tk()
text = StringVar(root)
label = Label(root, text='Your name')
entry_name = Entry(root, textvariable=text)
button = Button(root, text='click',
                command=partial(update_label, label, text))

label.grid(column=0, row=0)
entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)

root.mainloop()

```

The `Button` widget takes as parameters a text to be displayed and a function to be executed.

The function to be executed takes no parameter, and to use a function that has already been written, it must be encapsulated with the [`partial`](https://docs.python.org/3.12/library/functools.html) module. 

With the help of the tutorial available [here](https://www.python-course.eu/tkinter_radiobuttons.php), explore other Tkinter widgets such as [`Radiobutton`](https://www.python-course.eu/tkinter_radiobuttons.php) or [`Checkbutton`](https://www.python-course.eu/tkinter_checkboxes.php).


## Containers

Containers are widgets designed to hold other widgets.

In our examples, we've already used windows (Toplevel) via the `root = Tk()` instruction. 

Containers also include the `Frame` widget, which can be used to group several widgets together. This can simplify their subsequent placement, as in the example below, which you'll need to execute.

```python
from tkinter import Tk, Label, Frame

root = Tk()
f1 = Frame(root, bd=1, relief='solid')
Label(f1, text='I am in F1').grid(row=0, column=0)
Label(f1, text='I too am in F1').grid(row=0, column=1)

f1.grid(row=0, column=0)
Label(root, text='I am in root').grid(row=1, column=0)
Label(root, text='I too am in root').grid(row=2, column=0)

root.mainloop()
```

## Events

It is possible to retrieve events, such as a keystroke or mouse click, to perform special processing. For a widget to be able to process events, it must have focus. Typically, a widget takes focus when it is clicked. Events can then be processed by a function.

The code you'll be testing below displays *Hello* according to clicks or keyboard presses.

```python
from tkinter import *
from pprint import pformat

def print_bonjour(i):
    label.config(text="Hello")

root = Tk()
frame = Frame(root, bg='white', height=100, width=400)
entry = Entry(root)
label = Label(root)

frame.grid(row=0, column=0)
entry.grid(row=1, column=0, sticky='ew')
label.grid(row=2, column=0)

frame.bind('<ButtonPress>', print_bonjour)
entry.bind('<KeyPress>', print_bonjour)
root.mainloop()
```

You can now get to work on your 2048 with graphical interface. Don't hesitate to return to the various tutorials mentioned to deepen your knowledge of Tkinter.

We can now move on to our [Feature 8: Displaying the game grid in a Tkinter window](./2048_S6_affichagegrille.md).
