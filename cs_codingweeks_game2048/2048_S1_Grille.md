# Feature 1: Represent a game grid


The aim of this first feature is to be able to **represent the game grid**. It involves:

+ Create a game space.
+ Create and add 2 tiles (`2` or `4`) to the game space.


Here, we're going to familiarize ourselves with the [**TDD (Test Driven Development)**](https://fr.wikipedia.org/wiki/Test_driven_development) approach, which consists in specifying the expected behavior via a test before actually implementing it. The principle is therefore to first write the test and then the simplest possible code that allows the test to pass and thus satisfy the specified behavior. The code can then be improved. The idea is to focus on functionality rather than code.

We'll start with a step-by-step approach, and you'll gradually gain autonomy in this area. 

## Create a game area

To create the game area, we need to identify the data to be manipulated, i.e., the data required to represent the game grid, which is a container of tiles, and the contents of the tiles. The next step is to select the data structures to be used.

1. **Acceptance criteria**

One of the first tasks here is to research and list all the criteria that will enable us to correctly meet the needs that the *Create a game area* functionality is supposed to cover. These criteria are called **acceptance criteria**. Here, it's very simple: the acceptance criterion for *Create a game space* is to **have a game space with two tiles (`2` or `4`) randomly placed in the game space**.

2. **TDD**

By TDD **(Test Driven Development)**, we mean that the first line of your program must be in a test file. In our case, we'll be using the [`pytest`](https://docs.pytest.org/en/latest/) module, which must be added to your project. The principle of TDD is based on 3 complementary steps.
 
   + First step (**<span style='color:red'>RED</span>**): Write a test that fails.
   + Second step (**<span style='color:green'>GREEN</span>**): Write the most simple code that secures a success to the test.
   + Third step (**REFACTOR**): Improve the source code.

We're going to apply this method to the grid creation functionality of the 2048 game.

#### **<span style='color:red'>RED STEP</span>**

Our first test will be to make sure that when our 2048 game is created, it has an empty game grid of dimension *4 x 4*.

```python
from game2048.grid_2048 import create_grid
from pytest import *


def test_create_grid():
    assert create_grid() == [[' ',' ',' ', ' '],[' ',' ',' ', ' '],[' ',' ',' ', ' '],[' ',' ',' ', ' ']]

```

Copy this code into a `test_grid_2048.py` file and add it to your project. This test should fail, since in the current state of the project, the code for `create_grid` does not exist, and we therefore get the following error when executing the code <span style='color:red'>`ImportError: cannot import name 'create_grid'`<span>

#### **<span style='color:green'>GREEN STEP</span>**

We're now going to write the code to make this test succeed as quickly as possible.

All we need to do is:

+ Create the `grid_2048.py` file in the `game2048` package.
+ Create and complete a `create_grid` function in the `grid_2048.py` file such that the previous test succeeds.

We may, for instance, define the following function in the `grid_2048.py` file:

```python
def create_grid():
    s = []
    for i in range(0,4):
        s.append([' ',' ',' ', ' '])
    return s
```

Your test should turn green after this **<span style='color:green'>GREEN STEP</span>**


#### **<span style='color:black'>REFACTOR STEP</span>**

The last step is a [refactoring](https://refactoring.com/) one, to put in place if necessary.

[Refactoring](https://en.wikipedia.org/wiki/Code_refactoring) (or _code_ refactoring) is a programming principle that consists in changing the internal structure of a program without changing its observable behavior. This step should always be carried out when the various tests are green, but is not mandatory. Its main purpose is to improve the **quality of the code**, for example by improving:
 
+ **the design**: break down your code into functions, modules and classes to make it as simple as possible.
+ **code readability**: here, you need to take the time to apply the principles of [clean code](https://cleancoders.com) introduced by Robert C. Martin in his book of the same name, one of which is that of the Boy Scouts (*"The Boy Scout Rule "*): *Always leave a place in better condition than you found it "*.
 
In our case, one of the first principles is to check the correct naming (variables, functions, packages, classes and so on) and the presence of comments in our code. 
 
You'll find [here](https://github.com/zedr/clean-code-python#objects-and-data-structures) some principles of clean code transposed to the python language. Take the time to read this site quickly and apply these principles to the code you're about to write.
 
In this **<span style='color:black'>REFACTOR STEP</span>**, we can also work on optimizing program performance if really necessary.

In our case, for example, we could rename the `s` variable. For example, it might be more explicit to name this variable `game_grid`. To carry out this renaming step, we strongly advise you to use your IDE's Refactoring functions. It may not seem very useful here, but with more complex code, it can be very useful.

![Refactorexample](./Images/refactor.png)

To be as generic as possible, we can also choose to define the grid size as a parameter of our `create_grid` function. The code and test code will have to be modified accordingly.

#### **WARNING**

1. **After this step, don't forget to run the tests again to check that the behavior of your code hasn't changed and that everything is still <span style="color:green">GREEN</span>!**

2. We've just completed the *Create a game space* step, so you need to **commit this change in git with an explicit commit message stating the purpose of the step**.


#### **YOUR TURN TO PLAY!**

You will now proceed iteratively to finish writing the *Represent game grid* feature, whose acceptance criterion is as follows: **have a game space with two tiles (`2` or `4`) randomly placed in the game space**.

Each iteration begins with a test that fails, but is only written if it brings new behavior to the system. At the end of each iteration, the question arises as to which test to write next. In our case, we haven't yet dealt with the addition of the two initial tiles to our game grid, so the next test to be written may concern the addition of these two tiles to the game grid.

##### Spawn two tiles of value 2 or 4 in the game grid

1. **Acceptance criteria**

+ Two new tiles are created.
+ These new tiles are positioned in the game grid at coordinates randomly selected from the possible positions.

2. **TDD**

Applying the TDD principle described above, you will write the code to complete the writing of this functionality.
In particular, you are advised to iterate over the tests described below. These tests correspond to each iteration of the **<span style='color:red'>RED STEP</span>**. You'll be asked to complete the other two steps in the TDD cycle to turn these tests green and improve the quality of your code.

+ **Iteration 1 - Test: A tile with the value `2` is placed at a given position** 

```python
def test_grid_add_new_tile_at_position():
    game_grid=create_grid(4)
    game_grid=grid_add_new_tile_at_position(game_grid,1,1)
    assert game_grid==[[' ',' ',' ', ' '],[' ', 2 ,' ', ' '],[' ',' ',' ', ' '],[' ',' ',' ', ' ']]
```


+ **Iteration 2 - Test: The value of a new tile is either `2` or `4`**. 


```python
def test_grid_add_new_tile_at_position():
    game_grid=create_grid(4)
    game_grid=grid_add_new_tile_at_position(game_grid,1,1)
    tiles = get_all_tiles(game_grid)
    assert 2 in tiles or 4 in tiles

def test_get_value_new_tile():
    assert get_value_new_tile() in {2, 4}


def test_get_all_tiles():
    assert get_all_tiles( [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64], [1024,2048,512, ' ']]) == [0,4,8,2,0,0,0,0,0,512,32,64, 1024,2048,512,0]
    assert get_all_tiles([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]]) == [16, 4, 8, 2, 2, 4, 2, 128, 4, 512, 32, 64, 1024, 2048, 512, 2]
    assert get_all_tiles(create_grid(3))== [0 for i in range(9)]
```

More precisely, a new tile added to the game has a 90% chance of having the value 2 and a 10% chance of having the value 4. We won't test this aspect, but your code should be able to add tiles to the game that respect this property.

 + **Iteration 3 - Test: a new tile is randomly placed in an available slot in the game grid.**

```python
def test_get_empty_tiles_positions():
    assert get_empty_tiles_positions([[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
    assert get_empty_tiles_positions([[' ', 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
    assert get_empty_tiles_positions(create_grid(2))==[(0,0),(0,1),(1,0),(1,1)]
    assert get_empty_tiles_positions([[16,4,8,2], [2,4,2,128], [4,512,32,64], [1024,2048,512,2]])==[]

```

```python
def test_get_new_position():
    grid = [[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]]
    x,y=get_new_position(grid)
    assert(grid_get_value(grid,x,y)) == 0
    grid = [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64], [1024,2048,512, ' ']]
    x,y=get_new_position(grid)
    assert(grid_get_value(grid,x,y)) == 0
```

```python
def test_grid_add_new_tile():
    game_grid=create_grid(4)
    game_grid=grid_add_new_tile(game_grid)
    tiles = get_all_tiles(game_grid)
    assert 2 in tiles or 4 in tiles
    
```

Here, you are required in the **<span style='color:black'>REFACTOR STEP</span>** to change the name of the `grid_add_new_tile_at_position` function for `grid_add_new_tile`. You can use once again the Refactoring features of your IDE to do it faster.

 + **Iteration 4 - Test: add 2 tiles in the game area.** 

```python
def test_init_game():
    grid = init_game(4)
    tiles = get_all_tiles(grid)
    assert 2 in tiles or 4 in tiles
    assert len(get_empty_tiles_positions(grid)) == 14
```

At the end of these 4 iterations, you should have written all the code needed to realize feature 1: **Display the game grid**.

#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 3: Write code in a TDD fashion </span> 



**<span style='color:blue'>Commit your changes now with git. It is also time to share and review your code with the other members of the team.</span>**

#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 4: a first real use of git and gitlab with my group</span> 

## On code coverage

Code coverage corresponds to the percentage of code that is covered by tests. Therefore, it lets us know what remains in the shadows in our project.

As a rule of thumb, we consider that a code coverage of over 80% is a sign of a well-tested project, to which it will then be easier to add new features.

To find out the coverage rate of your project, you can use the python libraries [`coverage`](https://coverage.readthedocs.io/en/7.3.2/) and [`pytest_cov`](https://pytest-cov.readthedocs.io/en/latest/), which you need to install either from the command line or from your IDE.

```bash
# Either
pip3 install coverage
# or
pip install coverage
```
```bash
# Either
pip3 install pytest-cov
# or
pip install pytest-cov
```

Then, you need to go in the project working directory and run:

```bash
pytest --cov=game2048 --cov-report html test_*.py
```

This command tests the files contained in the `game2048` folder. It creates an html report and places it in the `htmlcov` directory, using the tests in this directory of the form `test_<foo>.py`.

When you open the `index.html` file in the `htmlcov` directory, you'll see a summary of the coverage test, which should be good given the TDD approach we've used. Clicking on each of the files will also display a report for that particular file.

![testcoverage](./Images/testcoverage.png)

![testcoverage](./Images/testcoveragebis.png)


#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 5: a first code coverage of my project </span> 

## On version control

<span style='color:blue'> For the remainder of the project, you are asked to:</span> 

+ <span style='color:blue'>Commit as soon as a feature or a sub-feature is implemented.</span> 
+ <span style='color:blue'>Tag your last commit at the end of the day </span> 
+ <span style='color:blue'>Review you code as a team for each feature.</span>
+ <span style='color:blue'>Put your code in the stable `main` branch.</span>
+ <span style='color:blue'>Push your code on your remote repo on GitLab.</span> 
+ <span style='color:blue'>Make a code coverage test at the end of each day, and push the summary on your distant GitLab repo.</span>

You can now move on to [**Feature 2**: Display the game grid.](./2048_S1_Display_Grille.md).
