# Feature 2: Display the game grid


The aim of this second feature is to be able to **display the game grid cleanly**, whatever the value of the tiles making up the game or whatever the theme chosen.

For this MVP, we've opted for a simple display of the game on the console.

![Vue du jeu 2048](./Images/2048_maquette.png)

For the rest of the project, we leave you free to apply the TDD method or not. The following steps and associated tests are provided as a guide to help you design your program.


## Step 1: Display an initial grid

Th test could be as follows:

```python
def test_grid_to_string():
    grid = [[' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [2, ' ', ' ', 2]]
    a ="""
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
| 2 |   |   | 2 |
 === === === ===
    """
    assert grid_to_string(grid, 4) == a[1:-1] # on enleve le premier et le dernier retour chariot
```

## Step 2: Take the tile width into account

Based on the previous step, write the test and the code to pass the test for displaying the game grid, taking into account the width of the tiles and therefore the maximum length of the numbers or symbols to be displayed in each square.
To be uniform in your solutions, we suggest you write:

+ a function `grid_to_string_with_size(grid,n)` which transforms the game grid into a well-formatted string, and taking into account the length of the string to print.
+ a function `long_value(grid)` which returns the length of the longest string in the game grid.

## Step 3: Take the theme into account

The final step is to take the theme into account when displaying the grid. Remember that the theme is provided in the form of the variable `THEMES` given below.

```python
THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}
```

Here, your code should be able to pass the tests below, which you can of course complete to be more exhaustive:

```python
def test_long_value_with_theme():
    grid =[[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]]
    assert long_value_with_theme(grid,THEMES["0"]) == 4
    assert long_value_with_theme(grid,THEMES["1"]) == 2
    assert long_value_with_theme(grid,THEMES["2"]) == 1
    grid = [[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512, 2]]
    assert long_value_with_theme(grid,THEMES["0"]) == 4
    assert long_value_with_theme(grid,THEMES["1"]) == 2
    assert long_value_with_theme(grid,THEMES["2"]) == 1
    
```

and

```python
def test_grid_to_string_with_size_and_theme():
    grid=[[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 64], [1024, 2048, 512, 2]]
    a="""
=============
|Be|He|Li|H |
=============
|H |He|H |N |
=============
|He|F |B |C |
=============
|Ne|Na|F |H |
=============
"""
    assert grid_to_string_with_size_and_theme(grid,THEMES["1"],4)== a[1:-1]
    
```

You just implemented a new feature. Do not forget to:

+ <span style='color:blue'>Commit.</span> 
+ <span style='color:blue'>Review and synchronize your code.</span> 

Now that we have a grid, we can allow for some interaction, by giving a player the possibility to play the 2048. It corresponds to [**Feature 3** : Have someone play](./2048_S2_joueur.md).
   
    
    
    
    
    
    










