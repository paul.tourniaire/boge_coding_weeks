# Feature 3: Have a player

The aim of this feature is to make the game interactive by allowing a player to give game instructions. For this MVP, we'll be content with textual instructions given by the user via the standard input.

Here, we can take some time to think about the design of our game. In **Sprint 1**, we defined a set of functions in the `grid_2048` module of our project, which will enable us to manipulate our game grid. This module has been designed without taking into account the interaction aspect of the game, and we can therefore think of it as generic, whatever the interaction modalities.

Here, for this functionality, we're looking to set up interactions with the player, and depending on whether we're using text-based interactions or a graphical interface and associated events, we're likely to have a solution specific to the type of interaction.

This reflection should lead you to define a new module for this part of the project. We'll call this module `textual_2048` and it involves adding a `textual_2048.py` file to your project. In the interests of software quality, you can also immediately add a `test_textual_2048.py` file in which you define the associated tests.

Remember that in the 2048 game, the player must and can only choose between 4 directions (up, down, left, right). 

This functionality is therefore very simple to implement, using python's predefined `input` function, but once again, we urge you do it iteratively.


## Step 1: Ask the user for a direction.

For this step, we won't apply the TDD approach, as writing a test that tests a function calling `input` isn't easy.

So let's assume you've written the `read_player_command` function in the `textual_2048.py` file, which is the simplest (but not the most robust) solution for the requested functionality.

```python
def read_player_command():
    move = input("Enter your command (l (left), r (right), u (up), b (bottom)):")
    return move
```

To test this function with `pytest` or any other python test utility, you need to be able to **mock** the predefined `input` function. In programming, this is the principle of defining functions whose purpose is to mimic the behavior of other objects we don't want to test (such as a request to an external API).

To fully understand the importance of **mocks** for the test, you may take a look at this [OpenClassRooms tutorial](https://openclassrooms.com/fr/courses/4425126-testez-votre-projet-avec-python/4435224-utilisez-des-mocks). Other tutorials can also help you write this test ([here](https://codefellows.github.io/sea-python-401d7/lectures/mock.html) and [here](https://changhsinlee.com/pytest-mock/)).

To test our `read_player_command()` function with pytest, we have to:

+ Use the [`monkeypatch`](https://docs.pytest.org/en/latest/monkeypatch.html) helper from pytest.
+ Define a `mock_input_return(obj)` function which takes as a parameter the object on which the method is called.
+ Use the `monkeypatch.setattr()`function to simulate a player entering his choice of direction on the command prompt.

With this principle in mind, write the test for the `read_player_command()` function.

#### <span style="color: #26B260">:clap: At this stage, you have reached MILESTONE 7: Create a mock for the test </span> 

## Step 2: Ask the user and check his direction choice.

In the previous step, we didn't mention the case where the user enters an invalid game command. 

In this step, we need to improve the `read_player_command()` function so that if the given command is not correct, the user will be asked to re-enter his choice of game.


## Step 3: Allow the user to choose the size of the game grid as well as the game theme.

The final step for this feature is to allow the user to choose a size for the game grid as well as the display theme.

The first question to ask yourself at this stage concerns the design of your program.

Do you need to define new functions to enable these two choices, or is a modification of the `read_player_command()` function sufficient?

During the game, the `read_player_command()` function will be called on each of the player's moves. What do you suggest?

We're going to define two other functions that will be needed to enter the size of the grid desired by the player (`read_size_grid()`) and the chosen theme (`read_theme_grid()`).

As for every ending of a feature sprint, you'll need to:

+ <span style='color:blue'>Commit</span> 
+ <span style='color:blue'>Review and synchronize </span> 

We can now turn our attention to the design of the game logic itself, by managing the movement of the various tiles.
It corresponds to [**Feature 4**: Move management](./2048_S3_regles.md)






