# 2048 - Sprint 0: Problem analysis

One of the first steps in any software programming and development project, whatever the development methodology used, is to carry out a quick **problem analysis**, before any implementation phase.

The aim of this analysis is to identify the main functionalities to be developed in order to achieve the desired behavior of the system being developed. This initial list of functionalities need not be exhaustive or fixed, but it will enable you to build your first developments.

## Requirements analysis: the main functionalities

The aim here is to enable a player (you!) to **play 2048 as quickly as possible**, so we'll be designing software that lets you play 2048 in console mode, without a graphical interface or player management.

The **[MVP (Minimum Viable product)](https://en.wikipedia.org/wiki/Minimum_viable_product)** of this project will be to deliver a first version of the game in console mode. Your solution will

+ **Allow you to cleanly display a 4 x 4 grid in which 2 tiles with the number 2 or 4 are randomly placed, as well as the tiles resulting from the game**.
+ **Allow you to manage several game themes.** In particular, we'll consider the 3 themes below, which will be supplied in the form of a python dictionary

```python
THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}
```

+ **Allow the user to play by requesting one of several directions (down, up, right, left)**. 
+ **Modify the game grid by taking into account the player's instruction and performing the necessary transformations**. 
+ **Allow you to move on to the next move by randomly placing a 2 or 4 in an empty square**.
+ **Test the end of the game (won or lost) and display the score obtained**.

 
 Other features can of course be added later to improve the game once you've finished this MVP, but by enabling the game, you'll be able to quickly confront your product with its users.

Based on this analysis, we can sketch out an initial mock-up as shown below.

![Vue du jeu 2048](./Images/2048_maquette.png)

You can now continue with the [sprint 0: Design thinking](./Sprint0Conception.md).