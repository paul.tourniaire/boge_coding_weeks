# 2048 - Sprint 0: Design thinking

After this analysis phase, we can take an initial design approach and try to identify the main objects of our MVP.

Reading the [game description on Wikipedia](https://en.wikipedia.org/wiki/2048_(video_game)), the concepts of **grid**, **tile** and **move** are identified. We'll confine ourselves to these three objects in this phase.

## Towards a common language

In this phase of design thinking, to encourage collaborative work and a common understanding between all project members, it's important to define, right from the start of the project, a **common vocabulary** around business terms. In software development jargon, this is known as [**ubiquitous language**](https://tanzu.vmware.com/developer/blog/ubiquitous-language/). This principle stems from the *Domain Driven Design* approach described in the [book](https://lesdieuxducode.com/blog/2019/7/introduction-au-domain-driven-design) of the same name by Eric Evans, and consists in identifying and defining a common language around business terms.

The process consists of producing what are known as [**User Stories**](https://en.wikipedia.org/wiki/User_story) (with all project stakeholders), which represent the user needs to be implemented. This work also helps define the shared language.

A **user story** is nothing more than a simple sentence, in natural language, describing the content of a feature to be developed, specifying the *Who*, the *What* and the *Why*.

 `As <who>, I want <what> so that <why>`

Here, a typical example would be:

+ As a user, I want to be able to **initiate a game** and **play with my keyboard**.
+ As a player, I want to be able to **go back** in the game.
+ As a player, I want to be able to **save a game** and **reload it later**.
+ ...

In the case of the 2048 game, this work may seem tedious and certainly unnecessary, but it will be very useful for many other projects.

As far as shared language is concerned, in the case of the 2048 game, several terms are used to refer to the game grid: *board*, *grid*, *puzzle*... 

For the rest of the project, we'll use the name **grid** to designate the game board, which we'll define as the container for all the tiles in the game.

A **tile** is a game element which can take on a value between 2 and 2048, such that its value is a power of 2, and which can be moved in 4 directions on the game grid.

A **move** is an indication of the direction given by the player to move all the tiles in the game grid.

We're now going to create these three objects from a computer point of view.

#### <span style="color: #26B260"> :clap: At this stage, you have reached MILESTONE 2: Analysis and Conception of my product </span> 

You can now move on to [**Feature 1:** Represent a game grid](./2048_S1_Grille.md)