# Feature 9: Allow game configuration via the graphical interface


In this feature, we're going to add a number of widgets to our main window (the white window left empty) to enable :

+ Choose grid size.
+ Choose the theme.
+ Exit the game.
+ Initialize a game and play.
+ Display a victory message when a game is won.

At the very least, your game could look like this.

![Vue du jeu 2048 en Tkinter](./Images/gui2048_step4.png)

Now you can add more features to your game. Here are a few ideas:

 + Save and load an existing grid.
 + Allow you to backtrack after a disappointing move.
 + Add sounds to your program using [`pygame`](https://www.pygame.org/news).
 + ...

 
## To finish up

+ <span style='color:blue'>Commit your last changes.</span> 
+ <span style='color:blue'>Tag this last commit</span> 
+ <span style='color:blue'>Synchronize</span> 
+ <span style='color:blue'>Make a code coverage test of your MVP and push the obtained summary on your remote GitLab repo.</span>


