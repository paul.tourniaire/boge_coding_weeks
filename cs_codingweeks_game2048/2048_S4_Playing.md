# Feature 6: Game orchestration

We're almost at the end of our MVP. Now it's time to orchestrate the game flow. For this too, we'll proceed in stages. 

## Step 1: Have the computer play randomly.

This involves starting a game with the computer playing in the player's place, so your program should consist of:

+ Initialize a game grid, the game parameters being the default settings.
+ Display it
+ Set up a loop to perform the following actions if the game is not finished
	+ Select a possible random movement
	+ Apply this move to the grid
	+ Add a new tile
	+ Display the grid
+ Test whether the configuration obtained at the end of the game is a winning configuration.

With respect to what you have done so far, do you have all the elements to set up this game program?
 
If so, write down the `random_play()` function that implements this game.


## Step 2: Allow for game configuration.

We want to give the user a choice of starting grid size and theme. To achieve this, write the functions:

+ `ask_and_read_grid_size()`
+ `ask_and_read_grid_theme()`

which can be used to request and enter the user's choices.


## Step 3: Welcome a player.
 
Now we need to enable someone to play. So we write the `game_play()` function, which:

+ Asks the user to choose a grid size and theme.  
+ Initializes a game grid that respects the requested parameters.
+ Displays it
+ Sets up a loop to perform the following actions if the game is not completed
	+ Asks the user to enter his/her choice of direction.
	+ Applies this movement to the grid
	+ Adds a new tile
	+ Displays the resulting grid
+ Tests whether the configuration obtained at the end of the game is a winning configuration.


## Step 4: Start the game.

The final step in completing this functionality is to launch the game. To do this, you need to add a main function to your `textual_2048.py` file.

```python
if __name__ == '__main__':
    game_play()
    exit(1)  
```

#### :clap: <span style="color: #26B260">At this stage, you have reached MILESTONE 8: orchestrate a project with a main function</span> 

We've completed our objective 1 and therefore have our MVP for the 2048 game. Before moving on and updating your git repository, we ask you to take the time to add as many unit tests as possible to your project (if you've applied the TDD methodology, this shouldn't take you too long) and to comment out your various functions.

#### :clap: <span style="color: #26B260">At this stage, you have reached MILESTONE 9: reinforce the developments, as well as MILESTONE 10: Conceive and implement a MVP</span> 


## Step 5 : Handling parameters with `argparse`.

The aim here is to enable a given user to start the game program from the command line by specifying the various game parameters.

To do this, we'll be using the `argparse` module from python, for which documentation is available [here](https://docs.python.org/3/howto/argparse.html).

Train yourself quickly on this module and use it to start the game from the command line with parameters. Here, you could enable a mechanism to:

* start the game from the command line with parameters.
* or launch the program as we have done up to now, asking the player for the game parameters after the program has been launched.


## Step 6: User feedbacks on the game.

At this stage, we'll also be seeking user feedback on this MVP. You're going to take this role and test your game by playing with it. Each member of the group can play a game, for example.

What is your feedback on this MVP? Are there any missing features you'd like to add to your project?

Take the time to discuss them within the group and list the features you'd like to add to your game. Prioritize them too.

Following this brainstorming, you'll add a `TO_DO.txt` (or `TO_DO.md`) file to your project in which you'll list, in order of priority, these different features.

#### :clap: <span style="color: #26B260">At this stage, you have reached MILESTONE 11: Get and analyze user feedbacks after a first MVP</span> 


## To finish up

+ <span style='color:blue'>Commit your last changes.</span> 
+ <span style='color:blue'>Tag this last commit</span> 
+ <span style='color:blue'>Synchronize</span> 
+ <span style='color:blue'>Make a code coverage test of your MVP and push the obtained summary on your remote GitLab repo.</span>


We can now move on to [objective 2](./TemplateProject_2048.md#objective-2-a-2048-with-a-graphical-user-interface-mvp-improvement).
