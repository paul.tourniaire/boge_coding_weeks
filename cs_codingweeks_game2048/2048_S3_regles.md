# Feature 4: Handling tile movements.

At this stage of the project, it's assumed that you've acquired a certain degree of autonomy in implementing the various functionalities, so the rest of the project will be a little less guided.


## Step 1: Move a tile line to the left.

To manage tile movements, a simple way is to start by reasoning at the level of a line and implementing the movement of this line in a given horizontal direction, for example moving a line to the left.

We are going to write a test that will enable us to test the leftward movement of a given line.


```python
def test_move_row_left():

    assert move_row_left([0, 0, 0, 2]) == [2, 0, 0, 0]
    assert move_row_left([0, 2, 0, 4]) == [2, 4, 0, 0]
    assert move_row_left([2, 2, 0, 4]) == [4, 4, 0, 0]
    assert move_row_left([2, 2, 2, 2]) == [4, 4, 0, 0]
    assert move_row_left([4, 2, 0, 2]) == [4, 4, 0, 0]
    assert move_row_left([2, 0, 0, 2]) == [4, 0, 0, 0]
    assert move_row_left([2, 4, 2, 2]) == [2, 4, 4, 0]
    assert move_row_left([2, 4, 4, 0]) == [2, 8, 0, 0]
    assert move_row_left([4, 8, 16, 32]) == [4, 8, 16, 32]
```

In which module will you write this test and the associated function(s)?

Movement management will concern the game grid, so we'll place the elements corresponding to this functionality in the `grid_2048` module.


## Step 2: Move a tile line to the right.

Using the previous function `move_row_left(row)`, write the function `move_row_right(row)` which moves a row of tiles to the right.

A corresponding test could be:

```python
def test_move_row_right():

    assert move_row_right([2, 0, 0, 0]) == [0, 0, 0, 2]
    assert move_row_right([0, 2, 0, 4]) == [0, 0, 2, 4]
    assert move_row_right([2, 2, 0, 4]) == [0, 0, 4, 4]
    assert move_row_right([2, 2, 2, 2]) == [0, 0, 4, 4]
    assert move_row_right([4, 2, 0, 2]) == [0, 0, 4, 4]
    assert move_row_right([2, 0, 0, 2]) == [0, 0, 0, 4]
    assert move_row_right([2, 4, 2, 2]) == [0, 2, 4, 4]
    assert move_row_right([2, 4, 4, 0]) == [0, 0, 2, 8]
    assert move_row_right([4, 8, 16, 32]) == [4, 8, 16, 32]
```

Take a close look at these two test functions. This should give you a tip on how to write the `move_row_right` function to pass the previous test.


## Step 3: Grid movement following a `d` instruction

Here we write the function `move_grid(grid,d)`, which moves all the tiles in the grid `grid` in a given direction `d`.

Tip: for vertical movements, you can use a transformation on your grid, which will enable you to use the above functions.

You can use the test below to test your grid movement function.


```python

def test_move_grid():
    assert move_grid([[2,0,0,2], [4, 4, 0, 0], [8, 0, 8, 0], [0, 2, 2, 0]],"left") == [[4,0,0,0], [8, 0, 0, 0], [16, 0, 0, 0], [4, 0, 0, 0]]
    assert move_grid([[2,0,0,2], [4, 4, 0, 0], [8, 0, 8, 0], [0, 2, 2, 0]],"right") == [[0,0,0,4], [0, 0, 0, 8], [0, 0, 0, 16], [0, 0, 0, 4]]
    assert move_grid([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 2, 0]],"up") == [[4,8,4,2], [16, 2, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    assert move_grid([[2,0,0,2], [2, 4, 0, 0], [8, 4, 2, 0], [8, 2, 2, 0]],"down") == [[0, 0, 0, 0], [0, 0, 0, 0],[4,8,0,0],[16, 2, 4, 2]]
```

We've now completed this feature, you'll need to:

+ <span style='color:blue'>Commit</span> 
+ <span style='color:blue'>Review and synchronize</span> 


We can now turn our attention to the next stage in the design of the game logic itself, by testing the end of the game.
This is [**Feature 5**: Testing the end of the game](./2048_S3_Finjeu.md).
