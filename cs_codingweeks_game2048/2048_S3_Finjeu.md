# Feature 5: Testing the game over

Here we write the functions needed to test the game over, i.e. that the grid is full or that no further movement is possible.

## Step 1: Test if the grid is full.

Write the function `is_grid_full(grid)` which tests whether the grid is full or not.

Write the unit test for this function in your test file.

## Step 2: Test if there are still possible moves.

Write the function `move_possible(grid)` which tests for each of the possible directions whether movement is possible and returns a list of Booleans.

Your function should pass the following test

```python
def test_move_possible():
    assert move_possible([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]) == [True,True,True,True]
    assert move_possible([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2]]) == [False,False,False,False]
```

## Step 3: Test game over.

Write the function `is_game_over(grid)` to test whether the game is over or not, depending on the state of the grid.

## Step 4: Test if game is won.

To complete this functionality, we now need to test whether the resulting set is a winner or not. A winning configuration is one in which there is a tile in the grid with a value greater than or equal to 2048.

You therefore need to equip your program with a `get_grid_tile_max()` function, which returns the highest value of the tiles in the grid.

Now that we've completed this functionality, you'll need to:

+ <span style='color:blue'>Commit</span> 
+ <span style='color:blue'>Review and synchronize</span> 

We now have all of the ingredients to play the game. Let us move on to **Feature 6**: [Game orchestration](./2048_S4_Playing.md)




