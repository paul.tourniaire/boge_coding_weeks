# Feature 8: Display the game grid in a Tkinter window


The aim here is to set up your Tkinter interface for displaying the game. We'll start by working on the `display_grid` module.


## Step 1: Creating the Tkinter structure to display the game grid.

Step 1 consists in displaying the initial game grid in Tkinter. In this step, we'll focus solely on displaying the grid. There will be no actions associated with the various grid objects.

For this step, you need to :

+ Define the type of widget objects our grid will be represented with.
+ Define a number of constants that will be used to display the grid, such as the colors of the different tiles according to their values, the colors of the values themselves, and the size of the main window.


### The choice of game grid representation in Tkinter

To display our game grid with Tkinter, we're going to use the `Toplevel` widget, which lets you create primary windows, i.e. windows that have an independent existence for the operating system's window manager.

In a function you'll name `2048_graphical_grid_init()`, you'll start by:

+ Create a main window with Tkinter, entitled 2048, and create a `Toplevel` widget, entitled 2048, which you will associate with your main window.
+ Place this widget using Tkinter's `grid()` placement function.

At this point, you should have this at program runtime.



![Vue du jeu 2048 en Tkinter](./Images/gui2048_step1.png)

We therefore have two windows. At this point, you might be wondering why! You'll understand the purpose of these two windows later.

### Tile grid representation

We're now going to create and display the universe itself.

+ Initialize a game environment.
+ To represent our game environment, which is a grid of cells, each cell representing a tile, we'll first define a `Frame` which will represent our game's backgroud and *contain* the various cells. So create the `background` variable, which will refer to a `Frame` widget representing the background of our game.
+ Look at the [`Canvas`](https://tkdocs.com/tutorial/canvas.html) widget to display the game grid.


![Vue du jeu 2048 en Tkinter](./Images/gui2048_step2.png)


At this stage, we have two grid objects:

+ `grid_game`, which contains the game data to be displayed (the **MODEL**).
+ `graphical_grid`, our Tkinter data structure, which contains the presentation of the graphical interface (the **VIEW**).

This is a classic approach to GUI development. It corresponds to a software architecture pattern called [**Model-View-Controller or MVC**](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur) dedicated to graphical user interfaces, which dates back to 1978 and has become very popular. This pattern is made up of three types of modules with three different responsibilities: models, views and controllers:

+ A **Model** containing the data to be displayed.
+ A **View** containing the graphical interface presentation.
+ A **Controller** containing the logic for the actions performed by the user.

So far, we've respected this principle with at least the first two modules. However, we still need to connect the view to the model, and that's what we're going to do right now.

###  Displaying the values of the true game grid

Now you need to display on your interface the values of the tiles corresponding to the *real* game grid, i.e. the `grid_game` grid, your model.

To do this, you need to configure your cells by updating the `text`, `bg` and `fg` parameters of your various `Frame` objects with the corresponding values in `grid_game`.

Before writing your code, you need to take some time to think about the design. The kind of question you need to ask yourself is how many times this view update operation can occur in the course of the game. Once? Several times? Each game turn?

Depending on your answer, it may therefore be useful to put the code corresponding to this task in a function, for example called `display_and_update_graphical_grid()`, which you can call whenever necessary.

For an initial grid corresponding to `[[2, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2], [0, 0, 0, 0]]`, you should have this type of view:


![Vue du jeu 2048 en Tkinter](./Images/gui2048_step3.png)


#### :clap: <span style="color: #26B260">At this stage, you have reached MILESTONE 12: Get familiar with the MVC design pattern</span> 


## Step 2: Set up player action via the graphical interface.


We now need to work on our interface's event processing to respond to user actions. To play, the user just needs to indicate a direction, so it seems natural to do so via the 4 arrows on the keyboard. We're therefore dealing with a `KeyPress` event, and you can find out about the different ways of naming the keys [here](https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html)
 <!-- or in this [tutorial](http://tkinter.fdex.eu/doc/event.html). -->

Write a `key_pressed(event)` function that executes game actions, i.e. directions given by the user via his keyboard, on the game grid and updates the GUI. 

This function is an event handler, so don't forget to bind it to your game widget, the `graphical_grid` window, using the `bind` function to make our GUI responsive to user actions.


### Step 3: Play!

At this stage of the project, you should already be able to play 2048 via your graphical interface. Take the time to do so, and don't forget to list any features you feel are missing.

Now that we've completed this functionality, you'll need:

+ <span style='color:blue'>Commit</span> 
+ <span style='color:blue'>Synchronize and review</span> 

We can now add graphical means to configure the game. It corresponds to [Feature 9: Allow game configuration via the graphical interface](./2048_S6_configgrille.md)
 