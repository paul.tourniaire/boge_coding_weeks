# Programing a 2048 game

The **objective** of this mini-project is to develop, in a very incremental way, the 2048 game in order to train you in good programming practices and the culture of software quality. In particular, through this project, you will discover several principles of the so-called [*Software Craftsmanship* paradigm](https://github.com/jnguyen095/clean-code/blob/master/Clean.Code.A.Handbook.of.Agile.Software.Craftsmanship.pdf). 


## About the 2048 game

2048 is a puzzle-style video game designed in March 2014 by Italian independent developer Gabriele Cirulli and published online under an open license via Github [here](http://gabrielecirulli.github.io/2048/). If you're not familiar with this game, take 5 minutes to play a few games!

![Vue du jeu 2048](./Images/2048.jpg)

The rules of the game are simple:

 + On each turn, a number 2 or 4 appears in an empty square of a 4x4 grid. 
 + The player can choose between 4 directions (up, down, left, right).
 + The numbers in the grid are moved as if they were falling in the player's desired direction.
 + Two consecutive identical numbers that collide during movement are replaced by their sum. 
 	+ Any square resulting from a merge in this round cannot be used in a new merge in this round.
 	+ Mergers are performed starting with the one closest to the edge (of the chosen direction), then moving upwards in the opposite direction. 	


For example, if you move to the left, line `2 2 4 4` will become line `4 8 0 0`, and if you move to the right, line `4 2 2 0` will become `4 4 0 0`, as the second `4` has just been created and will not be merged this round. 


The aim of the game is to drag tiles across the grid to create a tile bearing **at least** the number 2048. However, the player can continue to play after this goal has been reached to achieve the best possible score. Similarly, the default grid size is 4 x 4, but we can also make this a parameter of our program, which will default to 4 x 4.


## Mini-project organization

This mini-project is broken down into several objectives, themselves broken down into **sprints** and **features**. The notion of sprint refers to the [agile method](https://en.wikipedia.org/wiki/Agile_software_development). A sprint corresponds to an interval of time during which the project team will complete a certain number of tasks.


This breakdown has been done for you, but it's one of the first steps to be taken for any software development project, at least macroscopically. **Think about it next week!**

### Objective 1 (MVP): A minimal 2048, without graphics nor a player management system

The first objective is to build and implement a simple version of the 2048 game that could be described as a **[MVP (Minimum Viable product)](https://www.youtube.com/watch?v=joNKkWPafZs)**. This was the minimum objective for the first week.

This MVP concept was popularized by Eric Ries, author of [The Lean Startup](http://theleanstartup.com/), a specific approach to starting up a business and launching a product. The figure below explains the concept.

<img src="./Images/mvp.png" alt="drawing" width="500"/>


+ **Sprint 0**
	- [Installation of the technical foundation](./Sprint0Installbis.md)
	+ [Requirements analysis](./Sprint0Analyse.md)
	+ [Design review](./Sprint0Conception.md)

+ **Sprint 1: Setting up the game data**
 	+ [**Feature 1**: Represent a game grid](./2048_S1_Grille.md)
 	+ [**Feature 2**: Display a game grid](./2048_S1_Display_Grille.md)

+ **Sprint 2: Player actions**
 	+ [**Feature 3**: Have a player—Give game instruction](./2048_S2_joueur.md)

+ **Sprint 3: Tile movement management**
 	+ [**Feature 4**: Move management](./2048_S3_regles.md)
 	+ [**Feature 5**: Test game end](./2048_S3_Finjeu.md)


 + **Sprint 4: Play!**
 	+ [**Feature 6**: Game orchestration](./2048_S4_Playing.md)


### Objective 2: A 2048 with a graphical user interface (MVP improvement)

When you tested your MVP, you must have found the game without a graphical user interface a real pain. This is also the first user feedback. Your job here is to develop your product to take into account this initial feedback, in particular by improving the interface through the addition of a graphical interface. 


+  **Sprint 5: Skill enhancement: graphical interfaces**
	+ [New skill: graphical interfaces with python](./2048_S5_GUI_Tutorial.md)
+  **Sprint 6: A graphical interface for the grid** 
	+ [**Feature 8**: Display the grid in a Tkinter window](./2048_S6_affichagegrille.md)
	+ [**Feature 9**: Allow for the game configuration via the graphical interface](./2048_S6_configgrille.md)


### Objective 3: A 2048 with player management, scores, move suggestions and more.

Through the two previous objectives, you have discovered development methods, and in particular the analysis, design and development stages. The analysis and design stages should enable you to break down an objective into _sprints_, themselves made up of different **features**. 

Once again, the completion of objective 2 should have enabled you to obtain initial user feedback. If you've reached this stage of the project, you can now apply all this to Objective 3. Along with your project code, you'll need to submit a file describing the proposed breakdown, in the same way as you did for the first two exercises.

